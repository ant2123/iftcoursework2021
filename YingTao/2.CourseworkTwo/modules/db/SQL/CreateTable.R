#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Ying Tao
# Date : 2021-12-15
# Topic : Coursework 2- Create Table
#--------------------------------------------------------------------------------------
# SEE THE OTHER COMPLETE STEPS AND DATA IN THE FILE NAMED "MAIN.R"

#Step 3
#Before create the new table, we need to check whether it is exists
dbListTables(conSql)
#If you want to remove the table
dbRemoveTable(conSql, "trades_suspects")
#If there is no new table, then Create new table: trades_suspects
dbExecute(conSql, "
  CREATE TABLE trades_suspects
  (
  Trader TEXT PRIMARY KEY,
  Symbol TEXT NOT NULL,
  close REAL NOT NULL,
  high REAL NOT NULL,
  low REAL NOT NULL,
  Quantity INTEGER NOT NULL,
  Notional REAL NOT NULL,
  NewPrice INTEGER NOT NULL,
  FOREIGN KEY (Symbol) REFERENCES equity_prices (symbol_id)
  )")

#using dbAppendTable or dbWriteTable to append the outliers we find in the new table "trades_suspects"
dbWriteTable(conn = conSql, name = "trades_suspects", value =OutliersRange, append = TRUE)

#if you want to remove the table, then use the code below and check
dbRemoveTable(conSql, "trades_suspects",OutliersRange)
dbListTables(conSql)
