#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Ying Tao
# Date: 2021-12-07
# Topic : retrieve all trades as per end of day (2021-11-11 and 2021-11-12) 
#--------------------------------------------------------------------------------------
# SEE THE OTHER COMPLETE STEPS AND DATA IN THE FILE NAMED "MAIN.R"

tradesEleven <- conMongo$find(query = '{"DateTime": {"$gte":  "ISODate(2021-11-11T00:00:00.000Z)", "$lt" : "ISODate(2021-11-11T23:59:59.999Z)"}}')             
tradesTwelve <- conMongo$find(query = '{"DateTime": {"$gte":  "ISODate(2021-11-12T00:00:00.000Z)", "$lt" : "ISODate(2021-11-12T23:59:59.999Z)"}}') 
#aggregate the 2 days data
trades_sub <- rbind(tradesEleven,tradesTwelve)

priceEleven <- dbGetQuery(conSql, paste0( "SELECT * FROM equity_prices WHERE cob_date = '11-Nov-2021'"))
priceTwelve <- dbGetQuery(conSql, paste0( "SELECT * FROM equity_prices WHERE cob_date = '12-Nov-2021'"))
