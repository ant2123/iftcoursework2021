SELECT = "SELECT"
UPDATE = "UPDATE"
DELETE = "DELETE"
INSERT = "INSERT"
CREATE = "CREATE"
trader_statics = {
    'database': "backend/Equity.db",
    'table': "trader_static",
    'columns': ["trader_id", "trader_name","fund_name","fund_type","fund_focus","fund_currency","is_active","golive_date","termination_date"]
}
portfolio = {
    'database': "backend/Equity.db",
    'table': "portfolio_positions",
    'columns': ["pos_id", "cob_date","trader","symbol","ccy","net_quantity","net_amount"]
}
