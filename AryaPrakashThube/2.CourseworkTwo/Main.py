#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import json
import pymongo
import pandas as pd
from pymongo import MongoClient
import matplotlib.pyplot as plt
import numpy as np


def connect():
	client = pymongo.MongoClient('mongodb://127.0.0.1:27017')
	#print(client)
	mydb = client['trades']
	info = mydb.tradesinfo

	with open('CourseworkTwo.json') as file:
	    file_data = json.load(file)

	if isinstance(file_data, list):
	    info.insert_many(file_data)  
	else:
	    info.insert_one(file_data)


# In[ ]:


import json
import pymongo
import pandas as pd
from pymongo import MongoClient
import sqlite3 as sql
from sklearn import preprocessing
from kmodes.kprototypes import KPrototypes
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler
from IPython.display import display

client = pymongo.MongoClient('mongodb://127.0.0.1:27017')
#print(client)
mydb = client['Equity']
info = mydb.CourseworkTwo


# In[ ]:


query={}


cursor = info.find(query)
print (cursor)
df =  pd.DataFrame(list(cursor))
print("df",df)


# In[ ]:


query1={
    'DateTime': {
    "$gte": 'ISODate(2021-11-11T00:00:00.000Z)',
    "$lt" :'ISODate(2021-11-11T23:59:59.000Z)'
    }
}
date11 = info.find(filter=query1)
print('date11', date11)
date11_df =  pd.DataFrame(list(date11))
print("date11_df",date11_df)


# In[ ]:


query2={
    'DateTime': {
    "$gte": 'ISODate(2021-11-12T00:00:00.000Z)',
    "$lt" :'ISODate(2021-11-12T23:59:59.000Z)'
    }
}

date12 = info.find(filter=query2)
print('date12', date12)
date12_df =  pd.DataFrame(list(date12))
print("date12_df",date12_df)


# In[ ]:


#For 11th Nov
kmeans = KMeans(n_clusters=2)
incosistenct_trades = kmeans.fit_predict(date11_df[['Quantity','Notional']])
date11_df['cluster']=incosistenct_trades
print('date11_df' , date11_df)

print(date11_df)

date11_df1 = date11_df[date11_df.cluster==1]
print('date11_df1' , date11_df1)
date11_df1 = date11_df1.applymap(str)
date11_df2 = date11_df[date11_df.cluster==2]
date11_df2 = date11_df2.applymap(str)

date11_df = date11_df.applymap(str)


# In[ ]:


#For 12th Nov
kmeans = KMeans(n_clusters=2)
inconsistent_trades = kmeans.fit_predict(date12_df[['Quantity','Notional']])

date12_df['cluster']=inconsistent_trades 
print('date12_df' , date12_df) 
print('date12_df[cluster',date12_df['cluster'])

date12_df1 = date12_df[date12_df.cluster==1]
print('date12_df1',date12_df1)
date12_df1 = date12_df1.applymap(str)
date12_df2 = date12_df[date12_df.cluster==2]
print('date12_df2',date12_df2)
date12_df = date12_df.applymap(str)


# In[ ]:


new_table = [date11_df1,date12_df2]
suspect_trades = pd.concat(new_table)
suspect_trades = suspect_trades.applymap(str)

conn = sql.connect('C:/Users/arya8/OneDrive/Desktop/Equity.db')
c = conn.cursor()
c.execute("""drop table if exists suspect_trades""")
c.execute("""CREATE TABLE "suspect_trades")
suspect_trades.to_sql('suspect_trades', conn, if_exists='append',index=False)


# In[ ]:


df= df.applymap(str)
df.to_sql('all_trades', conn, if_exists='append',index=False)


# In[ ]:


c.execute (""" INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)
select all_trades.tradeid||all_trades.DateTime||equity_static.symbol , 
all_trades.dateTime , all_trades.trader  , all_trades.symbol , all_trades.ccy , sum( quantity) , sum(notional)
from all_trades join equity_static 
on all_trades.symbol = equity_static.symbol
group by all_trades.tradeid||all_trades.dateTime||equity_static.symbol , 
all_trades.dateTime , all_trades.trader  , all_trades.symbol , all_trades.ccy
""")


# In[ ]:


conn.commit()


# In[ ]:




