#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Coursework Two : 2021-12-19
# Topic   : retrieve day11 and day12
#--------------------------------------------------------------------------------------

# Load the whole database
wholedatabase <- conMongo$find() 
head(wholedatabase)

# This is the code for MongoDB to find all the data on day 11 ------------------------
# db.CourseworkTwo.find({DateTime: {"$gte": "ISODate(2021-11-12T00:00:00.000Z)", "$lt": "ISODate(2021-11-13T00:00:00.000Z)" }})
# Extract all trading data for 2021-11-11 and 2021-11-12 in R
Day11 <- conMongo$find("{\"DateTime\":{\"$gte\":\"ISODate(2021-11-11T00:00:00.000Z)\",\"$lt\":\"ISODate(2021-11-12T00:00:00.000Z)\"}}")
Day12 <- conMongo$find("{\"DateTime\":{\"$gte\":\"ISODate(2021-11-12T00:00:00.000Z)\",\"$lt\":\"ISODate(2021-11-13T00:00:00.000Z)\"}}")

