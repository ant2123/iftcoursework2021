#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Coursework Two : 2021-12-19
# Topic   : Fat Finger Day12
#--------------------------------------------------------------------------------------

# Select day12 price information from equity database
Day12Equityprice <- dbGetQuery(conSql, "SELECT symbol_id, high, low, cob_date FROM equity_prices WHERE cob_date = '12-Nov-2021';")
Day12Equityprice

# 'Day12' is the trading information we get from json file
# left join those two tables in R using left_join() function---------------------
Day12fulldate <- left_join(Day12,Day12Equityprice, by= c('Symbol' = 'symbol_id'))
Day12fulldate

# add calculated pricelevel to dataframe
Day12fulldate$Pricelevel <- Day12fulldate[ ,6]/Day12fulldate[ ,5]
Day12fulldate

# find the person who made mistake ---------------------------------------
logicalDay12 <- Day12fulldate$Pricelevel >= Day12fulldate$low & Day12fulldate$Pricelevel <= Day12fulldate$high
logicalDay12
Day12mistake <- Day12fulldate[!logicalDay12, ]
Day12mistake
