#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Weihao Sheng
# Coursework Two : 2022-01-31
# Topic   : Historical performance and risk
#--------------------------------------------------------------------------------------

# Historical return and risk based on day 12's portfolio allocation ----------------------------------------------
# benchmark price is S&P500
benchmarkPrices <- getSymbols.yahoo('^GSPC', from='2020-01-02',to='2021-11-13', periodicity = "daily", auto.assign=FALSE)[,4]

# US 10 year treasure price
TNXPrices <- na.omit(getSymbols.yahoo('^TNX', from='2020-01-02',to='2021-11-13', periodicity = "daily", auto.assign=FALSE)[,4])

# check if we have missing data
colSums(is.na(mergedf2))
colSums(is.na(benchmarkPrices))
colSums(is.na(TNXPrices))

# calculate historical daily return
stockReturn <- na.omit(ROC(mergedf2))
benchmarkReturn <- na.omit(ROC(benchmarkPrices))
TNXReturn <- na.omit(ROC(TNXPrices))

# portfolio historical return, based on day 12 portfolio allocation
portfolioReturn <- Return.portfolio(weights = weights,stockReturn)

# variance or risk 
matrixweights <- as.matrix(sapply(weights, as.numeric)) 
tmatrixweights <- t(matrixweights)
cov <- var(stockReturn)
var <- (tmatrixweights %*% cov)%*% matrixweights 

# volatility or std
std <- sqrt(var)

# risk free rate is the average return on US 10 year treasure
rfReturn <- sum(TNXReturn)/471

# alpha
alpha <- CAPM.jensenAlpha(portfolioReturn, benchmarkReturn, rfReturn)

# beta
beta <- CAPM.beta(portfolioReturn, benchmarkReturn, rfReturn)

# sharp ratio
SharpeRatio(portfolioReturn, rfReturn)

# Annualized return
table.AnnualizedReturns(portfolioReturn)
table.CalendarReturns(portfolioReturn)




