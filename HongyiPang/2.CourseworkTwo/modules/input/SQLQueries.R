#--------------------------------------------------------------------------------------
# CW2
# Author: Hongyi Pang
# Topic   : Create SQL queries 
#--------------------------------------------------------------------------------------


# 1. SQL Query to load portfolio_positions ----------------------------------------
PortfolioPositions <- paste0("SELECT * FROM portfolio_positions")

# 2. SQL Query to load traderl_limits ----------------------------------------
Limits <- paste0("SELECT * FROM trader_limits")

# 3. SQL Query to load equity_prices
EquityPrices <- paste0("SELECT * FROM equity_prices")

#4.  SQL Query to load equity_static
EquityStatic <- paste0("SELECT * FROM equity_static")

#5.  SQL Query to load trader_static
TraderStatic <- paste0("SELECT * FROM trader_static")
