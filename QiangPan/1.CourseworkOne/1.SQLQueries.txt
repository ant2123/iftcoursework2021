/*
Query1
*/
SELECT * from equity_prices
LEFT join equity_static                                                   --join tables
on equity_prices.symbol_id=equity_static.symbol
where cob_date like '%2021' and symbol = 'COST';  --checking the information about 'costco' in 2021


 /*
Query2
*/
WITH A AS(
SELECT cob_date,symbol_id,
high,low,(high-low)/low as fluctuation_rate,
open,volume,currency
from equity_prices)                    --temporarily create a table called 'A' containing some information
SELECT symbol_id,ROUND(stdev(fluctuation_rate),5) as fluc from A GROUP BY symbol_id
having cob_date like '%2021';    --check the fluctuation rate of different stocks in 2021


 /*
Query3 --calculate the pnl 
*/
SELECT portfolio_positions.cob_date,trader,symbol,net_quantity,net_amount,
open,high,low,close,volume,currency,
net_quantity * close as mtm_amount,
net_quantity * close - net_amount as pnl FROM portfolio_positions --SELECT information and calculate pnl 
LEFT JOIN equity_prices                                                        --join tables
on equity_prices.symbol_id = portfolio_positions.symbol
where equity_prices.cob_date = '03-Jan-2020' ;              --choose the specific date 
