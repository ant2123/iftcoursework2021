#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#SQLQueries
def SQL_AllFinancialsData():
    return ("SELECT * from equity_prices LEFT join equity_static on equity_prices.symbol_id=equity_static.symbol where GICSSector = 'Financials'")

def SQL_Financials():
    return ("SELECT * from portfolio_positions where trader='SML1458'")

def SQL_ReturnData():
    return ("SELECT cob_date,symbol_id,(close-open)/open as return from equity_prices LEFT join equity_static on equity_prices.symbol_id=equity_static.symbol where GICSSector = 'Financials'")

def SQl_BenchmarkdailyReturn():
    return ("SELECT cob_date,Benchmark_daily_return from benchmark_returns where cob_date BETWEEN '2021-10-14' AND '2021-11-11'  order by cob_date")

