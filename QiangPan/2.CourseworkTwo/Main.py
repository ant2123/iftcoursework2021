#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pymongo import MongoClient
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import datetime as dt
from bson import json_util
import json




# In[2]:
# import our Stock Data from MongoDB
# open connectivity to MongoDB
# MongoDB Query

con = MongoClient('mongodb://localhost')
db = con.equity
col = db.CourseworkOne
fullFinancials = col.find({"StaticData.GICSSector":"Financials"}) 
fullFinancials = pd.DataFrame(fullFinancials)


# In[3]:

#lets see what is in our dataframe
fullFinancials.head()
fullFinancials.info()


# In[4]:
#show the dataframe

mongo_data = list(db['CourseworkOne'].find({"StaticData.GICSSector":"Financials"}))
sanitized = json.loads(json_util.dumps(mongo_data))
fullFinancials = pd.json_normalize(sanitized)
df1 = pd.DataFrame(fullFinancials)
df1


# In[5]:


df2 = df1[['Symbol','MarketData.MarketCap']]
df2


# In[6]:
#calculate the marketcap percentage of each stock

df3= df2['MarketData.MarketCap'].apply(lambda x :x/df2['MarketData.MarketCap'].sum())
df3


# In[7]:


df4 = pd.concat([df2,df3], axis=1) 
df4.columns=['Symbol','MarketData.MarketCap','MarketCap_Weighted']
df4


# In[8]:
#set up directories

GITRepoDirectory = "/Users/abrazar"


# In[9]:
# import packages

from sqlalchemy import create_engine
import datetime


# In[10]:
#create engine and connect to SQL

def SQL_AllFinancialsData():
    return ("SELECT * from equity_prices LEFT join equity_static on equity_prices.symbol_id=equity_static.symbol where GICSSector = 'Financials'")

engine = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()

# In[11]:
#import Financials Stock Information from SQL

df5 = pd.read_sql_query(SQL_AllFinancialsData(),engine)
df5


# In[12]:


df6 = df5[['symbol_id','cob_date','close','GICSSector']]
df6


# In[13]:
#combine the marketcap percentage and the stock information

df7=pd.merge(df4,df6,how='inner',left_on='Symbol',right_on='symbol_id',sort=True)
df7


# In[14]:
#calculate the weighted Prices of each stock in different date

df8 = df7[['Symbol','cob_date','MarketCap_Weighted','close']]
df8['Weighted_Prices']=df8['MarketCap_Weighted']*df8['close']
df8


# In[15]:
#calculate the annual weighted price of each stock
#market cap weighted index price

df8['cob_date'] = pd.to_datetime(df8['cob_date'])
df9 = df8.groupby(['cob_date'])['Weighted_Prices'].sum()
df9 = df9.reset_index()
df9.columns=['cob_date','Weighted_sum_prices']
df9


# In[16]:
#market cap weighted index price in the past year

df10=df9[(df9['cob_date']>'2020-11-10')].reset_index()
df10
df11=df10[['cob_date','Weighted_sum_prices']]
df11


# In[17]:


df12 = df11.sort_values(by="cob_date",ascending=False) 
df13 = df12.reset_index()
df14 = df13[['cob_date','Weighted_sum_prices']]
df14


# In[18]:
#calculate the benckmark turn

prices=df14['Weighted_sum_prices']
def daily_return(prices):
    return prices[:-1].values /prices[1:] - 1
df15=prices[:-1].values /prices[1:] - 1
df16=df15.reset_index()
df17=df16[['Weighted_sum_prices']]


# In[19]:


df18 = pd.concat([df14,df17], axis=1) 
df18.columns=['cob_date','Weighted_sum_prices','Benchmark_daily_return']
df18


# In[20]:


def SQLDeleteTable():
    return("DROP TABLE benchmark_returns")

def SQLCreateRetTable():
    Command = "CREATE TABLE benchmark_returns (    Symbol TEXT NOT NULL,    cob_date datetime NOT NULL,    MarketCap_Weighted INTEGER,    close INTEGER,    Weighted_Prices INTEGER,    Benchmark_daily_Return INTEGER    )"
    return (Command)


# In[21]:
#create a table and store all benchmark returns for the past year into a new sql table called benchmark_returns.

df18.to_sql('benchmark_returns',con=engine,if_exists='replace',index=False) 


# In[22]:

#select the portfolio information of the trader 'Simon Mare'
def SQL_Financials():
    return ("SELECT * from portfolio_positions where trader='SML1458'")

df19 = pd.read_sql_query(SQL_Financials(),engine)
df19['cob_date'] = pd.to_datetime(df19['cob_date'])
df19


# In[23]:
#select the stock that the trader 'Simon Mare' has in 2021-11-10

df20 = df19[['cob_date','trader','symbol','net_quantity','net_amount']]
df21 = df20[(df20['cob_date']>'2021-10-21')].reset_index()
df22 = df21[['cob_date','trader','symbol','net_quantity','net_amount']]
df22 = df22[(df22['cob_date']=='2021-11-10')].reset_index()
df22 = df22[['cob_date','trader','symbol','net_quantity','net_amount']]
df22


# In[24]:


df23 = df22[(df22['cob_date']=='2021-11-10')].reset_index()
df24 = df23[['cob_date','trader','symbol','net_quantity','net_amount']]
df24


# In[25]:
#calculate the portfolio allocations on 2021-11-10

df23= df22['net_amount'].apply(lambda x :x/df22['net_amount'].sum())
df23


# In[26]:


df24 = pd.concat([df22,df23], axis=1)
df24.columns=['cob_date','trader','symbol','net_quantity','net_amount','Portfolio_Allocation']
df24


# In[27]:
#list all the stock symbol 'Simon Mare' has in 2021-11-10

list1=df24['symbol'].values.tolist()
list1


# In[28]:
#calculate the daily return of the stock which is in the Simon Mare's portfolio in the past 20 days prior to 2021-11-11

def SQL_ReturnData():
    return ("SELECT cob_date,symbol_id,(close-open)/open as return from equity_prices LEFT join equity_static on equity_prices.symbol_id=equity_static.symbol where GICSSector = 'Financials'")
df25 = pd.read_sql_query(SQL_ReturnData(),engine)
df25['cob_date'] = pd.to_datetime(df25['cob_date'])
df25 = df25[(df25['cob_date']>'2021-10-13')&(df25['cob_date']<'2021-11-11')].reset_index()
df25 = df25[['cob_date','symbol_id','return']]
df25 = df25.sort_values(by=['cob_date','symbol_id'],ascending=(True,True))
df25 = df25.reset_index()
df25 = df25[['cob_date','symbol_id','return']]
df25


# In[29]:
#calculate the daily return of the stock(in list1) in the past 20 days prior to 2021-11-11

d = pd.DataFrame({'symbol_id':list1})
df26 = pd.merge(d,df25)
df26 = df26.sort_values(by=['cob_date','symbol_id'],ascending=(True,True))
df26 = df26.reset_index()
df26 = df26[['cob_date','symbol_id','return']]
df26


# In[30]:
# a is the dataframe containing the information about the symbol_id of stock in Simon Mare's portfolio and its daily return in the past 20 days prior to 2021-11-11

a=df26.pivot_table(index='cob_date',columns='symbol_id')
a


# In[31]:
#transfer the value of  'a' to an array

b = df24[['Portfolio_Allocation']].values.tolist()
b = np.array(b)
b


# In[32]:
#derive the daily returns for the portfolio allocations on 2021-11-10 over the past 20 days for the selected trader "Simon Mare"

daily_return = np.dot(a,b)
daily_return


# In[33]:
#select the marketcap weighted benckmark between 2021-10-14 and 2021-11-11

def SQl_BenchmarkdailyReturn():
    return ("SELECT cob_date,Benchmark_daily_return from benchmark_returns where cob_date BETWEEN '2021-10-14' AND '2021-11-11'  order by cob_date")


# In[34]:


df27 = pd.read_sql_query(SQl_BenchmarkdailyReturn(),engine)
df27


# In[35]:


df28 = pd.DataFrame(daily_return)
df28.columns=['daily_return']
df28


# In[36]:


df29 = pd.concat([df27,df28], axis=1) 
df29


# In[37]:
#count the date if the portfolio daily return > benchmark daily return

df29 = df29[(df29['daily_return']>df29['Benchmark_daily_return'])]
df29['cob_date'].count()


# In[ ]:
#final check
print("After final check,the trader has exceeded the benchmark",df29['cob_date'].count(),"times in the past 20 (working)days prior to 2021-11-11.")




# In[ ]:





# %%
