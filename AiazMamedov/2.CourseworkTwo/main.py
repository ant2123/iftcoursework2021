#UCL -- Big Data in Quantitative Finance
#Author : Aiaz Mamedov
#Topic  : Coursework 2 - Incorrect Trade Detection (Python)
#Target : Extract data for trades on 11-Nov-2021 and 12-Nov-2021 from the database(NoSQL), which are submitted
#         by traders. Verify that these trades are genuine and in line with market expectations by comparisson
#         with the data in Equity.db database(SQL). Load the suspect trades in a new table in Equity.db and 
#         load the remaining non-suspect trades in existing portfolio_positions table in Equity.db 

from db import create_tables
from less import prepare_sql_data, save_result


if __name__ == '__main__':
    print('Creating the tables...')
    create_tables()
    print('Ready! Collecting data from SQL...')
    trades = prepare_sql_data()
    print('Just found some suspects! Moving them in trades_suspects and others in porfolio_positions...')
    save_result(trades)
    print('Finito!')
