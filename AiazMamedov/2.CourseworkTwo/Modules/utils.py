from datetime import datetime

def convert_date(date):
    date = date.split('(')[1].split('T')[0]
    date = datetime.strptime(date, '%Y-%m-%d').strftime('%d-%b-%Y')
    return date
print('Finished running utils.py!')
