import json
from db import db_session, engine, NewTable, Portfolio
from sqlalchemy import MetaData, select
from utils import convert_date


def prepare_sql_data():
    meta = MetaData()
    meta.reflect(bind=engine)
    table = meta.tables['equity_prices']
    s = select(table).where(table.c.cob_date.in_(('11-Nov-2021', '12-Nov-2021')))
    rows = engine.connect().execute(s)

    trades = {}
    for row in rows:
        if row[-2] in ['12-Nov-2021', '11-Nov-2021']:
            symbol = row[-1]
            trades.setdefault(symbol, {})
            trades[symbol][row[-2]] = {'low': row[3], 'high': row[2]}
    return trades



def save_result(trades):
    with open ('CourseworkTwo.json', 'r', encoding='utf-8') as f:
        data_1 = json.load(f)
        for row in data_1:
            if '2021-11-11' in row['DateTime'] or '2021-11-12' in row['DateTime']:
                symbol = row['Symbol']
                price = row['Notional'] / row['Quantity']
                date = convert_date(row['DateTime'])
                if trades[symbol][date]['low'] > price or price > trades[symbol][date]['high']:
                    check = NewTable.query.filter(NewTable.trade_id==row['TradeId']).first()
                    if not check:
                        new_row = NewTable(
                            date=row['DateTime'],
                            trade_id=row['TradeId'],
                            trader=row['Trader'],
                            symbol=row['Symbol'],
                            quantity=row['Quantity'],
                            notional=row['Notional'],
                            trade_type=row['TradeType'],
                            ccy=row['Ccy'],
                            counter_party=row['Counterparty'],
                            low=trades[symbol][date]['low'],
                            high=trades[symbol][date]['high'],
                            price=price
                        )
                        db_session.add(new_row)
                else:
                    check = Portfolio.query.filter(Portfolio.pos_id==row['TradeId']).first()
                    if not check:
                        new_row = Portfolio(
                                    pos_id=row['TradeId'],
                                    cob_date=date,
                                    trader=row['Trader'],
                                    symbol=row['Symbol'],
                                    ccy=row['Ccy'],
                                    net_quantity=row['Quantity'],
                                    net_amount=row['Notional']
                        )
                        db_session.add(new_row)
                db_session.commit()
print('Finished running less.py!')