#UCL -- Big Data in Quantitative Finance
#Author : Aiaz Mamedov
#Topic  : Coursework 2 - Incorrect Trade Detection (Python)
#Target : Extract data for trades on 11-Nov-2021 and 12-Nov-2021 from the database(NoSQL), which are submitted
#         by traders. Verify that these trades are genuine and in line with market expectations by comparisson
#         with the data in Equity.db database(SQL). Load the suspect trades in a new table in Equity.db and 
#         load the remaining non-suspect trades in existing portfolio_positions table in Equity.db 

#-------------------------------------------------------------------------------------------------------------#
#PART_1: We need to establish connection with SQL database Equity.db and create table suspect_trades. 
#-------------------------------------------------------------------------------------------------------------#

from sqlalchemy import create_engine, Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import MetaData, select, insert
import os

import json
from datetime import datetime
from pprint import pprint

# This code is used to secure consistency for connection to database, when using different operating systems. 
basedir = os.path.dirname(__file__)
path = os.path.join(basedir, 'Equity.db')

# Create engine to use SQL and start the session to work with the database
engine = create_engine('sqlite:///' + path)
db_session = scoped_session(sessionmaker(bind=engine))

# Create a new class(Base), which has the feautures of the SQL table and create an attribute for SQL queries
Base = declarative_base()
Base.query = db_session.query_property()

# Create a new table in Equity.db, which has all columns from NoSQL table, columns 'low' and 'high' from the
# equity_prices table in Equity.db and column 'price' (price of the stock, obtained at a later stage)
class NewTable(Base):
    __tablename__ = 'suspect_trades'
    id = Column(Integer, primary_key=True)
    date = Column(String)
    trade_id = Column(String)
    trader = Column(String)
    symbol = Column(String)
    quantity = Column(Integer)
    notional = Column(Float)
    trade_type = Column(String)
    ccy = Column(String)
    counter_party = Column(String)
    low = Column(Float)
    high = Column(Float)
    price = Column(Float)

# Similarly, we connect to 'portfolio_positions' table in Equity.db in order to insert data from NoSQl for
# non-suspect trades, after identifying suspect trades
class Portfolio(Base):
    __tablename__ = 'portfolio_positions'
    pos_id = Column(String, primary_key=True)
    cob_date = Column(String)
    trader = Column(String)
    symbol = Column(String)
    ccy = Column(String)
    net_quantity = Column(Integer)
    net_amount = Column(Float)

if __name__ == '__main__':
    Base.metadata.create_all(bind=engine)

#-------------------------------------------------------------------------------------------------------------#
#PART_2: Identifying suspect trades and inserting them into a suspect_trades table in Equity.db, non-suspect 
#        trades are inserted into porfolio_positions table in Equity.db 
#-------------------------------------------------------------------------------------------------------------#

# Create meta-environment to work with the equity_prices table from Equity.db using SQL
meta = MetaData()
meta.reflect(bind=engine)
table = meta.tables['equity_prices']

# In order to select the data from the required dates, execute a query using SqlAlchemy ORM
s = select(table).where(table.c.cob_date.in_(('11-Nov-2021', '12-Nov-2021')))
rows = engine.connect().execute(s)

# In order to identify trade for each equity, we exctract the 'low' and 'high' of the stock for each date from
# equity_prices, using symbol_id as a key - obtained in this format {AAPL: {'11-Nov': {'low': 56, 'high': 62}}}
trades = {}
for row in rows:
    if row[-2] in ['12-Nov-2021', '11-Nov-2021']:
        symbol = row[-1]
        trades.setdefault(symbol, {})
        trades[symbol][row[-2]] = {'low': row[3], 'high': row[2]}

# We extract the necessary data from the CoureworkTwo.json database for 11-Nov-2021 and 12-Nov-2021
with open('CourseworkTwo.json', 'r', encoding='utf-8') as f:
    data_1 = json.load(f)
    for row in data_1:
        # Due to different formats of date, we need to transform the format used in .json file to match
        # the format used in equity_prices table from Equity.db & we obtain the value of price (equity)
        # by dividing the notional amount by quantity
        if '2021-11-11' in row['DateTime'] or '2021-11-12' in row['DateTime']:
            symbol = row['Symbol']
            price = row['Notional'] / row['Quantity']
            date = row['DateTime']
            date = date.split('(')[1].split('T')[0]
            date = datetime.strptime(date, '%Y-%m-%d').strftime('%d-%b-%Y')
            # Identify which trades are suspects by checking the price is in the range from equity_prices
            if trades[symbol][date]['low'] > price or price > trades[symbol][date]['high']:
                # Check whether the value has been added before, in case code is ran multiple times
                check = NewTable.query.filter(NewTable.trade_id==row['TradeId']).first()
                if not check:
                    # If the trade price is outside the specified range for each stock, we add this trade 
                    # to the suspect_trades table
                    new_row = NewTable(
                        date=row['DateTime'],
                        trade_id=row['TradeId'],
                        trader=row['Trader'],
                        symbol=row['Symbol'],
                        quantity=row['Quantity'],
                        notional=row['Notional'],
                        trade_type=row['TradeType'],
                        ccy=row['Ccy'],
                        counter_party=row['Counterparty'],
                        low=trades[symbol][date]['low'],
                        high=trades[symbol][date]['high'],
                        price=price

                    )
                    db_session.add(new_row)
            else:
                # If the trade is within the range, we insert the values into the portfolio_positions table
                check = Portfolio.query.filter(Portfolio.pos_id==row['TradeId']).first()
                if not check:
                    new_row = Portfolio(
                                pos_id=row['TradeId'],
                                cob_date=date,
                                trader=row['Trader'],
                                symbol=row['Symbol'],
                                ccy=row['Ccy'],
                                net_quantity=row['Quantity'],
                                net_amount=row['Notional']
                    )
                    db_session.add(new_row)
            db_session.commit()



