import configparser
import pytest

def read_ini(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)
    debug = config['default']['database']
    print(type(debug))
    db_repo = config.get('default', 'database', fallback='database repo is not defined')
    print(db_repo)
    return debug


def test_validation_configureparser():
    # It raises KeyError rather FileNotFoundError
    with pytest.raises(KeyError):
        read_ini(file_path='./config/non.ini')  # File not exist in the foler

