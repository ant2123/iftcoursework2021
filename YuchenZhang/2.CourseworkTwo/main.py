# ===============================================================================
# cd D:\Git\iftcoursework2021\YuchenZhang\2.CourseworkTwo
# python main.py

# ===============================================================================

from sqlalchemy import create_engine
from pymongo import MongoClient
import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt


from modules.encrypt.encrypt import read_encrypted, get_key
from modules.db.SQL.SQLQueries import SQLEquityPrices, SQLDeleteSuspectTable
from modules.db.SQL.CreateTable import SQLCreateSuspectTradeTable

import argparse
import configparser

config = configparser.ConfigParser()
config.read('./config/config.ini')
# Load 'default' section
defaults = config['default']

parser = argparse.ArgumentParser()
# Add SQL database path as one of the argument
parser.add_argument("-db", "--database", default= defaults['database'], type=str, help="SQL Database Path")
# Pase arguments
args = parser.parse_args()
print(args)


# 0.0 Extract relevant data from SQL database and MongoDB ---------------------------------------

# 0.1 SQL Database --------------------------------------------------
RepoDirectory = args.database
engine = create_engine(f"sqlite:///{RepoDirectory}/000.DataBases/SQL/Equity.db")
con = engine.connect()

print("[INFO]", datetime.now().strftime('%F %T'),  "Opened SQL database successfully...\n")

equityPrices = pd.read_sql_query(SQLEquityPrices(), engine)
print(equityPrices.head())

con.close()


# 0.2 MongoDB ------------------------------------------------------
USER = read_encrypted('./config/yuchen_credential.cr', get_key('./config/yuchen_key'))['username']
PASSWORD = read_encrypted('./config/yuchen_credential.cr', get_key('./config/yuchen_key'))['password']

connection_string = f"mongodb+srv://{USER}:{PASSWORD}@cluster0.u1l6v.mongodb.net/CourseworkTwo?ssl=true&ssl_cert_reqs=CERT_NONE"

client = MongoClient(connection_string)
db = client['Equity']
col = db['CourseworkTwo']
print("[INFO]", datetime.now().strftime('%F %T'),  "Opened MongoDB successfully...\n")

# MongoDB query
tradeRecord = col.find({})
tradeRecord = pd.DataFrame(tradeRecord)
print(tradeRecord.head())


# Copy the original dataframe and calcualte the trade price for each trade
tradePrice = tradeRecord.copy()
tradePrice['TradePrice'] = tradePrice.Notional/tradePrice.Quantity
tradePrice.head()

# Clean and parse the DateTime column 
tradePrice['DateTime'] = tradePrice['DateTime'].map(lambda x: x.split('(')[1].split(')')[0])
tradePrice['DateTime'] = pd.to_datetime(tradePrice['DateTime'])
tradePrice.head()

client.close()


# 1. Extract all trades as per end of day----------------------------------------------------

# Create start and end time to filter data
eleven_start_time = '2021-11-11T00:00:00'
eleven_end_time = '2021-11-11T23:59:59'
# Trades for 2021-11-11
trade_11 = tradePrice[(tradePrice['DateTime'] >= eleven_start_time) & (tradePrice['DateTime'] <= eleven_end_time)]
print(trade_11)

# Create start and end time to filter data

twelve_start_time = '2021-11-12T00:00:00'
twelve_end_time = '2021-11-12T23:59:59'

# Trades for 2021-11-12
trade_12 = tradePrice[(tradePrice['DateTime'] >= twelve_start_time) & (tradePrice['DateTime'] <= twelve_end_time)]
print(trade_12)


# 2.1. Use clustering analysis to identify trades susspects ------------------------------------
# Determine the number of clusters using graph
wss = np.zeros(15)
for i in range(15):
    wss[i] = KMeans(n_clusters=i+1).fit(tradePrice[['Quantity', 'Notional']]).inertia_
plt.figure(figsize=(13,6))
plt.plot(wss,'co--')
plt.show()

# We pick 6 clusters
kmeans = KMeans(n_clusters=6)
# Fit the model
kmeans.fit(tradePrice[['Quantity', 'Notional']])
# Find cluster centers
clusterCenters = pd.DataFrame(kmeans.cluster_centers_)
# Show the cluster centers
print(clusterCenters)

# Plot the positions of the clusters 
plt.figure(figsize=(15,7))
plt.scatter(tradePrice.loc[:,'Quantity'], tradePrice.loc[:,'Notional'],c=kmeans.labels_)
plt.scatter(clusterCenters.iloc[:,0],clusterCenters.iloc[:,1],c=range(kmeans.n_clusters),marker='x')
plt.scatter(clusterCenters.iloc[:,0],clusterCenters.iloc[:,1],c=range(kmeans.n_clusters),alpha=0.3,s=2000)
plt.xlabel('Quantity')
plt.ylabel('Notional')
plt.show()
# The plot shows some extreme values and we can filter them out
print(tradePrice[tradePrice.Quantity > 40000])
print(tradePrice[tradePrice.Notional > 100000000])

# 2.2. Use hypothesis testing to detect suspect trades --------------------------------------

# First, we need to calculate the mean and standard deviation of the equities from Equity Databse
# Create a equity stats dataframe for each stock
equityStats = equityPrices.groupby('symbol_id')['close'].agg(['mean','std']).reset_index()
print(equityStats)

# Merge dataframe trade_11 and equityStats 
merged_11 = pd.merge(
    trade_11,
    equityStats[['symbol_id', 'mean', 'std']],
    how="left",
    left_on='Symbol',
    right_on='symbol_id')

# Calculate the z score for each trading price 
merged_11['zScore'] = abs(merged_11['mean'] - merged_11['TradePrice'])/merged_11['std']

# Trade prices with a z score above 3 are deemed suspicious
suspect_11 = merged_11[merged_11['zScore'] >3]


# Merge dataframe trade_12 and equityStats 
merged_12 = pd.merge(
    trade_12,
    equityStats[['symbol_id', 'mean', 'std']],
    how="left",
    left_on='Symbol',
    right_on='symbol_id')
merged_12['zScore'] = abs(merged_12['mean'] - merged_12['TradePrice'])/merged_12['std']
suspect_12 = merged_12[merged_12['zScore'] >3]

# Put trades suspects from 2021-11-11 and 2021-11-12 together
trades_suspects = suspect_11.append(suspect_12)
trades_suspects.drop(columns=['_id', 'TradePrice', 'mean', 'std', 'zScore', 'symbol_id'], inplace=True)
trades_suspects = trades_suspects.reset_index(drop=True)
print('[INFO] Suspected Trades...\n',   trades_suspects)


# 3. Create data table in the SQL database ---------------------------------------------------

RepoDirectory = args.database
# Create engine
engine = create_engine(f"sqlite:///{RepoDirectory}/000.DataBases/SQL/Equity.db")
con = engine.connect()

print("[INFO]", datetime.now().strftime('%F %T'), "Opened database successfully...\n")

# Depends on whether there is a trades_suspects table in the Equity.db
# con.execute(SQLDeleteSuspectTable())

con.execute(SQLCreateSuspectTradeTable())

print('[INFO]', datetime.now().strftime('%F %T')," Insert records into trades_suspects....\n")
for i in range(len(trades_suspects)):
    con.execute(f'INSERT INTO trades_suspects (trade_id, trade_time, trader, symbol, quantity, notional,trade_type, ccy, counterparty)\
                  VALUES ("{trades_suspects.loc[i, "TradeId"]}", \
                      "{trades_suspects.loc[i, "DateTime"]}", \
                      "{trades_suspects.loc[i, "Trader"]}",\
                      "{trades_suspects.loc[i, "Symbol"]}",\
                      {trades_suspects.loc[i, "Quantity"]},\
                      {trades_suspects.loc[i, "Notional"]},\
                      "{trades_suspects.loc[i, "TradeType"]}",\
                      "{trades_suspects.loc[i, "Ccy"]}",\
                      "{trades_suspects.loc[i, "Counterparty"]}")')
print('[INFO]', datetime.now().strftime('%F %T')," Insert successfully and new table created....\n")

con.close()


# 4. Aggregate Quantity and Notional for all trades  ---------------------------------------------------

# Use the orginal dataset extract from MongoDB
# Remove trades_suspects in all the trades
tradeRecord = tradeRecord[~tradeRecord['TradeId'].isin(trades_suspects['TradeId'])]

# Clean the time column
tradeRecord['DateTime'] = tradeRecord['DateTime'].map(lambda x: x.split('(')[1].split(')')[0])
# Parse the time and store it in a new column named 'cob_date'
tradeRecord['cob_date'] = pd.to_datetime(tradeRecord['DateTime'])
# Change the time to a specific format
tradeRecord['cob_date'] = tradeRecord['cob_date'].apply(lambda x: x.strftime('%d-%b-%Y'))
# Use lower characters to all column names
tradeRecord.columns = tradeRecord.columns.map(lambda x: x.lower())

# Aggregate 'quantity' and 'notional' for all trade
aggTrade = tradeRecord[['cob_date', 'trader', 'symbol', 
                        'ccy', 'quantity', 'notional']].groupby(['cob_date','trader','symbol','ccy']).sum()
aggTrade = aggTrade.reset_index().rename(columns={'quantity':'net_quantity', 'notional':'net_amount'})

# Create a tempTime column to generate unique 'pos_id'
aggTrade['tempTime'] = pd.to_datetime(aggTrade['cob_date']).astype(str).apply(lambda x: ''.join(x.split('-')))
aggTrade['pos_id'] = aggTrade['trader'].astype(str) + aggTrade['tempTime'] + aggTrade['symbol'].astype(str)
aggTrade.drop(['tempTime'], axis=1, inplace=True)
# Cheack the final dataset
print(aggTrade.head())

RepoDirectory = './../..'
engine = create_engine(f"sqlite:///{RepoDirectory}/000.DataBases/SQL/Equity.db")
con = engine.connect()

print("[INFO]", datetime.now().strftime('%F %T'), "Opened database successfully...\n")

for i in range(len(aggTrade)):
    con.execute(f'INSERT OR IGNORE INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)\
                  VALUES ("{aggTrade.loc[i, "pos_id"]}", \
                      "{aggTrade.loc[i, "cob_date"]}", \
                      "{aggTrade.loc[i, "trader"]}",\
                      "{aggTrade.loc[i, "symbol"]}",\
                      "{aggTrade.loc[i, "ccy"]}",\
                      {aggTrade.loc[i, "net_quantity"]},\
                      {aggTrade.loc[i, "net_amount"]})')
print('[INFO]', datetime.now().strftime('%F %T')," Insert completed....\n")

con.close()