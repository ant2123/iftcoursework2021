# ----------------------------------------------------------------------------------
# cd /iftcoursework2021/YuchenZhang/3.CourseworkThree
# python -m scr.App
# ----------------------------------------------------------------------------------


from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, validators
from flask import Flask, render_template, flash
from modules.encrypt.encrypt import read_encrypted, get_key
from modules.db.db_mongo import get_mongo_trade_record
from modules.db.db_sql import get_sql_equity_static, get_sql_equity_prices
from modules.trader.trader_stats import generate_trader_plot, calcualte_portfolio_var
from datetime import datetime
import configparser


# 1. Extract configrations ------------------------------------------------------
config = configparser.ConfigParser()
config.read('config/config.ini')

MONGODB = config['DATABASE']['MONGODB']
COLLECTION = config['DATABASE']['COLLECTION']
SQLDB_PATH = config['DATABASE']['SQLDB_PATH']

USER = read_encrypted('config/yuchen_credential.cr',
                      get_key('config/yuchen_key'))['username']
PASSWORD = read_encrypted('config/yuchen_credential.cr',
                          get_key('config/yuchen_key'))['password']

CONNECTION_STRING = f"mongodb+srv://{USER}:{PASSWORD}@{MONGODB}/{COLLECTION}?ssl=true&ssl_cert_reqs=CERT_NONE"


def generate_static_images(connection_string, sqldb_path):
    # 1.1 Connect to MongoDB
    # Get dataframe tradeRecord
    tradeRecord = get_mongo_trade_record(connection_string=connection_string)

    # 1.2 Connect to SQL
    equityStatic = get_sql_equity_static(
        sqldb_path)     # Get SQL dataframe equityStatic
    equityPrices = get_sql_equity_prices(sqldb_path)

    # 1.3 Extract a given trader's info
    traders = tradeRecord.Trader.unique()

    for trader in traders:
        generate_trader_plot(tradeRecord, equityStatic, trader)
        print(f"  >> Generating plots for Trader {trader}...")
        calcualte_portfolio_var(equityPrices, tradeRecord, trader)
        print(f"  >> Calculating for Trader {trader}...")

    print("\n[INFO]", datetime.now().strftime('%F %T'),
          "Printing complete and plots are stored in \"static\" folder...")


# Run this function if there are no images in the "static" folder
# generate_static_images(CONNECTION_STRING, SQLDB_PATH)


# 2. Build app ------------------------------------------------------------------
app = Flask(__name__, static_folder='./../static/', template_folder='./../')

app.config['SECRET_KEY'] = 'seceret'


# Create a Form Class
class TraderForm(FlaskForm):
    trader = StringField("What's the trader' ID", [
        validators.AnyOf(['DGR1983', 'DHB1075', 'DMZ1796', 'JBX1566', 'MRH5231', 'SML1458'])],
        render_kw={'placeholder': "e.g., DGR1983"})
    submit = SubmitField("Submit")


@app.route('/', methods=['GET', 'POST'])
def trader():
    trader = None
    form = TraderForm()
    # Validate Form
    if form.validate_on_submit():
        trader = form.trader.data
        form.trader.data = ''
        flash("Form Submitted Successfully!")

    return render_template("Home.html",
                           trader=trader,
                           form=form)


# 3. Run app -------------------------------------------
if __name__ == '__main__':
    app.run(debug=True)
