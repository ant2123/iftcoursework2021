
db.CourseworkOne.distinct('StaticData.GICSSector')
db.CourseworkOne.find({"MarketData.MarketCap": null})

db.CourseworkOne.find({"StaticData.GICSSubIndustry" : {"$in": ["Interactive Media & Services Discretionary", "Interactive Home Entertainment"]}}, 
 {Symbol: 1, "StaticData.Security": 1, "MarketData.Beta": 1, "FinancialRatios.PERatio": 1})

db.CourseworkOne.aggregate([
    {$match: {"MarketData.MarketCap":  {$exists:1}}},
    {$group: {_id: "$StaticData.GICSSector", totalCapitalization: {$sum: "$MarketData.MarketCap"}, meanPE: {$avg: "$FinancialRatios.PERatio"}, count:{$sum:1}}},
    {$sort: {totalCapitalization: -1}}
])


