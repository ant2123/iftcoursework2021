#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Bainuo Chen
# Coursework : Coursework2
# Use Case   : Incorrect Trade Detection
# File       : CreateTable.R
#--------------------------------------------------------------------------------------

SQLCreateSusTable <- "CREATE TABLE trades_suspects (
  date_time TEXT NOT NULL,
  trade_id TEXT NOT NULL PRIMARY KEY,
  trader TEXT NOT NULL,
  symbol TEXT NOT NULL,
  quantity INTEGER NOT NULL,
  notional INTEGER NOT NULL,
  trade_type TEXT NOT NULL,
  ccy TEXT NOT NULL,
  counterparty TEXT NOT NULL,
  price_id TEXT NOT NULL,
  FOREIGN KEY (price_id) REFERENCES equity_prices(price_id))"

