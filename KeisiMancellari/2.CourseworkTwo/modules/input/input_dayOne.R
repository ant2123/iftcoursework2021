#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Keisi Mancellari
# Topic   : Coursework 2
# File    : Input loader
#--------------------------------------------------------------------------

library(mongolite)
library(ggplot2)
library(dplyr)

# Importing relevant values

dayOne <- conMongo$find('{"DateTime":{"$gte":"ISODate(2021-11-11T00:00:00.000Z)", 
                        "$lt":"ISODate(2021-11-12T00:00:00.000Z)"}}')

# Some summary statistics

summary(dayOne)

SummaryStatistics <- dayOne %>% 
  group_by(., Trader, Symbol, Ccy) %>% 
  summarise(., AvgQua = mean(Quantity), MedQua = median(Quantity), AvgNot =  mean(Notional),
            MedNot = median(Notional))

# Run kmeans clustering

tradesClusters <- kmeans(dayOne[,c("Notional", "Quantity")], 5)
str(tradesClusters)

dayOne$cluster <- factor(tradesClusters$cluster)
tradesCenters <- as.data.frame(tradesClusters$centers)
head(tradesCenters)

ggplot(data=dayOne, aes(x=Notional, y=Quantity, color= cluster)) + 
  geom_point() + 
  geom_point(data=tradesCenters, aes(x=Notional,y=Quantity, color='Center')) +
  geom_point(data=tradesCenters, aes(x=Notional,y=Quantity, color='Center'), size=52, alpha=.3, legend=FALSE)

# Use MAD to remove outliers

MAD <- median(abs(dayOne$Quantity - median(dayOne$Quantity)))

MADZScores <- (0.6745 * abs(dayOne$Quantity - median(dayOne$Quantity))) / MAD

# Use the score of 3.5 as the cut-off value for determining outliers

dayOneOutliers <- dayOne[MADZScores < 3.5, ]
nrow(dayOne)
nrow(dayOneOutliers)

# Fix date format

dayOneOutliers$DateTime <- substr(dayOneOutliers$DateTime, 9, 18)

# Running kmeans on Notional and Quantity

tradesClusters <- kmeans(dayOneOutliers[,c("Notional","Quantity")], 2)

# Visualise data

dayOneOutliers$cluster <- factor(tradesClusters$cluster)
tradesCenters <- as.data.frame(tradesClusters$centers)

ggplot(data=dayOneOutliers, aes(x = Quantity, y = Notional, color= cluster)) + 
  geom_point() + 
  geom_point(data=tradesCenters, aes(x = Quantity, y = Notional, color='Center')) +
  geom_point(data=tradesCenters, aes(x = Quantity, y = Notional, color='Center'), size=52, alpha=.3, legend=FALSE)
