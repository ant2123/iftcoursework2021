#--------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Keisi Mancellari
# Topic   : Coursework 2
# File    : Trade Suspects
#--------------------------------------------------------------------------

library(DBI)
library(RSQLite)

# Create a table of suspects

dayOneSuspects <- dayOne[MADZScores > 3.5, ]

# clean column name before loading into SQLite

dayOneSuspects <- as.data.frame(list(Date = dayOneSuspects$DateTime,
                                     Trader = dayOneSuspects$Trader,
                                     Symbol = dayOneSuspects$Symbol,
                                     Ccy = dayOneSuspects$Ccy,
                                     Quantity = dayOneSuspects$Quantity,
                                     Notional = dayOneSuspects$Notional), stringsAsFactors = F)
dayOneSuspects$Key <- paste0(dayOneSuspects$Trader, dayOneSuspects$Symbol, dayOneSuspects$Ccy)

# Open DB Connection ------------------------------------------------------

conSQL <- dbConnect(RSQLite::SQLite(), Config$Directories$EquityDatabase)

dbExecute(conSQL, "CREATE TABLE IF NOT EXISTS trades_suspects (
          Key TEXT PRIMARY KEY,
          Date TEXT NOT NULL,
          Trader TEXT NOT NULL,
          Symbol TEXT NOT NULL,
          Ccy TEXT NOT NULL,  
          Quantity INTEGER NOT NULL,
          Notional INTEGER NOT NULL,
          FOREIGN KEY (symbol) REFERENCES equity_static (symbol)
          );")

dayOneSuspects[, c("Key", "Date", "Trader", "Symbol", "Ccy", "Quantity", "Notional")]

dbWriteTable(conSQL, "dayOneSuspects", dayOneSuspects, append = TRUE)

# Check data got loaded correctly

dbGetQuery(conSQL, "SELECT * FROM dayOneSuspects;")


# Close SQL Connections --------------------------------------------------

dbDisconnect(conSQL)
