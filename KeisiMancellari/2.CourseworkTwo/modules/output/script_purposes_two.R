#--------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Keisi Mancellari
# Topic   : Coursework 2
# File    : Aggregated Trades
#--------------------------------------------------------------------------

library(dplyr)
library(RSQLite)

# Query to aggregate trades

portfolio_positions <- dayTwoOutliers %>% 
  group_by(., DateTime, Trader, Symbol, Ccy) %>% 
  summarise(., net_quantity = sum(Quantity), net_amount = sum(Notional))

names(portfolio_positions)[1:4] <- c("cob_date", "trader", "symbol", "ccy")

portfolio_positions$pos_id <- paste0(portfolio_positions$trader, portfolio_positions$cob_date, portfolio_positions$symbol)

# SQL Connection

conSQL <- dbConnect(RSQLite::SQLite(), Config$Directories$EquityDatabase)

portfolio_positions[, c("pos_id", "cob_date", "trader", "symbol", "ccy", "net_quantity", "net_amount")]

dbWriteTable(conSQL, "portfolio_positions", portfolio_positions, append = TRUE)

# Check data got loaded correctly

dbGetQuery(conSQL, "SELECT * FROM portfolio_positions;")


# Close SQL Connections -------------------------------------------

dbDisconnect(conSQL)
