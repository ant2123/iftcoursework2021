// MongoDB 
// 1) Aggregate all Sectors and return the average Beta by Sector for the stocks that have a market cap greater than or equal to 20000 
 
db.CourseworkOne.aggregate([ {$match: {"MarketData.MarketCap": {"$gte": 20000} } }, {$group: {_id: "$StaticData.GICSSector", average: {$avg: "$MarketData.Beta"} } }]) 
// 2) Count the equities with PE ratio between 10 and 25 
db.CourseworkOne.find({"FinancialRatios.PERatio": {"$gte": 10, "$lte": 25 }}).count() 

// 3) Find all stocks with market price lower than 200 and a negative beta. 
db.CourseworkOne.find({$and: [{"MarketData.Price": {"$lt": 200} }, {"MarketData.Beta": {"$lt":0 } } ] } ) 
 
// 4) Find all stocks with beta less than 1 and dividend yield greater than 2 
db.CourseworkOne.find({$and: [{ "MarketData.Beta": {"$lt": 1} }, {"FinancialRatios.DividendYield": {"$gte": 2 } } ] } ) 