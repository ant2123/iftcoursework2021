SQL 
 
SELECT DISTINCT pos_id FROM portfolio_positions 

SELECT price_id, open, high, low, cob_date, volume, symbol_id FROM equity_prices WHERE volume > 2000000; 

SELECT * FROM equity_static WHERE GICSSector = 'Energy'; 

SELECT * FROM trader_limits GROUP BY limit_amount; 

SELECT limit_amount, COUNT(limit_id) AS no_limits FROM trader_limits GROUP BY limit_amount; 
SELECT price_id, 
SUM(volume) AS sum_volume, 
ROUND(AVG(volume), 1) AS mean_volume, 
MIN(volume), MAX(volume) 
FROM equity_prices 
where open > 150 
GROUP BY price_id 
ORDER BY sum_volume ASC 
LIMIT 5; 
  
SELECT * FROM equity_static 
LEFT JOIN equity_prices ON equity_static.symbol = equity_prices.symbol_id 
WHERE volume < 1000000; 