# Import relevant packages
import sqlite3
import pandas as pd
from pymongo import MongoClient

pd.set_option('display.expand_frame_repr', False)

# Set up SQL and Mongodb connection
sql_Equity = "/Users/andreaspantelides/Desktop/BigData/iftcoursework2021/000.DataBases/SQL/Equity.db"
con_sql = sqlite3.connect(sql_Equity)
cursor = con_sql.cursor()

mongoc = MongoClient('mongodb://localhost')
db = mongoc.Equity
colle = db.CourseworkTwo

# Retrieve all the trades for Nov 11 and Nov 12 from Mongodb
Nov_11 = pd.DataFrame(colle.find({"DateTime": {"$gt": "ISODate(2021-11-11T00:00:00.000Z)", "$lte": "ISODate(2021-11-12T00:00:00.000Z)"}}))
Nov_12 = pd.DataFrame(colle.find({"DateTime": {"$gt": "ISODate(2021-11-12T00:00:00.000Z)", "$lte": "ISODate(2021-11-13T00:00:00.000Z)"}}))

# From SQL, select relevant columns for both dates to get the prices
sql_Nov_11 = pd.read_sql("SELECT symbol_id, low, high, cob_date from equity_prices WHERE cob_date = '11-Nov-2021' ", con_sql)
sql_Nov_12 = pd.read_sql("SELECT symbol_id, low, high, cob_date from equity_prices WHERE cob_date = '12-Nov-2021' ", con_sql)

# Divide Notional/Quantity 
Nov_11["Trade_Prices"] = Nov_11["Notional"]/Nov_11["Quantity"]
Nov_12["Trade_Prices"] = Nov_12["Notional"]/Nov_12["Quantity"]

# print(Nov_11.head())
# print(Nov_12.head())

# Using merge method to merge both the data from both databases
Nov_11_merge = pd.merge(Nov_11, sql_Nov_11, left_on='Symbol', right_on='symbol_id')
Nov_12_merge = pd.merge(Nov_12, sql_Nov_12, left_on='Symbol', right_on='symbol_id')

# print(Nov_11_merge.head())
# print(Nov_12_merge.head())

Nov_11_12_merge = pd.concat([Nov_11_merge, Nov_12_merge], ignore_index=True, sort=False)

# print(Nov_11_12_merge.head())

# Create a condition to identify the mistakes by Front Office
Nov_11_12_mistakes = Nov_11_12_merge.where((Nov_11_12_merge['Trade_Prices'] < Nov_11_12_merge['low']) | (Nov_11_12_merge['Trade_Prices'] > Nov_11_12_merge['high']))
Nov_11_12_mistakes = Nov_11_12_mistakes.dropna()
Nov_11_12_mistakes = Nov_11_12_mistakes.reset_index(drop=True)

print(Nov_11_12_mistakes)
print(len(Nov_11_12_mistakes))

# Create new table in SQL
def trade_fat_fingers():
    fat_fingers = "CREATE TABLE trade_suspects("\
                  " TradeId TEXT PRIMARY KEY, "\
                  " DateTime TEXT NOT NULL, "\
                  " Trader TEXT NOT NULL,"\
                  " Symbol TEXT NOT NULL,"\
                  " Quantity INT,"\
                  " Notional INT,"\
                  " TradePrices FLOAT,"\
                  " Low FLOAT, "\
                  " High FLOAT, "\
                  " FOREIGN KEY (Trader) REFERENCES equity_prices(symbol_id))"
    return fat_fingers


# con_sql.execute(trade_fat_fingers())


def table_checker():
    if con_sql.execute("SELECT COUNT(*) FROM trade_suspects"):
        return True


print(table_checker())

# Identified mistakes are inserted into the new SQL table
sql = "INSERT OR REPLACE INTO trade_suspects (TradeId, DateTime, Trader, Symbol, Quantity, Notional, TradePrices, Low, High) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
val = []
print(Nov_11_12_mistakes.columns)

for i in range(len(Nov_11_12_mistakes)):
    ls = [Nov_11_12_mistakes.loc[i, "TradeId"],
                     Nov_11_12_mistakes.loc[i, "DateTime"],
                     Nov_11_12_mistakes.loc[i, "Trader"],
                     Nov_11_12_mistakes.loc[i, "Symbol"],
                     Nov_11_12_mistakes.loc[i, "Quantity"],
                     Nov_11_12_mistakes.loc[i, "Notional"],
                     Nov_11_12_mistakes.loc[i, "Trade_Prices"],
                     Nov_11_12_mistakes.loc[i, "low"],
                     Nov_11_12_mistakes.loc[i, "high"]]
    val.append(tuple(ls))
print(val)
con_sql.executemany(sql, val)

# For each day aggregate quantity and notional by date, trader , symbol and ccy
Nov_11_agg = Nov_11_merge.groupby(['cob_date', 'Trader', 'Symbol', 'Ccy']).agg(Quantity=('Quantity', 'sum')).reset_index()
Nov_11_am = Nov_11_merge.groupby(['cob_date', 'Trader', 'Symbol', 'Ccy']).agg(Notional=('Notional', 'sum')).reset_index()
Nov_11_agg = Nov_11_agg.rename(columns={'Quantity': 'net_amount'})
Nov_11_am = Nov_11_am.rename(columns={'Notional': 'net_quantity'})

Nov_11_join = Nov_11_agg.merge(Nov_11_am, how="left")
print(Nov_11_agg)
print(Nov_11_am)
print(Nov_11_join)

# pod_id column created
Nov_11_join["pos_id"] = Nov_11_join["Trader"] + '20211111' + Nov_11_join["Symbol"]
print(Nov_11_join)

Nov_12_agg = Nov_12_merge.groupby(['cob_date', 'Trader', 'Symbol', 'Ccy']).agg(Quantity=('Quantity', 'sum')).reset_index()
Nov_12_am = Nov_12_merge.groupby(['cob_date', 'Trader', 'Symbol', 'Ccy']).agg(Notional=('Notional', 'sum')).reset_index()
Nov_12_agg = Nov_12_agg.rename(columns={'Quantity': 'net_amount'})
Nov_12_am = Nov_12_am.rename(columns={'Notional': 'net_quantity'})

Nov_12_join = Nov_12_agg.merge(Nov_12_am, how="left")
print(Nov_12_agg)
print(Nov_12_am)
print(Nov_12_join)

Nov_12_join["pos_id"] = Nov_12_join["Trader"] + '20211112' + Nov_12_join["Symbol"]
print(Nov_12_join)

# Upload our results to portfolio_positions SQL
def upload_table(table):
    for i in range(len(table)):
        cursor.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) \
                    VALUES ("{table.loc[i, "pos_id"]}",\
                    "{table.loc[i, "cob_date"]}",\
                    "{table.loc[i, "Trader"]}",\
                    "{table.loc[i, "Symbol"]}",\
                    "{table.loc[i, "Ccy"]}",\
                    {table.loc[i, "net_amount"]},\
                    {table.loc[i, "net_quantity"]})')


# upload_table(Nov_11_join)
# upload_table(Nov_12_join)

con_sql.commit()

con_sql.close()
