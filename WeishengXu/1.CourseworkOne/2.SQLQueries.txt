SELECT symbol_id, COUNT(cob_date) AS date_recorded
FROM equity_prices
GROUP BY symbol_id;

SELECT symbol_id,
MAX(close) AS highest_price,
MIN(close) AS lowest_price,
ROUND(AVG(volume), 1) AS mean_volume
FROM equity_prices
GROUP BY symbol_id
ORDER BY mean_volume DESC

SELECT GICSSector, COUNT(security) AS amount
FROM equity_static
GROUP BY GICSSector