Query 1 - Find the number of companies who are under the category: "GICSSector": "Real Estate"

db.CourseworkOne.find({"StaticData.GICSSector": {"$eq": "Real Estate" }}).count()


Query 2 - Aggregate all Sectors and return the average MarketCapitalization by Sector

db.CourseworkOne.aggregate([
{$match: {}},
{$group: {_id: "$StaticData.GICSSector", avg_MarketCap: {$avg: "$MarketData.MarketCap"}}},
{$sort: {avg_MarketCap:-1}}])



