Query 1 - top 5 equities which have the higheset order volume

SELECT equity_prices.open, equity_prices.volume, equity_prices.symbol_id, equity_static.security
FROM equity_prices
JOIN equity_static on equity_prices.symbol_id = equity_static.symbol
ORDER BY volume DESC
LIMIT 5;



Query 2 - profit and loss when the COB date is "29-Sep-2020"

SELECT portfolio_positions.trader, portfolio_positions.symbol, portfolio_positions.net_quantity, portfolio_positions.net_amount, equity_prices.close, equity_prices.cob_date, 
portfolio_positions.net_quantity * equity_prices.close AS m_to_m_value,
portfolio_positions.net_quantity * equity_prices.close - portfolio_positions.net_amount AS pnl
FROM portfolio_positions
LEFT JOIN equity_prices on portfolio_positions.symbol = equity_prices.symbol_id
WHERE equity_prices.cob_date = '29-Sep-2020';