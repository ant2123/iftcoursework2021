# SQL creates a new table: traders_12

SQLCreateTable <- "CREATE TABLE traders_12 (
  TradeId TEXT PRIMARY KEY,
  Counterparty TEXT NOT NULL,
  Symbol TEXT NOT NULL,
  Quantity INTERGER NOT NULL,
  Notional REAL NOT NULL,
  TradeType TEXT NOT NULL,
  DateTime TEXT NOT NULL,
  Ccy TEXT NOT NULL
  FOREIGN KEY (Symbol) REFERENCES equity_prices(Symbol))"
