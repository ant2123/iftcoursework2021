from fastapi.encoders import jsonable_encoder
import pymongo
import datetime
from motor.motor_asyncio import AsyncIOMotorClient
client = AsyncIOMotorClient("localhost:27017")
db = client.Equity


async def load_trade(trade_data):
    trade_time =  'ISODate('+datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.000Z)')
    trade_data.DateTime = trade_time
    trade_data.TradeId = trade_data.TradeType[0] + trade_data.Trader + trade_data.Symbol + datetime.datetime.strptime(trade_time, 'ISODate(%Y-%m-%dT%H:%M:%S.000Z)').strftime('%Y%m%d%H%M%S')
    trade = jsonable_encoder(trade_data)

    new_trade = await db["CourseworkTwo"].insert_one(trade)
    created_trade = await db["CourseworkTwo"].find_one({"_id": new_trade.inserted_id}, {'_id': 0})
    return created_trade