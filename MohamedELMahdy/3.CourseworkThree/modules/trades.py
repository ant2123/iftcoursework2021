from pydantic import BaseModel
from typing import Optional
import datetime

class Item(BaseModel):
     DateTime: Optional[datetime.datetime] = None
     TradeId: Optional[str] = None
     Trader: str
     Symbol: str
     Quantity: float
     Notional: float
     TradeType: str
     Ccy: str
     Counterparty: str