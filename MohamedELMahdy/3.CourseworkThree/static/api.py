from fastapi import FastAPI, Request
import modules.db.mongo as mongodb
from pydantic import BaseModel
from motor.motor_asyncio import AsyncIOMotorClient
import asyncio
from typing import Optional
import datetime

class Item(BaseModel):
     DateTime: Optional[datetime.datetime] = None
     TradeId: Optional[str] = None
     Trader: str
     Symbol: str
     Quantity: float
     Notional: float
     TradeType: str
     Ccy: str
     Counterparty: str

app = FastAPI()

@app.on_event("shutdown")
async def shutdown_db_client():
    client.close()

@app.post("/trade/")
async def create_item(trade: Item):
    str(trade)
    trade_handshake = mongodb.load_trade(trade)
    return trade_handshake