/*
Name	  : Mohamed ELMahdy
Student_ID: 21161896
Coursework: 1 - SQL queries
*/

-- Query1

select a.cob_date , b.GICSSector, round(max(a.close-a.open),2) security_earning , b.security
from equity_prices		a
left join equity_static		b
on a.symbol_id = b.symbol
where a.close-a.open > 0
group by a.cob_date, b.GICSSector
having cob_date like '%Nov-2021'
order by a.cob_date, round(max(a.close-a.open),2) desc, b.GICSSector


-- Query2

select x.trader_id, y.trader_name, x.limit_type, sum(x.limit_amount) 'Total Limit', x.currency
from trader_limits		x
left JOIN trader_static		y
on x.trader_id = y.trader_id
where x.limit_end is null
group by y.trader_name, x.limit_type, x.currency
having x.currency = 'USD'
order by y.trader_name, sum(x.limit_amount) desc

-- Query3

select f.symbol_id, h.security, max(f.close) 'Highest Price' , f.cob_date, h.GICSIndustry, h.GICSSector 
from equity_prices		f
left join equity_static		h
on f.symbol_id = h.symbol
group by f.symbol_id
order by 3 desc
