import sqlite3


def create_table(conn, c, trade_list, price_list):
    c.execute("PRAGMA foreign_keys=ON;")
    c.execute("CREATE TABLE trades_suspects (DateTime TEXT NOT NULL, TradeId TEXT PRIMARY KEY NOT NULL, Trader TEXT NOT NULL, Symbol TEXT NOT NULL, Quantity INTEGER NOT NULL, Notional REAL NOT NULL, TradeType TEXT NOT NULL, Ccy TEXT NOT NULL, Counterparty TEXT NOT NULL,price_id TEXT NOT NULL, FOREIGN KEY(price_id) REFERENCES equity_prices(price_id) ON DELETE CASCADE);")
    num_insert = len(price_list)
    for i in range(num_insert):
        trade = trade_list[i]
        price_id = price_list[i]
        statement = "INSERT INTO trades_suspects (DateTime, TradeId, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty, price_id) VALUES ("
        for key in trade.keys():
            if key != '_id':
                if key == "Quantity" or key == "Notional":
                    statement = statement + str(trade[key]) + ","
                else:
                    statement = statement + "'" + str(trade[key]) + "',"
        statement = statement + "'" + str(price_id) + "'" + ")"
        c.execute(statement)
        conn.commit()