# import argparse
import sys
import sqlite3
import importlib
import configparser



conf = configparser.ConfigParser()
def readconfig():
    conf.read("./config/script.config")
    x = conf.get("config","sqldatabase")
    SQLname = x
    x = conf.get("config","dbconnection")
    #print(x)
    sql_conn = importlib.import_module(x).sql_conn
    mongo_conn = importlib.import_module(x).mongo_conn
    x = conf.get("config", "dbcreate")
    create_table = importlib.import_module(x).create_table
    return SQLname, sql_conn, mongo_conn, create_table

def readparams():
    conf.read("./config/script.params")
    x = conf.get("params","db")
    db = x
    x = conf.get("params","collection")
    collection = x
    x = conf.get("params","url")
    url = x
    return db, collection, url


def main(sql_name, mongo_db, mongo_collection, url, sql_conn, mongo_conn, create_table):
    try:
        collection = mongo_conn(mongo_db, mongo_collection, url)
    except:
        print("Load MongoDB database error! This may be because you don't have a mongoDB dataset loacally or the local service is not started.")
        sys.exit(-1)
    try:
        conn, c = sql_conn(sql_name)
    except:
        print("Load sqlite3 database error! This may be because you don't provide a valid path to the sql database. You should provide your path like '/root/.../Equity.db'.")
        sys.exit(-1)
    

    trade_list = []
    price_list = []
    is_error = False
    # For 2021-11-11
    trader_info = []
    mongo_query = {"DateTime":{ "$regex":"2021-11-11","$options":"$i"}}
    for x in collection.find(mongo_query):
        trader_info.append(x)
    #Get prices information on 2021-11-11
    equity_prices = []
    sql_query="SELECT high, low, symbol_id, price_id FROM equity_prices WHERE cob_date == '11-Nov-2021'"
    cursor = c.execute(sql_query)
    for x in cursor:
        equity_prices.append(x)
    #Compare
    print("Begin to verify trader info in 2021-11-11.")
    print("======================================")
    for equity in equity_prices:
        symbol = equity[2]
        high=equity[0]
        low=equity[1]
        price_id = equity[3]
        total_notional = 0
        for trade in trader_info:
            if trade['Symbol'] == symbol:
                price = trade['Notional']/trade['Quantity']
                if price > high or price < low:
                    is_error = True
                    print("Error Detected.")
                    print("Mongo:",trade)
                    print("Sqlite3",equity)
                    trade_list.append(trade)
                    price_list.append(price_id)
                total_notional = total_notional + trade['Notional']


    # For 2021-11-12
    trader_info = []
    mongo_query = {"DateTime":{ "$regex":"2021-11-12","$options":"$i"}}
    for x in collection.find(mongo_query):
        trader_info.append(x)
    #Get prices information on 2021-11-12
    equity_prices = []
    sql_query="SELECT high, low, symbol_id, price_id FROM equity_prices WHERE cob_date == '12-Nov-2021'"
    cursor = c.execute(sql_query)
    for x in cursor:
        equity_prices.append(x)
    #Compare
    print("Begin to verify trader info in 2021-11-12.")
    print("======================================")
    for equity in equity_prices:
        symbol = equity[2]
        high=equity[0]
        low=equity[1]
        price_id = equity[3]
        total_notional = 0
        for trade in trader_info:
            if trade['Symbol'] == symbol:
                price = trade['Notional']/trade['Quantity']
                if price > high or price < low:
                    is_error = True
                    print("Error Detected.")
                    print("Mongo:",trade)
                    print("Sqlite3",equity)
                    trade_list.append(trade)
                    price_list.append(price_id)
                total_notional = total_notional + trade['Notional']


    # Create table
    if is_error is True:
        print("Creating suspect table.")
        create_table(conn, c, trade_list, price_list)
    else:
        print("No error detected. Skip creating table part.")


    # aggregate
    print("Aggregate tables.")
    # Get all symbols and traders
    symbols = []
    traders_name = []
    cursor = c.execute("SELECT symbol FROM equity_static")
    for x in cursor:
        symbols.append(x[0])
    cursor = c.execute("SELECT trader_id FROM trader_static")
    for x in cursor:
        traders_name.append(x[0])
    #print(symbols)
    #print(traders_name)    

    trader_info = []
    date = "'11-Nov-2021'"
    mongo_query = {"DateTime":{ "$regex":"2021-11-11","$options":"$i"}}
    for x in collection.find(mongo_query):
        trader_info.append(x)
    #print(trader_info)
    for i in symbols:
        for name in traders_name:
            pos_id = name + "20211111" + i
            notional = 0
            quantity = 0
            is_op = False
            for trade in trader_info:
                if trade['Symbol'] == i:
                    is_op = True
                    notional += trade['Notional']
                    quantity += trade['Quantity']
            insert_state = "INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) VALUES ("
            insert_state = insert_state + "'"+pos_id+"'" + "," + date + ","+"'"+ name +"'"+ "," +"'"+ i+"'" + ", 'USD', " + str(quantity) + "," + str(notional) + ")"
            #print(insert_state)
            if is_op is True:
                c.execute(insert_state)
    
    trader_info = []
    date = "'12-Nov-2021'"
    mongo_query = {"DateTime":{ "$regex":"2021-11-12","$options":"$i"}}
    for x in collection.find(mongo_query):
        trader_info.append(x)
    for i in symbols:
        for name in traders_name:
            pos_id = name + "20211112" + i
            notional = 0
            quantity = 0
            is_op = False
            for trade in trader_info:
                if trade['Symbol'] == i:
                    is_op = True
                    notional += trade['Notional']
                    quantity += trade['Quantity']
            insert_state = "INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) VALUES ("
            insert_state = insert_state + "'"+pos_id+"'" + "," + date + ","+"'"+ name +"'"+ "," +"'"+ i+"'" + ", 'USD', " + str(quantity) + "," + str(notional) + ")"
            if is_op is True:
                c.execute(insert_state)
    conn.commit()
    conn.close()



if __name__ == '__main__':
    SQLname, sql_conn, mongo_conn, create_table = readconfig()
    db, collection, url = readparams()

    main(SQLname,db, collection, url, sql_conn, mongo_conn, create_table)
