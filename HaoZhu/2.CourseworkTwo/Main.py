import sys
import configparser
import datetime
import pandas as pd
import pymongo
import sqlite3
from modules.db.db_connection import sql_connect, mongo_connect
from modules.db.load_mongo import load_mongo
from modules.db.SQL.CreateTable import CreateTabel

if __name__ == '__main__':
    config = configparser.ConfigParser()

    # Load SQL database
    config.read('./config/script.config')
    sql_db = config.get('sql', 'sql_dbpath')

    try:
        conn, c = sql_connect(sql_db)
        print("[INFO] Successfully load SQL database!!")
    except:
        print("[ERROR] Error when loading SQL database..")
        sys.exit(-1)

    load_mongo()

    config.read('./config/script.params')
    db_mongo = config.get('nosql', 'mongodb')
    collection = config.get('nosql', 'collection')
    url = config.get('nosql', 'url')

    try:
        collection = mongo_connect(db_mongo, collection, url)
        print("[INFO] Successfully load NoSQL database!!")
    except:
        print("[ERROR] Error when loading NoSQL database..")
        sys.exit(-1)

    print("[INFO] Begin to check transaction data on 2021-11-11...")

    equity_prices, price_ids = [], []
    trade_, trade_suspect = [], []
    mongo_query = {"DateTime": {"$regex": "2021-11-11", "$options": "$i"}}

    trade_ = [trade for trade in collection.find(mongo_query)]

    sql_query = "SELECT high, low, volume, symbol_id, price_id FROM equity_prices WHERE cob_date == '11-Nov-2021'"
    cursor = c.execute(sql_query)

    any_error = False
    for equity in cursor:
        high, low, volume, symbol, price_id = equity[0], equity[1], equity[2], equity[3], equity[4]
        for trade in trade_:
            if trade['Symbol'] == symbol:
                trade_price = trade['Notional'] / trade['Quantity']
                if trade_price > high or trade_price < low or trade['Quantity'] > volume:
                    print("[ERROR] False Transaction Detected...")
                    any_error = True
                    trade_suspect.append(trade)
                    price_ids.append(price_id)

    print("[INFO] Begin to check transaction data on 2021-11-12...")
    mongo_query = {"DateTime": {"$regex": "2021-11-12", "$options": "$i"}}
    trade_ = [trade for trade in collection.find(mongo_query)]

    sql_query = "SELECT high, low, volume, symbol_id, price_id FROM equity_prices WHERE cob_date == '12-Nov-2021'"
    cursor = c.execute(sql_query)

    for equity in cursor:
        high, low, volume, symbol, price_id = equity[0], equity[1], equity[2], equity[3], equity[4]
        for trade in trade_:
            if trade['Symbol'] == symbol:
                trade_price = trade['Notional'] / trade['Quantity']
                if trade_price > high or trade_price < low or trade['Quantity'] > volume:
                    print("[ERROR] False Transaction Detected...")
                    any_error = True
                    trade_suspect.append(trade)
                    price_ids.append(price_id)

    # Create and insert table for suspect trades
    if any_error:
        print("[INFO] Inserting suspect trades...")
        CreateTabel(conn, c, trade_suspect, price_ids)
        print("[INFO] Done inserting suspect trades...")

    # Now aggregate Quantity and Notional
    print("[INFO] Begin aggregating transactions")

    symbols = [equity[0] for equity in c.execute("SELECT symbol FROM equity_static")]
    traders = [trader[0] for trader in c.execute("SELECT trader_id FROM trader_static")]
    ccys = [c[0] for c in c.execute("SELECT DISTINCT currency FROM equity_prices")]
    dates = pd.date_range(start='2021-11-11', end='2021-11-12').to_pydatetime()
    dates = [str(date)[:10] for date in dates]
    for date in dates:
        for symbol in symbols:
            for trader in traders:
                for ccy in ccys:
                    mongo_query = {"DateTime": {"$regex": date, "$options": "$i"},
                                   "Trader": trader, "Symbol": symbol, "Ccy": ccy}
                    trans = [tr for tr in collection.find(mongo_query)]
                    pos_id = trader + datetime.datetime.strptime(date, '%Y-%m-%d').strftime('%Y%m%d') + symbol

                    to_ex = False
                    if len(trans) > 0:
                        to_ex = True
                        notional = sum([tran['Notional'] for tran in trans])
                        quantity = sum([tran['Quantity'] for tran in trans])

                        insert_sql = "INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) VALUES ("
                        insert_sql += "'" + pos_id + "','" + (datetime.datetime.strptime(date, '%Y-%m-%d').strftime('%d-%b-%Y'))
                        insert_sql += "','" + trader + "','" + symbol + "','" + ccy
                        insert_sql += "'," + str(quantity) + "," + str(notional) + ")"
                    if to_ex:
                        c.execute(insert_sql)
    conn.commit()
    print("[INFO] Done aggregating transactions")

    conn.close()
