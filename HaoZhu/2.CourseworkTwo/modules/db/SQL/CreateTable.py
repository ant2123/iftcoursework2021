import sqlite3


def CreateTabel(connection, cursor, transactions, price_ids):
    # Create the trades_suspects table
    cursor.execute("PRAGMA foreign_keys=ON;")
    cursor.execute(
        "CREATE TABLE trades_suspects (DateTime TEXT NOT NULL, TradeId TEXT PRIMARY KEY NOT NULL, Trader TEXT NOT NULL, Symbol TEXT NOT NULL, Quantity INTEGER NOT NULL, Notional REAL NOT NULL, TradeType TEXT NOT NULL, Ccy TEXT NOT NULL, Counterparty TEXT NOT NULL,price_id TEXT NOT NULL, FOREIGN KEY(price_id) REFERENCES equity_prices(price_id) ON DELETE CASCADE);")
    for idx in range(len(transactions)):
        insert_script = "INSERT INTO trades_suspects (DateTime, TradeId, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty, price_id) VALUES ("
        for field in transactions[idx].keys():
            if field == "_id":
                continue
            insert_script += (str(transactions[idx][field]) + ",") if field in ["Quantity", "Notional"] else \
                ("'" + str(transactions[idx][field]) + "',")
        insert_script += "'" + str(price_ids[idx]) + "')"
        cursor.execute(insert_script)
        connection.commit()
