// Number of securities of different sectors
db.CourseworkOne.aggregate({$group : {_id : "$StaticData.GICSSector", num_securities : {$sum : 1}}})

// Average Beta of different sectors
db.CourseworkOne.aggregate({$group : {_id : "$StaticData.GICSSector", avg_beta : {$avg: "$MarketData.Beta"}}})

// Find all securities with DividendYield greater than 0, Beta greater than 1.5, and (PERatio greater than 30 or PayoutRatio less than 45)
db.CourseworkOne.find({"FinancialRatios.DividendYield" : {"$gte" : 0}, "MarketData.Beta" : {"$gte" : 1.5}, $or : [{"FinancialRatios.PERatio" : {"$gte" : 30}}, {"FinancialRatios.PayoutRatio": {"lte": 45}}]}).pretty()

// Return the avg_PERatio, avg_PayoutRatio and the number of securities for those securities whose DividendYield greater than 1 
db.CourseworkOne.aggregate([{$match : {"FinancialRatios.DividendYield" : {"$gte" : 1}}},{$group : {_id : "$StaticData.GICSSector", avg_pe : {$avg: "$FinancialRatios.PERatio"}, avg_payout : {$avg : "$FinancialRatios.PayoutRatio"}, num : {$sum : 1}}}])