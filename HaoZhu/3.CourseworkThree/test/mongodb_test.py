from modules.db.connection import MongoDB
if __name__ == '__main__':
    test_client = MongoDB('localhost', 27017,'Test',None)
    test_client.change_collection('test')
    test_client.add_data({'name':'NaN', 'info':'Not a Number'})
    print(test_client.find_data({'name':'NaN'}))
    test_client.del_many({'name':'NaN'})
    print(test_client.find_data({'name':'NaN'}))
    test_client.close()
