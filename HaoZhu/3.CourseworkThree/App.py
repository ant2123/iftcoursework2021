from configparser import ConfigParser
from fastapi import FastAPI
from modules.verify.verify import Verify
from modules.db.connection import MongoDB
from modules.db.load_mongo import load_mongo
from static.DataItem import TradeItem
import uvicorn

load_mongo()
# Load MongoDB
config = ConfigParser()

config.read('./AntonyNjengwe/CourseworkThree/config/script.config')
db_mongo = config.get('nosql', 'mongodb')
collection = config.get('nosql', 'collection')
url = config.get('nosql', 'url')
port = config.get('nosql','port')

sql_path = config.get('sql','sqldb_path')
print(db_mongo)
print(collection)
# Initialize the client
mongodb = MongoDB(host=url, port=int(port), database=db_mongo, collection=collection)
    
app = FastAPI()

@app.post('/submit')
def submit(item:TradeItem):
    tool = Verify(sql_path,mongodb)
    result = tool.verify(item)
    if result == True:
        mongodb.add_data(item)
        return {'code':200,'msg':'Success.'}
    else:
        return result
        

if __name__ == '__main__':
    uvicorn.run(app='App:app', host="127.0.0.1", port=8000, reload=True, debug=True)