import imp
import sqlite3
from modules.db.connection import MongoDB
from typing import Union
from static.DataItem import TradeItem
import time 

class Verify:
    '''
    Design to ensure the data do not contains wrong information.
    '''
    __connection = None
    __cursor = None
    __mongodb_client = None
    __symbol_list = []

    def __init__(self, path: str, client: MongoDB) -> None:
        '''
        Initialize the class and specify database path and MongoDB client.
        '''
        self.__connection = sqlite3.connect(path)
        self.__cursor = self.__connection.cursor()
        cursor = self.__cursor.execute("SELECT symbol_id FROM equity_prices")
        self.__symbol_list = list(set([i[0] for i in cursor]))

        self.__mongodb_client = client

    def verify(self, data: TradeItem) -> Union[bool, str]:
        '''
        Check the data.
        '''
        if data.TradeType == 'BUY' or data.TradeType == 'SELL':
            if data.Ccy == 'USD':
                if data.Symbol in self.__symbol_list:

                    if (data.TradeType == 'SELL' and data.Notional < 0 and data.Quantity < 0) or (data.TradeType == 'BUY' and data.Notional > 0 and data.Quantity > 0):
                        temp = [x['TradeId'] for x in self.__mongodb_client.find_data({},{'TradeId':1})]                 
                        if data.TradeId not in temp :
                            if data.DateTime[:3] == 'ISO' or self.__datetime_verify(data.DateTime):
                                return True
                            else:
                                return 'Wrong trade time.'
                        else:
                            return 'Wrong trade id.'
                    else:
                        return 'Wrong price or quantity.'
                else:
                    return 'Wrong trade sympol.'
            else:
                return 'Wrong ccy.'
        else:
            return 'Wrong trade type.'

    def __datetime_verify(self,date):
        """Check the date."""
        try:
            if ":" in date:
                time.strptime(date, "%Y-%m-%d %H:%M:%S")
            else:
                time.strptime(date, "%Y-%m-%d")
            return True
        except Exception as e:
            print(e)
            return False
