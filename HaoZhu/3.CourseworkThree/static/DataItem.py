
from pydantic import BaseModel

class TradeItem(BaseModel):
    '''
    Item for transmitting trade data.
    '''
    DateTime:str
    TradeId :str
    Trader :str
    Symbol :str
    Quantity :int 
    Notional :float
    TradeType : str
    Ccy :str
    Counterparty :str
    
