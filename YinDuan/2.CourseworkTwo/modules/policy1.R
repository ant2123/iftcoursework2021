#--------------------------------------------------------------------------------------
# Coursework2
# Author  : Yin Duan
# Topic   : Policy1
#--------------------------------------------------------------------------------------

#Long
policy_breaches_long <- as.data.frame(matrix(nrow=0,ncol = 7))
for (j in 1:nrow(Data_1112)) {
  if((Data_1112$Quantity[j]>0)&(Data_1112$amount[j]>=LimitPolicy('long',Data_1112$Trader[j]))){
    policy_breaches_long <- rbind(policy_breaches_long,Data_1112[j,])
  }
}
#Short
policy_breaches_short <- as.data.frame(matrix(nrow=0,ncol = 7))
for (j in 1:nrow(Data_1112)) {
  if((Data_1112$Quantity[j]<=0)&(abs(Data_1112$Notional[j])>=LimitPolicy('short',Data_1112$Trader[j]))){
    policy_breaches_short <- rbind(policy_breaches_short,Data_1112[j,])
  }
}