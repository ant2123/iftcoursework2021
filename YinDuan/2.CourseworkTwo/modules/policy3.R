#--------------------------------------------------------------------------------------
# Coursework2
# Author  : Yin Duan
# Topic   : Policy3
#--------------------------------------------------------------------------------------

#VaR
#Calculate the VaR of portfolios on 2021-11-11
startdate <- as.Date('2021-08-30')
enddate <- as.Date('2021-11-11')
data11 <- filter(Data_1112,Data_1112$Date=='2021-11-11')
EP11 <- filter(Equity_Price,Equity_Price$cob_date>=startdate,Equity_Price$cob_date<=enddate,Equity_Price$symbol_id%in%data11$Symbol)
#VaR limits can be applied to only two traders DMZ1796 and JBX1566
#DMZ1796
EP11_DMZ1796 <- EP11
for (i in 1:nrow(EP11_DMZ1796)) {
  if(EP11_DMZ1796$symbol_id[i] %in% data11[which(data11$Trader=='DMZ1796'),]$Symbol){
    quantity <- filter(data11,data11$Symbol==EP11_DMZ1796$symbol_id[i],data11$Trader=='DMZ1796')$Quantity
    EP11_DMZ1796$position[i] <- EP11_DMZ1796$close[i]*quantity
  }else{
    EP11_DMZ1796$position[i] <-0
  }
}
EP11_DMZ1796<- filter(EP11_DMZ1796,EP11_DMZ1796$position!=0)
data11_DMZ1796 <- left_join(EP11_DMZ1796,data11[which(data11$Trader=='DMZ1796'),],by=c("symbol_id"="Symbol"))%>%select(Trader,symbol_id,close,cob_date,Quantity,Notional,position)
data11_DMZ1796 <- aggregate(data11_DMZ1796[c('Quantity','position')],by=data11_DMZ1796[c('Trader','cob_date')],FUN=sum)
return <-rep(0,53)
data11_DMZ1796 <- cbind(data11_DMZ1796,return)
for (j in 4:nrow(data11_DMZ1796)) {
    data11_DMZ1796$return[j] <- data11_DMZ1796$position[j]/data11_DMZ1796$position[j-3]-1
}
data11_DMZ1796 <- filter(data11_DMZ1796,data11_DMZ1796$return!=0)

#JBX1566
EP11_JBX1566 <- EP11
for (i in 1:nrow(EP11_JBX1566)) {
  if(EP11_JBX1566$symbol_id[i] %in% data11[which(data11$Trader=='JBX1566'),]$Symbol){
    quantity <- filter(data11,data11$Symbol==EP11_JBX1566$symbol_id[i],data11$Trader=='JBX1566')$Quantity
    EP11_JBX1566$position[i] <- EP11_JBX1566$close[i]*quantity
  }else{
    EP11_JBX1566$position[i] <-0
  }
}
EP11_JBX1566<- filter(EP11_JBX1566,EP11_JBX1566$position!=0)
data11_JBX1566 <- left_join(EP11_JBX1566,data11[which(data11$Trader=='JBX1566'),],by=c("symbol_id"="Symbol"))%>%select(Trader,symbol_id,close,cob_date,Quantity,Notional,position)
data11_JBX1566 <- aggregate(data11_JBX1566[c('Quantity','position')],by=data11_JBX1566[c('Trader','cob_date')],FUN=sum)
data11_JBX1566 <- cbind(data11_JBX1566,return)
for (j in 4:nrow(data11_JBX1566)) {
  data11_JBX1566$return[j] <- data11_JBX1566$position[j]/data11_JBX1566$position[j-3]-1
}
data11_JBX1566 <- filter(data11_JBX1566,data11_JBX1566$return!=0)

#Calculate the VaR of portfolios on 2021-11-12
startdate1 <- as.Date('2021-08-31')
enddate1 <- as.Date('2021-11-12')
data12 <- filter(Data_1112,Data_1112$Date=='2021-11-12')
EP12 <- filter(Equity_Price,Equity_Price$cob_date>=startdate1,Equity_Price$cob_date<=enddate1,Equity_Price$symbol_id%in%data12$Symbol)
#DMZ1796
EP12_DMZ1796 <- EP12
for (i in 1:nrow(EP12_DMZ1796)) {
  if(EP12_DMZ1796$symbol_id[i] %in% data12[which(data12$Trader=='DMZ1796'),]$Symbol){
    quantity <- filter(data12,data12$Symbol==EP12_DMZ1796$symbol_id[i],data12$Trader=='DMZ1796')$Quantity
    EP12_DMZ1796$position[i] <- EP12_DMZ1796$close[i]*quantity
  }else{
    EP12_DMZ1796$position[i] <-0
  }
}
EP12_DMZ1796<- filter(EP12_DMZ1796,EP12_DMZ1796$position!=0)
data12_DMZ1796 <- left_join(EP12_DMZ1796,data12[which(data12$Trader=='DMZ1796'),],by=c("symbol_id"="Symbol"))%>%select(Trader,symbol_id,close,cob_date,Quantity,Notional,position)
data12_DMZ1796 <- aggregate(data12_DMZ1796[c('Quantity','position')],by=data12_DMZ1796[c('Trader','cob_date')],FUN=sum)
data12_DMZ1796 <- cbind(data12_DMZ1796,return)
for (j in 4:nrow(data12_DMZ1796)) {
  data12_DMZ1796$return[j] <- data12_DMZ1796$position[j]/data12_DMZ1796$position[j-3]-1
}
data12_DMZ1796 <- filter(data12_DMZ1796,data12_DMZ1796$return!=0)

#JBX1566
EP12_JBX1566 <- EP12
for (i in 1:nrow(EP12_JBX1566)) {
  if(EP12_JBX1566$symbol_id[i] %in% data12[which(data12$Trader=='JBX1566'),]$Symbol){
    quantity <- filter(data12,data12$Symbol==EP12_JBX1566$symbol_id[i],data12$Trader=='JBX1566')$Quantity
    EP12_JBX1566$position[i] <- EP12_JBX1566$close[i]*quantity
  }else{
    EP12_JBX1566$position[i] <-0
  }
}
EP12_JBX1566<- filter(EP12_JBX1566,EP12_JBX1566$position!=0)
data12_JBX1566 <- left_join(EP12_JBX1566,data12[which(data12$Trader=='JBX1566'),],by=c("symbol_id"="Symbol"))%>%select(Trader,symbol_id,close,cob_date,Quantity,Notional,position)
data12_JBX1566 <- aggregate(data12_JBX1566[c('Quantity','position')],by=data12_JBX1566[c('Trader','cob_date')],FUN=sum)
data12_JBX1566 <- cbind(data12_JBX1566,return)
for (j in 4:nrow(data12_JBX1566)) {
  data12_JBX1566$return[j] <- data12_JBX1566$position[j]/data12_JBX1566$position[j-3]-1
}
data12_JBX1566 <- filter(data12_JBX1566,data12_JBX1566$return!=0)
