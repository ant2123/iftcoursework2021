#--------------------------------------------------------------------------------------
# Coursework2
# Author  : Yin Duan
# Topic   : Policy2
#--------------------------------------------------------------------------------------

#Sector relative
Data1 <- left_join(Data_1112,Equity_Static,by=c('Symbol'='symbol'))%>%select(Trader,Symbol,Ccy,Date,Quantity,Notional,amount,GICSSector)
Trader_A <- aggregate(Data1[c('Quantity','Notional','amount')],by=Data1[c('Date','Trader')],FUN=sum)
Sector_A <- aggregate(Data1[c('Quantity','Notional','amount')],by=Data1[c('Date','Trader','GICSSector')],FUN=sum)
policy_breaches_sector<- as.data.frame(matrix(nrow=0,ncol = 4))
for (k in 1:nrow(Sector_A)) {
  wholeposition <- filter(Trader_A,Trader_A$Trader==Sector_A$Trader[k],Trader_A$Date==Sector_A$Date[k])$amount
  if(Sector_A$amount[k]/wholeposition>=LimitPolicy('sector',Sector_A$Trader[k])){
    policy_breaches_sector <- rbind(policy_breaches_sector,Sector_A[k,])
  }
}


