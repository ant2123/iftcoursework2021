#%%
from importlib.abc import FileLoader
from jinja2 import Environment, FileSystemLoader
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
import modules.graph.Graph as gg
from modules.analysis.Beta import portfolio_beta


fileLoader = FileSystemLoader('templates')
env = Environment(loader= fileLoader)


rendered = env.get_template('chart3.html').render(graphJSON = gg.graph_risk_vs_return(), graphJSON2 = gg.graph_portfolio_vs_benchmark(), graphJSON3 = gg.graph_portfolio_allocation(), Beta = gg.beta(), Expected_return = gg.total_expected_return())
file_name = 'Home.html'


if __name__ == '__main__':
    
    with open(f'./test/{file_name}','w') as f:
        f.write(rendered)
# %%