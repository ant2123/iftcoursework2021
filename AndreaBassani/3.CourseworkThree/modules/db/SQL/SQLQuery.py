#%%
import pandas as pd 
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from SQL.SQLConnection import SQL_connection
from utility.Utility import get_config

def SQL_query_equity_static(): 
    '''Create a table list whith all the data from equity_static with the following columns:
    ['Symbol', 'Security', 'GICSSector', 'GICIndustry'] '''
    con = SQL_connection()
    cur = con.cursor()
    #Query the database
    query = f"SELECT * FROM equity_static"
    table_list = [a for a in cur.execute(query)]
    table_list = pd.DataFrame(list(table_list), columns =['Symbol', 'Security','GICSSector','GICIndustry'])
    #closing connection
    con.close()
    return table_list

def SQL_query_equity_prices(): 
    '''Create a table list whith all the data from equity_prices with the following columns:
    ['price_id', 'open','high','low', 'close','volume','currency','cob_date','symbol_id']
    date format: yyy-mm-dd '''
    con = SQL_connection()
    cur = con.cursor()
    query = f"SELECT * FROM equity_prices"
    table_list = [a for a in cur.execute(query)]
    table_list = pd.DataFrame(list(table_list), columns =['price_id', 'open','high','low', 'close','volume','currency','cob_date','symbol_id'])
    table_list['cob_date'] = pd.to_datetime(table_list['cob_date'], format='%d-%b-%Y')
    return table_list

def SQL_query_trader_static (): 
    '''Create a table list whith all the data from equity_prices with the following columns:
    ['price_id', 'open','high','low', 'close','volume','currency','cob_date','symbol_id']
    date format: yyy-mm-dd '''
    con = SQL_connection()
    cur = con.cursor()
    query = f"SELECT * FROM trader_static"
    table_list = [a for a in cur.execute(query)]
    table_list = pd.DataFrame(list(table_list), columns =['trader_id', 'trader_name','fund_name','fund_type', 'fund_focus','funs_currency','is_active','golive_date','termination_date'])
    trader_list = table_list.loc[table_list['trader_id'] == get_config()[5]]
    return trader_list

#SQL_query_equity_static()
#SQL_query_equityPrices()
# %%
