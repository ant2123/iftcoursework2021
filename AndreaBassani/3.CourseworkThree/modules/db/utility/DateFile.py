#%%
import sys
import datetime
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from dateconvertor.DateConvertor import dateConvertor_mongo
from utility.Utility import get_config

def check_data_format(number):
    format = '%Y-%m-%d'
    date = get_config()[number]
    try:
        datetime.datetime.strptime(date, format)
        return date
    except ValueError:
        print("This is the incorrect date string format. It should be YYYY-MM-DD")

def end_date():
    date = check_data_format(4)
    return date 


# %%
