#%%
from pymongo import MongoClient
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from utility.Utility import get_config

def MongoDB_connection():
    """connection_path:'mongodb://localhost'
       database_name:'Equity'
       collection_name:'CourseworkTwo' """
    con = MongoClient(get_config()[1])
    db = con[get_config()[2]]
    col = db[get_config()[3]]
    return col 
MongoDB_connection()

# %%
