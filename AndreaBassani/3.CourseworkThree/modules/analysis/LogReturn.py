#%%
import numpy as np 
import pandas as pd
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from analysis.SimpleReturn import simple_return
import analysis.ComulativeReturn as ac
import matplotlib.pyplot as plt
import analysis.ExpectedReturn as e
from db.SQL.SQLQuery import SQL_query_equity_static 
from db.utility.DateFile import end_date
from db.utility.Utility import get_config
from db.MongoDB.MongoDBQuery import start_date

traderId = get_config()[5]
enddate = end_date()
startdate =start_date()

def log_return():
    stock_prices = simple_return()[1]
    stock_log_return = np.log(stock_prices / stock_prices.shift(1))
    stock_log_return.fillna(0, inplace=True)
    return stock_log_return[1:]

def stock_volatility():
    log_return_table = log_return()
    stock_std = pd.DataFrame(np.std(log_return_table).dropna())
    stock_std = stock_std.iloc[1:]*252**0.5
    return stock_std

def portfolio_volatility(): 
    returns =  log_return()[ac.portfolio_comulative_weight().iloc[:,1:].columns.values]
    portfolio = np.array([ac.portfolio_comulative_weight().iloc[-1,1:]]).astype(np.float64)
    covariance = np.cov(returns.fillna(0).T)
    portfolio_volatility = np.sqrt(np.dot(np.dot(portfolio, covariance), portfolio.T))
    return portfolio_volatility

def risk_vs_return():
    equity_static =  SQL_query_equity_static()
    Z = pd.DataFrame(ac.portfolio_comulative_weight().iloc[-1,1:]).astype(np.float64)*10000
    Y = pd.DataFrame(log_return()[ac.portfolio_comulative_weight().iloc[:,1:].columns.values].sum())*100
    X = stock_volatility()*100
    df = pd.concat([X,Y,Z], axis = 1, join='inner').reset_index()
    df1 = df.set_axis(['Symbol','Stock_Volatility','Return','Portfolio_Weight'], axis=1, inplace=False)
    df1 = df1.merge(equity_static[['GICSSector','Symbol']], left_on='Symbol', right_on='Symbol', how ='left')
    #deus = e.portfolio_expected_returns ('2020-11-11', '2021-11-11', 'JBX1566')
    #deus = deus.merge(df1.reset_index(), left_on='Symbol', right_on='index')
    return df1



# %%
