#%%
import pandas as pd
import sys
import numpy as np
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from db.SQL.SQLQuery import SQL_query_equity_prices

#Formula applied .ptc_change() = xt-xt-1/xt-1

def simple_return():
    stock_prices = SQL_query_equity_prices()
    stock_prices = stock_prices.pivot_table(index=['cob_date'], columns = 'symbol_id', values = ['close'],
    fill_value=0)
    stock_prices.columns = [col[1] for col in stock_prices.columns.values]
    stock_daily_returns = stock_prices.pct_change()
    stock_daily_returns = stock_daily_returns[1:]
    stock_daily_returns.replace([np.inf, -np.inf], np.nan, inplace=True)
    return stock_daily_returns, stock_prices


# %%
