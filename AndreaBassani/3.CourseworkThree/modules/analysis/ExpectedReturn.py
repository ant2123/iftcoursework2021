#%%
import pandas as pd
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from db.MongoDB.MongoDBQuery import MongoDB_documents_btwDates
from db.SQL.SQLQuery import SQL_query_equity_static, SQL_query_equity_prices
from analysis.SimpleReturn import simple_return
from db.utility.DateFile import end_date
from db.utility.Utility import get_config
from db.MongoDB.MongoDBQuery import start_date

traderId = get_config()[5]
enddate = end_date()
startdate =start_date()

def portfolio_allocations():
    trades = MongoDB_documents_btwDates(enddate, traderId)
    equity_static = SQL_query_equity_static()
    portfolio = trades.merge(equity_static, left_on='Symbol', right_on='Symbol').groupby('GICSSector', as_index = False).agg(Notional = ('Notional','sum'))
    portfolio['allocation'] = portfolio['Notional']/portfolio['Notional'].sum()
    #labels = portfolio['GICSSector']
    #data = portfolio['Notional']
    return portfolio

def daily_trades():
    trades = MongoDB_documents_btwDates(enddate, traderId)
    trades_history = trades.groupby('DateTime', as_index=False).agg(Notional = ('Notional','sum'))
    return trades_history['DateTime'], trades_history['Notional'],trades.groupby(['DateTime','Symbol'], as_index=False).sum()


def portfolio_expected_returns():
    stock_daily_returns = simple_return()[0]
    #Annualised daily return per stock
    #[(daily return+1)^365-1]
    average_return = pd.DataFrame([(stock_daily_returns.mean()+1)**252-1]).transpose().reset_index()
    average_return = average_return.set_axis(['symbol', 'annual_return'], axis=1, inplace=False)
    positions_endDay = daily_trades()[2]
    positions_endDay = positions_endDay.loc[positions_endDay['DateTime'] == str(enddate)]
    positions_endDay['weight'] = positions_endDay['Notional']/positions_endDay['Notional'].sum()
    average_return_portfolio = positions_endDay.merge(average_return, left_on='Symbol', right_on='symbol')
    return average_return_portfolio

#xxx = portfolio_expected_returns ('2020-11-11', '2021-11-11', traderId)

# %%