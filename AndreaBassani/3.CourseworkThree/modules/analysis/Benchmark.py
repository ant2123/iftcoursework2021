#%%
import yfinance as yf
import datetime as datetime
from dateutil import parser
import pandas as pd
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from db.utility.DateFile import end_date
from db.utility.Utility import get_config
from db.MongoDB.MongoDBQuery import start_date

traderId = get_config()[5]
enddate = parser.parse(end_date())
startdate = parser.parse(start_date())

def get_benchmark(benchmark, start, end):
    benchmark = get_data(benchmark, start, end)
    benchmark = benchmark.drop(['symbol'], axis=1)
    benchmark.reset_index(inplace=True)
    return benchmark

def get_data(stocks, start, end):
    def data(ticker):
        df = yf.download(ticker, start=start, end=(end + datetime.timedelta(days=1)))
        df['symbol'] = ticker
        df.index = pd.to_datetime(df.index)
        return df
    datas = map(data, stocks)
    return(pd.concat(datas, keys=stocks, names=['Ticker', 'Date'], sort=True))

def daily_change_benchark():
    daily_benchmark = get_benchmark(['SPY'], startdate , enddate)
    daily_benchmark_return = daily_benchmark['Adj Close'].pct_change()
    return daily_benchmark_return, daily_benchmark

def comulative_benchmark_return():
    daily_benchmark_return, daily_benchmark= daily_change_benchark()
    daily_benchmark_return_comm = (1 + daily_benchmark_return).cumprod()-1
    daily_benchmark_return_comm = pd.DataFrame(daily_benchmark_return_comm)
    daily_benchmark_return_comm['Date'] = daily_benchmark['Date']
    return daily_benchmark_return_comm[1:]




# %%
