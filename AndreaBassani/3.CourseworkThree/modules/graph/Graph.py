#%%
import plotly.express as px
import plotly
import json
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from analysis.ExpectedReturn import portfolio_allocations, portfolio_expected_returns
from analysis.ComulativeReturn import portfolio_vs_benchmark
from analysis.LogReturn import risk_vs_return
from analysis.ComulativeReturn import portfolio_vs_benchmark
from analysis.Beta import portfolio_beta


def graph_risk_vs_return ():

    data = risk_vs_return().query(' Return >= -0.8')
    fig = px.scatter(data, x='Stock_Volatility',y='Return', hover_name ='Symbol', color='GICSSector',title='Stocks Returns Vs Risk', size='Portfolio_Weight')
    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    return graphJSON 


def graph_portfolio_vs_benchmark():

    data2 = portfolio_vs_benchmark()
    fig2 = px.line(data2, x=data2['Date'], y=data2.columns[1:], title='Cumulative return portfolio Vs Benchmark')
    graphJSON2 = json.dumps(fig2, cls=plotly.utils.PlotlyJSONEncoder)
    return graphJSON2


def graph_portfolio_allocation():

    data3 = portfolio_allocations()
    fig3 = px.pie(data3, values='allocation' , names='GICSSector', title='Daily portfolio allocation')
    graphJSON3 = json.dumps(fig3, cls=plotly.utils.PlotlyJSONEncoder)
    return graphJSON3

def total_expected_return():
    returns = portfolio_expected_returns()
    expected_return = (returns['weight']*returns['annual_return']).sum()
    return round(expected_return, 2 )

def beta():
    value = round(portfolio_beta()[0], 2)
    return value 
# %%
