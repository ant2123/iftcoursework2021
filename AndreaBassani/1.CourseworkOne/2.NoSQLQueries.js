// Find the average Beta and PERatio for each sector

db.CourseworkOne.aggregate([{$match:{}},{$group:{_id:"$StaticData.GICSSector",averageBeta:{$avg:"$FinancialRatios.DividendYield"}, averagePERatio:{$avg:"$FinancialRatios.PERatio"}}}]).sort({"averageBeta":-1})

// Generate a buy signal for all the companies that have a PERatio lower than the average but not lower or equal to 0, or sell for companies higher than the average

var averageGlobalPE = db.CourseworkOne.aggregate([{$group:{_id:"_id",averagePERatio:{$avg:"$FinancialRatios.PERatio"}}}]).toArray()[0]["averagePERatio"]

db.CourseworkOne.aggregate([{$match:{ "$and":[{"FinancialRatios.PERatio":{"$nin":["NA",null]}}, {"FinancialRatios.PERatio":{"$gt":0}}]}},{$project:{_id:0,"StaticData.Security":1,"FinancialRatios.PERatio":1,signal:{$cond:{if:{$gte:["$FinancialRatios.PERatio",averageGlobalPE]},then: "Sell", else: "Buy"}}}}]).sort({"FinancialRatios.PERatio":1})db