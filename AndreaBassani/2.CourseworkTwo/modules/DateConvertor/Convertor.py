import datetime

def dateConvertor(date):
    #ISODate first element on the array is the start the second the end
    ISODate = [f'ISODate({date.year}-{date.month}-{date.day}T00:00:00.000Z)',                f'ISODate({date.year}-{date.month}-{date.day}T23:59:59.000Z)']
    #SQLDate first element is for query and the second is for the TradeId
    SQLDate = [f'{date.day}-{date.strftime("%b")}-{date.year}',f'{date.year}{date.month}{date.day}']
    return ISODate, SQLDate