import modules.DateConvertor.Convertor as dc
import modules.db.MongoDB.Connection as mc 
import pandas as pd

def MongoDB_documents_btwDates(date):
    """connection_path:'mongodb://localhost'
       database_name:'Equity'
       collection_name:'CourseworkTwo'
       date format: "ISODate(2021-11-11T00:00:00.000Z)" """
    conv_date = dc.dateConvertor(date)
    col = mc.MongoDB_connection()   
    #Find the data for the selected period
    documents = col.find({"DateTime":{"$gt":conv_date[0][0],"$lt":conv_date[0][1]}}) 
    documents = pd.DataFrame(documents, columns =['DateTime','TradeId','Trader','Symbol','Quantity','Notional','TradeType','Ccy', 'Counterparty'])
    return documents