import sqlite3
from sqlite3 import Error 
import modules.db.SQL.Connection as mc

def SQL_create_table(dbCreatetable):
    
    con = mc.SQL_connection()
    cur = con.cursor()
    new_table = cur.execute(dbCreatetable)
    con.commit()
    con.close()
    return 

