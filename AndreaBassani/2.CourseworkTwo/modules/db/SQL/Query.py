import pandas as pd
import modules.db.SQL.Connection as mc
import modules.DateConvertor.Convertor as dc

def SQL_query(date): 
    """query: 'SELECT * FROM equity_static'"""
    conv_date = dc.dateConvertor(date)
    con = mc.SQL_connection()
    cur = con.cursor()
    #Query the database
    query = f"SELECT * FROM equity_prices WHERE cob_date LIKE '%{conv_date[1][0]}%'"
    table_list = [a for a in cur.execute(query)]
    table_list = pd.DataFrame(list(table_list))
    #closing connection
    con.close()
    return table_list 