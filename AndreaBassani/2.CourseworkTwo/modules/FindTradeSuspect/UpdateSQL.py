import modules.FindTradeSuspect.DetectSuspect as fd
import modules.db.SQL.Insert as si
import modules.db.SQL.CreateTable as sct

def updateTradeSuspect(date):
    dbCreatetable = 'CREATE TABLE IF NOT EXISTS trade_subspect (pos_id TEXT PRIMARY KEY, cob_date TEXT, trader TEXT, symbol TEXT, ccy TEXT, net_quantity INTEGER, net_amount INTEGER, traded_share_price INTEGER);'
    sct.SQL_create_table(dbCreatetable)
    record_count = 0
    anomaly_trades = fd.tradeVsPrice(date)   
    for i in range(len(anomaly_trades)):
        try:
            new_entry = f'INSERT INTO trade_subspect (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount, traded_share_price)\
                        VALUES ("{anomaly_trades.loc[i,"pos_id"]}",\
                                "{anomaly_trades.loc[i,"cob_date"]}",\
                                "{anomaly_trades.loc[i,"Trader"]}",\
                                "{anomaly_trades.loc[i,"Symbol"]}",\
                                "{anomaly_trades.loc[i,"Ccy"]}",\
                                 {anomaly_trades.loc[i,"Quantity"]},\
                                 {anomaly_trades.loc[i,"Notional"]},\
                                 {anomaly_trades.loc[i,"TradedSharePrice"]})'
            si.SQL_insert(new_entry)
            record_count += 1
        except:
            print(f'Trade: {anomaly_trades.loc[i,"pos_id"]} already recorded')
    return record_count


