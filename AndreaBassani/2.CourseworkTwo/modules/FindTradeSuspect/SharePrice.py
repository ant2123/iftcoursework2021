import modules.db.MongoDB.Query as mq
import pandas

def share_price_per_trade(date):
    original_table = mq.MongoDB_documents_btwDates(date)
    share_price_trade = original_table[['DateTime', 'Trader', 'Symbol', 'Quantity', 'Notional', 'TradeId', 'Ccy']]
    share_price_trade.insert(3, "TradedSharePrice", original_table.Notional/original_table.Quantity)
    return share_price_trade

