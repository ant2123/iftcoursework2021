# UCL IFT - Big Data 2021

## Guide on how to start the detection of the errors and update the portfolio_position table for the selected dates 
This script will perform the following task:

1. Retrieve all trades for the dates inputted in the params file, multiple date are accettables. As a default is set to the dates: 11-11-2021 and 12-11-2021  
2. Use trade vs price method to find errors on the records of the trades
3. Create and update the trade_suspect table
4. Update the portfolio_positions table with the trades for the selected days 

To trigger this script please update the params.py in config and from the command line, please:

open the command line from the:
cd .\iftcoursework2021\AndreaBassani\2.CourseworkTwo 
and run the code:
python3.9 Main.py 

Please note that python3.9  need to be changed based on the python version of the device 

This can be also be run in any IDE by runing the Main.py file