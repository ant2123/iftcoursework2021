#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Giulio Pelosio
# Coursework Three : Incorrect Trade Detection
#--------------------------------------------------------------------------------------

#%%
# 1) import libraries------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

import pandas as pd
import numpy as np
from lib2to3.pygram import Symbols
from os import sep
from sqlite3 import Date
from turtle import title
from winsound import SND_ASYNC
import matplotlib.pyplot as plt
from pymongo import MongoClient
from sklearn.cluster import KMeans
from sqlalchemy import create_engine
from matplotlib import pyplot as plt
import plotly.express as px
import io
from base64 import b64encode
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from datetime import datetime as dt 
import plotly.graph_objects as go
import yfinance as yf  
import pandas_market_calendars as mcal
from plotly.offline import init_notebook_mode, plot
init_notebook_mode(connected=True)
from sklearn.linear_model import LinearRegression
import pandas_datareader as web
from scipy import stats
import seaborn as sns
import datapane as dp
import ffn 

#%%
# 2.0) Import data

#-------------------------------------------CONNECTIONS-----------------------------------------------------------------------------------------------------


# 2.1) Connect MongoDB ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
con = MongoClient('mongodb+srv://giuliopelosio:<password>@cluster0.rzdlv.mongodb.net/myFirstDatabase?authSource=admin&replicaSet=atlas-3v4kka-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true')
db = con.Equity
col = db.CourseworkTwo


# 2.2) Connect SQL ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GITRepoDirectory = "C:/Users/Giulio/Desktop/Phyton UCL/LUCA/Second course work/data right"
engine = create_engine(f"sqlite:///C:/Users/Giulio/Desktop/Phyton UCL/LUCA/Second course work/data right/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()


#----------------------------------------DEFINE VARIABLES-----------------------------------------------------------------------------------------------------

Select_TraderID = "DGR1983"

From_date = "2020-11-11"
Too_date =  "2021-11-13"

Date_Range = 'From: ' + From_date + ' Too: ' + Too_date

Date_Range


#------------------------------------------IMPORT DATA-----------------------------------------------------------------------------------------------------------

#IMPORT DATA FROM YAHOO FINANCE FOR SP500


spy_ohlc_df = yf.download('SPY', start= From_date , end= Too_date)

spy_ohlc_df



#SELECT DATATES RANGE AND TRADER FROM MONGO DB


Select_dates_trader_MongoDB = col.find({"$and": [ {"Trader": {"$eq":  Select_TraderID}}, {"DateTime": {"$gte": "ISODate("+From_date+"T00:00:00.000Z)"}}, {"DateTime": {"$lt": "ISODate("+Too_date+"T00:00:00.000Z)"}} ] } )

Mongo_DB_Selection = pd.DataFrame(Select_dates_trader_MongoDB) 

Mongo_DB_Selection

Mongo_DB_Selection['Date'] = Mongo_DB_Selection['DateTime'].map(lambda x: x[8:18])

Mongo_DB_Selection

# IMPORT EQUITY STATIC FROM SQL DB --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def SQL_ImportEquityStatic():
    return ("SELECT equity_static.symbol AS Symbol , equity_static.security, equity_static.GICSIndustry, equity_static.GICSSector FROM equity_static")

Equity_Static = pd.read_sql_query(SQL_ImportEquityStatic(),engine)

Equity_Static 


#IMPORT EQUITY PRICES FROM SQL ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def SQL_ImportEquityPrices():
    return ("SELECT equity_prices.price_id, equity_prices.price_id, equity_prices.open, equity_prices.high, equity_prices.low, equity_prices.close, equity_prices.volume, equity_prices.currency, equity_prices.cob_date, equity_prices.symbol_id AS Symbol FROM equity_prices")

Equity_prices = pd.read_sql_query(SQL_ImportEquityPrices(),engine)

Equity_prices["Date"] = Equity_prices.cob_date.apply(lambda x: dt.strptime(x, "%d-%b-%Y")).values



Date_Selection = (Equity_prices['Date'] >= From_date) & (Equity_prices['Date'] <= Too_date)
Equity_prices_df = Equity_prices.loc[Date_Selection]

Equity_prices_df


#IMPORT PORTFOLIO POSITION FROM SQL -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def SQL_ImportPortfolioPositions():
    return ("SELECT portfolio_positions.pos_id, portfolio_positions.cob_date, portfolio_positions.symbol AS Symbol, portfolio_positions.ccy, portfolio_positions.net_quantity, portfolio_positions.net_amount, portfolio_positions.trader AS trader_id FROM portfolio_positions WHERE trader = '"+Select_TraderID+"'")

Portfolio_Position = pd.read_sql_query(SQL_ImportPortfolioPositions(),engine)

Portfolio_Position["Date"] = Portfolio_Position.cob_date.apply(lambda x: dt.strptime(x, "%d-%b-%Y")).values

Portfolio_Position_df = Portfolio_Position.loc[Date_Selection]

Portfolio_Position_df 


#IMPORT TRADER STATIC FROM SQL -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def SQL_Trader_Static():
    return ("SELECT trader_name AS TRADER_NAME, fund_name, fund_type, fund_focus, trader_id  FROM trader_static WHERE trader_id = '"+Select_TraderID+"'")


Trader_Static = pd.read_sql_query(SQL_Trader_Static(),engine)


#CALCULATION--------------------------------------------------------------------------------------------------------------------------------------

#MERGE MONGO DB WITH EQUITY STATIC-----------------------------------------------------------------------------------------------------

Mongo_DB_Selection_SQL_Merger = pd.merge(Equity_Static , Mongo_DB_Selection , on=["Symbol"])


#CHANGE DATA FROM ISO TO NORMAL------------------------------------------------------------------------------------------------------------------------

Mongo_DB_Selection_SQL_Merger['DateTime'] = Mongo_DB_Selection_SQL_Merger['DateTime'].map(lambda x: x[8:18])


#---------------------------------TRADER ALLOCATION FOR INDUSTRY----------------------------------------------------------------- 


Allocation_by_Industry = Mongo_DB_Selection_SQL_Merger.groupby('GICSIndustry', as_index = False).agg({'Notional': 'sum'})
Allocation_by_Industry["Notional"] = Allocation_by_Industry["Notional"]/Allocation_by_Industry["Notional"].sum() 


#CHART 

Industry_Allocation_Chart = px.pie(Allocation_by_Industry, values='Notional', names='GICSIndustry',)
Industry_Allocation_Chart.update_traces(textposition='inside')
Industry_Allocation_Chart.update_layout(uniformtext_minsize=12, uniformtext_mode='hide')

# Allocation_by_Industry_Chart.show()


#Allocation_by_Industry_Chart.write_html("C:/Users/Giulio/Desktop/Phyton UCL/LUCA/Test sumb/iftcoursework2021/GiulioPelosio/2.CourseworkTwo/Allocation_by_Industry_Chart.html") 

#-----------------------------------TRADER ALLOCATION BY SECTOR----------------------------------------------------------------------

Allocation_by_Sector = Mongo_DB_Selection_SQL_Merger.groupby('GICSSector', as_index = False).agg({'Notional': 'sum'})
Allocation_by_Sector
Allocation_by_Sector["Notional"] = Allocation_by_Sector["Notional"]/Allocation_by_Sector["Notional"].sum()


#CHART 
Sector_Allocation_Chart = px.pie(Allocation_by_Sector, values='Notional', names='GICSSector',)
Sector_Allocation_Chart.update_traces(textposition='inside')
Sector_Allocation_Chart.update_layout(uniformtext_minsize=12, uniformtext_mode='hide')


#--------------------------------------ALLOCATION BY SYMBOL-------------------------

Notinal_Symbol = Mongo_DB_Selection_SQL_Merger.groupby('Symbol', as_index = False).agg({'Notional': 'sum'})
Notinal_Symbol["Notional"] = Allocation_by_Industry["Notional"]/Allocation_by_Industry["Notional"].sum() 

selected_rows = Notinal_Symbol[~Notinal_Symbol['Notional'].isnull()]

Symbol_Allocation_Chart  = px.bar(selected_rows, y='Notional', x='Symbol', text='Symbol')
Symbol_Allocation_Chart.update_traces(texttemplate='%{text:.2s}', textposition='outside')
Symbol_Allocation_Chart.update_layout(uniformtext_minsize=8, uniformtext_mode='hide')




#--------------------------------------PORTFOLIO RETURN-----------------------------------------------------------------

#------DEFINE FUNCTION: Grabbing the Data

def create_market_cal(start, end):
    nyse = mcal.get_calendar('NYSE')
    schedule = nyse.schedule(From_date, Too_date)
    market_cal = mcal.date_range(schedule, frequency='1D')
    market_cal = market_cal.tz_localize(None)
    market_cal = [i.replace(hour=0) for i in market_cal]
    return market_cal


#--------DEFINE VARIABLES 
####


Merging = pd.merge(Mongo_DB_Selection , Equity_prices_df , left_on=['Symbol'],right_on = ['Symbol'] , how = 'left')
Merging['1day % return']=((Merging['close']-Merging['open'])/Merging['open'])*100
Merging["Strategy_close"]=Merging["Notional"]*Merging["close"]
portfolio_df = Merging.iloc[:, [4,5,7,16,20]]
portfolio_df["Adj cost"]=portfolio_df["Quantity"]*portfolio_df["close"]
portfolio_df.rename(columns={'Date_y': 'Open date', 'Quantity': 'Qty', 'TradeType': 'Type', 'close': 'Adj cost per share'}, inplace=True)
symbols = portfolio_df.Symbol.unique()
spy_ohlc_df = yf.download('SPY', start= From_date , end= Too_date)
spy_ohlc_df.reset_index(level=0, inplace=True)
Equity_prices.rename(columns={'Date_y': 'Date', 'Symbol': 'Ticker',  'close': 'Close'}, inplace=True)
daily_adj_close =Equity_prices[['Ticker','Date', 'Close']]
daily_benchmark= spy_ohlc_df
daily_benchmark = daily_benchmark[['Date', 'Close']]
market_cal = create_market_cal(From_date, Too_date)


#------DEFINE FUNCTION: Finding our Initial Active Portfolio

def position_adjust(daily_positions, sale):
    stocks_with_sales = pd.DataFrame()
    buys_before_start = daily_positions[daily_positions['Type'] == 'BUY'].sort_values(by='Open date')
    for position in buys_before_start[buys_before_start['Symbol'] == sale[1]['Symbol']].iterrows():
        if position[1]['Qty'] <= sale[1]['Qty']:
            sale[1]['Qty'] -= position[1]['Qty']
            position[1]['Qty'] = 0
        else:
            position[1]['Qty'] -= sale[1]['Qty']
            sale[1]['Qty'] -= sale[1]['Qty']
        stocks_with_sales = stocks_with_sales.append(position[1])
    return stocks_with_sales


def portfolio_start_balance(portfolio, start_date):
    positions_before_start = portfolio[portfolio['Open date'] <= start_date]
    future_sales = portfolio[(portfolio['Open date'] >= start_date) & (portfolio['Type'] == 'SELL')]
    sales = positions_before_start[positions_before_start['Type'] =='SELL'].groupby(['Symbol'])['Qty'].sum()
    sales = sales.reset_index()
    positions_no_change = positions_before_start[~positions_before_start['Symbol'].isin(sales['Symbol'].unique())]
    adj_positions_df = pd.DataFrame()
    for sale in sales.iterrows():
        adj_positions = position_adjust(positions_before_start, sale)
        adj_positions_df = adj_positions_df.append(adj_positions)
    adj_positions_df = adj_positions_df.append(positions_no_change)
    adj_positions_df = adj_positions_df.append(future_sales)
    adj_positions_df = adj_positions_df[adj_positions_df['Qty'] > 0]
    return adj_positions_df


#------DEFINE FUNCTION: Creating Daily Performance Snapshots

def fifo(daily_positions, sales, date):
    sales = sales[sales['Open date'] == date]
    daily_positions = daily_positions[daily_positions['Open date'] <= date]
    positions_no_change = daily_positions[~daily_positions['Symbol'].isin(sales['Symbol'].unique())]
    adj_positions = pd.DataFrame()
    for sale in sales.iterrows():
        adj_positions = adj_positions.append(position_adjust(daily_positions, sale))
    adj_positions = adj_positions.append(positions_no_change)
    adj_positions = adj_positions[adj_positions['Qty'] > 0]
    return adj_positions


def time_fill(portfolio, market_cal):
    sales = portfolio[portfolio['Type'] == 'SELL'].groupby(['Symbol','Open date'])['Qty'].sum()
    sales = sales.reset_index()
    per_day_balance = []
    for date in market_cal:
        if (sales['Open date'] == date).any():
            portfolio = fifo(portfolio, sales, date)
        daily_positions = portfolio[portfolio['Open date'] <= date]
        daily_positions = daily_positions[daily_positions['Type'] == 'BUY']
        daily_positions['Date Snapshot'] = date
        per_day_balance.append(daily_positions)
    return per_day_balance


#------DEFINE FUNCTION:Making Portfolio Calculations

def modified_cost_per_share(portfolio, adj_close, start_date):
    df = pd.merge(portfolio, adj_close, left_on=['Date Snapshot', 'Symbol'],
                  right_on=['Date', 'Ticker'], how='left')
    df.rename(columns={'Close': 'Symbol Adj Close'}, inplace=True)
    df['Adj cost daily'] = df['Symbol Adj Close'] * df['Qty']
    df = df.drop(['Ticker', 'Date'], axis=1)
    return df


def benchmark_portfolio_calcs(portfolio, benchmark):
    portfolio = pd.merge(portfolio, benchmark, left_on=['Date Snapshot'],
                         right_on=['Date'], how='left')
    portfolio = portfolio.drop(['Date'], axis=1)
    portfolio.rename(columns={'Close': 'Benchmark Close'}, inplace=True)
    benchmark_max = benchmark[benchmark['Date'] == benchmark['Date'].max()]
    portfolio['Benchmark End Date Close'] = portfolio.apply(lambda x: benchmark_max['Close'], axis=1)
    benchmark_min = benchmark[benchmark['Date'] == benchmark['Date'].min()]
    portfolio['Benchmark Start Date Close'] = portfolio.apply(lambda x: benchmark_min['Close'], axis=1)
    return portfolio


def portfolio_end_of_year_stats(portfolio, adj_close_end):
    adj_close_end = adj_close_end[adj_close_end['Date'] == adj_close_end['Date'].max()]
    portfolio_end_data = pd.merge(portfolio, adj_close_end, left_on='Symbol',
                                  right_on='Ticker')
    portfolio_end_data.rename(columns={'Close': 'Ticker End Date Close'}, inplace=True)
    portfolio_end_data = portfolio_end_data.drop(['Ticker', 'Date'], axis=1)
    return portfolio_end_data


def portfolio_start_of_year_stats(portfolio, adj_close_start):
    adj_close_start = adj_close_start[adj_close_start['Date'] == adj_close_start['Date'].min()]
    portfolio_start = pd.merge(portfolio, adj_close_start[['Ticker', 'Close', 'Date']],
                                    left_on='Symbol', right_on='Ticker')
    portfolio_start.rename(columns={'Close': 'Ticker Start Date Close'}, inplace=True)
    portfolio_start['Adj cost per share'] = np.where(portfolio_start['Open date'] <= portfolio_start['Date'],
                                                          portfolio_start['Ticker Start Date Close'],
                                                          portfolio_start['Adj cost per share'])
    portfolio_start['Adj cost'] = portfolio_start['Adj cost per share'] * portfolio_start['Qty']
    portfolio_start = portfolio_start.drop(['Ticker', 'Date'], axis=1)
    portfolio_start['Equiv Benchmark Shares'] = portfolio_start['Adj cost'] / portfolio_start['Benchmark Start Date Close']
    portfolio_start['Benchmark Start Date Cost'] = portfolio_start['Equiv Benchmark Shares'] * portfolio_start['Benchmark Start Date Close']
    return portfolio_start


def calc_returns(portfolio):
    portfolio['Benchmark Return'] = portfolio['Benchmark Close'] / portfolio['Benchmark Start Date Close'] - 1
    portfolio['Ticker Return'] = portfolio['Symbol Adj Close'] / portfolio['Adj cost per share'] - 1
    portfolio['Ticker Share Value'] = portfolio['Qty'] * portfolio['Symbol Adj Close']
    portfolio['Benchmark Share Value'] = portfolio['Equiv Benchmark Shares'] * portfolio['Benchmark Close']
    portfolio['Abs Value Compare'] = portfolio['Ticker Share Value'] - portfolio['Benchmark Start Date Cost']
    portfolio['Abs Value Return'] = portfolio['Abs Value Compare']/portfolio['Benchmark Start Date Cost']
    portfolio['Stock Gain / (Loss)'] = portfolio['Ticker Share Value'] - portfolio['Adj cost']
    portfolio['Benchmark Gain / (Loss)'] = portfolio['Benchmark Share Value'] - portfolio['Adj cost']
    portfolio['Abs. Return Compare'] = portfolio['Ticker Return'] - portfolio['Benchmark Return']
    return portfolio


def per_day_portfolio_calcs(per_day_holdings, daily_benchmark, daily_adj_close, From_date):
    df = pd.concat(per_day_holdings, sort=True)
    mcps = modified_cost_per_share(df, daily_adj_close, From_date)
    bpc = benchmark_portfolio_calcs(mcps, daily_benchmark)
    pes = portfolio_end_of_year_stats(bpc, daily_adj_close)
    pss = portfolio_start_of_year_stats(pes, daily_adj_close)
    returns = calc_returns(pss)
    return returns




active_portfolio = portfolio_start_balance(portfolio_df, From_date)
positions_per_day = time_fill(active_portfolio, market_cal)
stocks_start = From_date
combined_df = per_day_portfolio_calcs(positions_per_day, daily_benchmark, daily_adj_close, stocks_start) 
active_portfolio = portfolio_start_balance(portfolio_df, stocks_start)
positions_per_day = time_fill(active_portfolio, market_cal)
combined_df = per_day_portfolio_calcs(positions_per_day, daily_benchmark, daily_adj_close, stocks_start) 




#--------------------------------------------------------------------DATA VISUALIZATION


#CHART 1----------------------------------------------------------


Combined_df_static = pd.merge(Equity_Static , combined_df, on=["Symbol"])

return_date = Combined_df_static.groupby(['Date Snapshot'], as_index = False).agg({'Ticker Return' :'sum', 'Benchmark Return': 'sum' })


#----------------SYMBOLS PERFORMANCE CHART---------------------------

Symbols_Returns = Combined_df_static.groupby(['Symbol', 'Date Snapshot'], as_index = False).agg({'Ticker Return' :'sum'})

Symbol_Performance_Chart = px.line(Symbols_Returns, x='Date Snapshot',
              y='Ticker Return', color='Symbol',
              title='Performance - Daily Simple Returns',
              labels={'daily_return_pct':'daily returns (%)'})

Symbol_Performance_Chart 


#----------------SECTOR PERFORMANCE CHART---------------------------

Sector_Returns = Combined_df_static.groupby(['GICSSector', 'Date Snapshot'], as_index = False).agg({'Ticker Return' :'sum'})

Sector_Performance_Chart = px.line(Sector_Returns, x='Date Snapshot',
              y='Ticker Return', color='GICSSector',
              title='Performance - Daily Simple Returns',
              labels={'daily_return_pct':'daily returns (%)'})



#----------------INDUSTRY PERFORMANCE CHART---------------------------

Industry_Returns = Combined_df_static.groupby(['GICSIndustry', 'Date Snapshot'], as_index = False).agg({'Ticker Return' :'sum'})

Industry_Performance_Chart = px.line(Industry_Returns, x='Date Snapshot',
              y='Ticker Return', color='GICSIndustry',
              title='Performance - Daily Simple Returns',
              labels={'daily_return_pct':'daily returns (%)'})


#------------------ PORTFOLIO VS BENCHMARK RETURN GRAPH--------------------------

Portofolio_vs_Benchmark_Chart = px.line(return_date, x="Date Snapshot", y= return_date.columns[1:])

#-----------------------LINEAR REGRESSION GRAPH


Linear_Regression_Chart = px.scatter(return_date, x="Benchmark Return", y="Ticker Return", trendline="ols")




#----------------------------------------- VOLATILITY 


price_change = combined_df[['Date Snapshot','Ticker Share Value']]
Date_agg_PortfolioReturn = price_change.groupby(['Date Snapshot'], as_index = False).agg({'Ticker Share Value' :'sum' })
Close_Return_df = Date_agg_PortfolioReturn.rename(columns={'Ticker Share Value': 'close'})
Close_Return_df.sort_index(ascending=False, inplace=True)
Close_Return_df['returns'] = (np.log(Close_Return_df.close / Close_Return_df.close.shift(-1)))
daily_std = np.std(Close_Return_df.returns)
std = daily_std * 252 ** 0.5

# Plot histograms
Volatility_Chart, ax = plt.subplots(1, 1, figsize=(7, 5))
n, bins, patches = ax.hist(
   Close_Return_df.returns.values,
    bins=50, alpha=0.65, color='blue')

ax.set_xlabel('log return of stock price')
ax.set_ylabel('frequency of log return')
ax.set_title('Historical Volatility')

# get x and y coordinate limits
x_corr = ax.get_xlim()
y_corr = ax.get_ylim()

# make room for text
header = y_corr[1] / 5
y_corr = (y_corr[0], y_corr[1] + header)
ax.set_ylim(y_corr[0], y_corr[1])

# print historical volatility on plot
x = x_corr[0] + (x_corr[1] - x_corr[0]) / 30
y = y_corr[1] - (y_corr[1] - y_corr[0]) / 15
ax.text(x, y , 'Annualized Volatility: ' + str(np.round(std*100, 1))+'%',
    fontsize=11, fontweight='bold')
x = x_corr[0] + (x_corr[1] - x_corr[0]) / 15
y -= (y_corr[1] - y_corr[0]) / 20



#---------------------------PERFORMANE MATRIX TABLE----------------------------------------------------------------

Portfolio_Return_aggregate = combined_df.groupby('Date Snapshot', as_index = True).agg({'Ticker Share Value' :'sum'})
Portfolio_Performance_forMetrix = Portfolio_Return_aggregate.rename(columns={'Ticker Share Value' :'Portfolio'})
stats =  Portfolio_Performance_forMetrix.calc_stats()
stats.to_csv(sep=', ', path= 'performancemetrix.csv')
Statistic_Performances = pd.read_csv('performancemetrix.csv', on_bad_lines='skip')

#----------------------------HTML VARIABLES-----------------------------------------

page_title_text='Report'
Title = 'Historyc trading report'
TraderName = Trader_Static.TRADER_NAME[0]
DateRange = 'SELECTED DATE RANGE:      ' + From_date + '__TO__ ' + Too_date  + '                                             Total Return'
NameOfFund =  Trader_Static.fund_name[0]
FundType = Trader_Static.fund_type[0]
FundFocus = Trader_Static.fund_focus[0]
prices_text = 'Industry Allocation'
stats_text = 'Historical prices summary statistics'
Annual_return = Statistic_Performances.iloc[3,1]
Max_drowdown = Statistic_Performances.iloc[7,1]
CAGR_rate = Statistic_Performances.iloc[6,1]
Day_sharpe = Statistic_Performances.iloc[4,1]
DAy_Sortino = Statistic_Performances.iloc[5,1]
Calmar_ratio = Statistic_Performances.iloc[8,1]



#--------------------------------------HTLM REPORT-------------------------------------------


dp.Report(
  dp.Page(
    title="Trader Profile",
    blocks=[
             "### Trading Historical Report",
             dp.Group(dp.BigNumber( heading="Trader name", value= TraderName  ), dp.BigNumber( heading= DateRange, value= Annual_return, change= Annual_return, is_upward_change=True ), dp.BigNumber( heading="Type of fund", value= FundType), dp.BigNumber( heading="Focus of fund", value= FundFocus), columns=4), 
             dp.Select(blocks={ dp.Plot(Symbol_Allocation_Chart, label='Portfolio Weight'), dp.Plot(Sector_Allocation_Chart, label='Portfolio Diversication by Sector'),  dp.Plot(Industry_Allocation_Chart, label='Portfolio Diversication by Industry')}, )]
  
  ),
  
  
  
  dp.Page(
    title="Portfolio Performance",
    blocks=[
             dp.Group(dp.BigNumber( heading="Total Return", value= Annual_return ),dp.BigNumber( heading="Max Drowdown", value= Max_drowdown),dp.BigNumber( heading="Daily Sharpe", value= Day_sharpe), dp.BigNumber( heading="Daily Sortino", value= DAy_Sortino),dp.BigNumber( heading="Calmar Ratio", value=Calmar_ratio),dp.BigNumber( heading="CAGR", value=CAGR_rate), columns=6), 
             dp.Plot(Portofolio_vs_Benchmark_Chart),
             dp.Group(dp.Plot(Linear_Regression_Chart),dp.Plot(Volatility_Chart),columns=2),
             dp.Select(blocks={ dp.Plot(Industry_Performance_Chart, label='Industry Performance'), dp.Plot(Sector_Performance_Chart , label='Sector Performance'),  dp.Plot(Symbol_Performance_Chart, label='Symbol Performance')}),]

     
      ),

  dp.Page(
    title="Performance Metrix",
    blocks=["### Plot", Statistic_Performances]
             

  ),


            
).save(path='repora.html', open=True)





# %%
