#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Giulio Pelosio
# Coursework Two : Incorrect Trade Detection
#--------------------------------------------------------------------------------------


# 1) import libraries------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
import datetime


# 2.0) Import data
#%%

# 2.1) Connect MongoDB ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
con = MongoClient('mongodb+srv://giuliopelosio:<password>.rzdlv.mongodb.net/myFirstDatabase?authSource=admin&replicaSet=atlas-3v4kka-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true')
db = con.Equity
col = db.CourseworkTwo


# 2.2) Connect SQL ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GITRepoDirectory = "C:/Users/Giulio/Desktop/Phyton UCL/LUCA/Second course work/data right"
engine = create_engine(f"sqlite:///C:/Users/Giulio/Desktop/Phyton UCL/LUCA/Second course work/data right/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()


# 3.0) Select data from Mongo DB-------------------------------------------------------------------------------------------------------------------------------------



# 3.1) Select data for November 11 ---------------------------------------------------------------------------------------------------------------------------------              

November_11 = col.find({"$and": [ {"DateTime": {"$gte": "ISODate(2021-11-11T00:00:00.000Z)"}}, {"DateTime": {"$lt": "ISODate(2021-11-12T00:00:00.000Z)"} } ] } )


November_11_df= pd.DataFrame(November_11)

November_11_df


# 3.2) Select data for November 12---------------------------------------------------------------------------------------------------------------------------------  

November_12 = col.find({"$and": [ {"DateTime": {"$gte": "ISODate(2021-11-12T00:00:00.000Z)"}}, {"DateTime": {"$lt": "ISODate(2021-11-13T00:00:00.000Z)"} } ] } )

November_12_df= pd.DataFrame(November_12)



# 4.0) Select data from SQL ---------------------------------------------------------------------------------------------------------------------------------------


# 4.1) November 11 ---------------------------------------------------------------------------------------------------------------------------------

def SQLQueryStatic():
    return ("SELECT equity_prices.low, equity_prices.high, equity_prices.symbol_id AS Symbol, equity_prices.cob_date FROM equity_prices WHERE cob_date= '11-Nov-2021' ")


Equity_11 = pd.read_sql_query(SQLQueryStatic(),engine)

Equity_11


# 4.2) November 12 ---------------------------------------------------------------------------------------------------------------------------------------------------

def SQLQueryStatic_1():
    return ("SELECT equity_prices.low, equity_prices.high, equity_prices.symbol_id AS Symbol, equity_prices.cob_date FROM equity_prices WHERE cob_date= '12-Nov-2021' ")


Equity_12 = pd.read_sql_query(SQLQueryStatic_1(),engine)

Equity_12

# 5.0) Merge SQL and Mongo-------------------------------------------------------------------------------------------------------------------------------------------------------- 


# 5.1) 11 November---------------------------------------------------------------------------------------------------------------------------------------------------------------

Mongo_SQL_merged_11 = pd.merge(November_11_df, Equity_11, on=["Symbol"])


Merged_df_11 = Mongo_SQL_merged_11[['Symbol', 'low', 'high', 'cob_date', 'Trader', 'Notional', 'Quantity']]

Merged_df_11


# 5.2) 12 November ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Mongo_SQL_merged_12 = pd.merge(November_12_df, Equity_12, on=["Symbol"])


Merged_df_12 = Mongo_SQL_merged_12[['Symbol','low', 'high', 'cob_date', 'Trader', 'Notional', 'Quantity']]

Merged_df_12

# 6.0) Concatenate 11 and 12 November ---------------------------------------------------------------------------------------------------------------------------------------------------------------


November_11_12 = pd.concat([Merged_df_12, Merged_df_11])

November_11_12


# 7.0) Calculate share price  ---------------------------------------------------------------------------------------------------------------------------------------------------------------


November_11_12['SharePrice'] = November_11_12['Notional'] / November_11_12['Quantity']

November_11_12


# 8.0) Create conditions for database filtering -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


conditions = [
    (November_11_12['SharePrice'] > November_11_12['low']) &  (November_11_12['SharePrice'] < November_11_12['high']),
    (November_11_12['SharePrice'] < November_11_12['low']), 
    (November_11_12['SharePrice'] > November_11_12['high'])
]

values = ['correct','Suspect', 'Suspect',]
    

November_11_12['status'] = np.select(conditions, values)


November_11_12


#%%
# 9.0) Filtering 'status' by 'Suspect' ---------------------------------------------------------------------------------------------------------------------------------------------------------------



is_suspect =  November_11_12['status']=='Suspect'

Suspect_trades =  November_11_12[is_suspect]

Suspect_trades


# 10.0) create a new table in SQLite: trades Suspect -----------------------------------------------------------------------------------

# SQL query - insert table--------------------------------------------------------------------------------------------------------

def SQLCreateRetTable():
    Command = "CREATE TABLE trades_suspects (\
    Symbol TEXT NOT NULL,\
    SharePrice INTEGER,\
    low INTEGER,\
    high INTEGER,\
    cob_date TEXT,\
    Trader TEXT,\
    status TEXT,\
    FOREIGN KEY (Trader) REFERENCES equity_prices (symbol_id))"
    return (Command)

con.execute(SQLCreateRetTable())

#%%

# SQL query - insert data in 'trades_suspects'


for i in range(len(Suspect_trades)):
    print('[INFO] Loading into trades_suspects',Suspect_trades.loc[i,'Trader'],'...\n')
    con.execute(f'INSERT INTO trades_suspects (Symbol, SharePrice, low, high, cob_date, Trader, status) \
                VALUES ("{Suspect_trades.loc[i,"Symbol"]}",\
                {Suspect_trades.loc[i,"SharePrice"]},\
                {Suspect_trades.loc[i,"low"]},\
                {Suspect_trades.loc[i,"high"]},\
                "{Suspect_trades.loc[i,"cob_date"]}",\
                "{Suspect_trades.loc[i,"Trader"]}",\
                "{Suspect_trades.loc[i,"status"]}")')
            
            

# %%
# 11.  Adding new position to portfolio position ------------------------------------------------------------------


date_11 = '20211111'
date_12 = '20211112'


Mongo_SQL_merged_11['pos_id'] = Mongo_SQL_merged_11['Trader'].astype(str) + date_11 + Mongo_SQL_merged_11['Symbol']

Mongo_SQL_merged_12['pos_id'] = Mongo_SQL_merged_12['Trader'].astype(str) + date_12 + Mongo_SQL_merged_12['Symbol']

Final_Merge = pd.concat([Mongo_SQL_merged_11 , Mongo_SQL_merged_12])

Final_Merge

# %%
Target = Final_Merge.groupby(['pos_id', 'cob_date', 'Trader', 'Symbol', 'Ccy',]).agg(Quantity = ('Quantity','sum'),Notional = ('Notional', 'sum')).reset_index()

Target 

   
# %%
for i in range(len(Target)):
    print('[INFO] Loading into portfolio_positions',Target.loc[i,'Trader'],'...\n')
    con.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) \
                VALUES ("{Target.loc[i,"pos_id"]}",\
                "{Target.loc[i,"cob_date"]}",\
                "{Target.loc[i,"Trader"]}",\
                "{Target.loc[i,"Symbol"]}",\
                "{Target.loc[i,"Ccy"]}",\
                {Target.loc[i,"Quantity"]},\
                {Target.loc[i,"Notional"]})')
            
# %%
con.close() 
# %%
