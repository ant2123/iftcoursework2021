#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Yufan Xie
# Topic   : CW3_input_loader 
#--------------------------------------------------------------------------------------

# 1. Select required data from data frame

# 2.Equity price Data from 11-11 to 11-12
Data_1112 <- conMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-11T00:00:00.000Z)\", \"$lt\": \"ISODate(2021-11-13T00:00:00.000Z)\"}}")
# Change DateTime to Date
Data_1112$DateTime <- as.Date(substr(Data_1112$DateTime,9,18))
# Change Column name of DateTime into Date
names(Data_1112)[names(Data_1112) == 'DateTime'] <- 'Date' 
# aggregate all trades by Trader, ISIN, Currency and Date
Data_1112_A <- aggregate(Data_1112[c("Quantity","Notional")],by=Data_1112[c("Trader","Symbol","Ccy","Date")],
                         FUN=sum)

# 3. Delete irrelevant trade limit policy
TraderLimits_Clean <- TraderLimits[is.na(TraderLimits["limit_end"]),]

# 4. Turning Value for PCG into percentage
for(i in 1:nrow(TraderLimits_Clean)) {
  if (TraderLimits_Clean$limit_category[i] =="relative"){
    TraderLimits_Clean$limit_amount[i] <- TraderLimits_Clean$limit_amount[i]/100
  }
}

# 5. Change the date format for equity price table
EquityPrices <- as.data.frame(dbGetQuery(conSql, Equityprices))
EquityPrices$cob_date <- as.Date(EquityPrices$cob_date,format ="%d-%b-%Y")

# 6. Add a column with the equity close price for Data_1112_A
MtMPrice <- function(x,y){
  subset(EquityPrices, EquityPrices$symbol_id == x & EquityPrices$cob_date==y,
         select = close)}

for(i in 1:nrow(Data_1112_A)){
  Data_1112_A$ MtM_Close [i] <- MtMPrice(Data_1112_A$Symbol[i],Data_1112_A$Date[i])
}
Data_1112_A$MtM_Close <- as.numeric(Data_1112_A$MtM_Close)

# 7. Add a column with the equity volume for Data_1112_A
EquityVolume <- function(x,y){
  subset(EquityPrices, EquityPrices$symbol_id == x & EquityPrices$cob_date==y,
         select = volume)}

for(i in 1:nrow(Data_1112_A)){
  Data_1112_A$ EquityVolume [i] <- EquityVolume(Data_1112_A$Symbol[i],Data_1112_A$Date[i])
}
Data_1112_A$EquityVolume <- as.numeric(Data_1112_A$EquityVolume)

# 8. Add a column with the sector for Data_1112_A
EquitySector <- function(x,y){
  subset(EquityStatic, EquityStatic$symbol == x,
         select = GICSSector)}

for(i in 1:nrow(Data_1112_A)){
  Data_1112_A$ EquitySector [i] <- EquitySector(Data_1112_A$Symbol[i])
}
Data_1112_A$ EquitySector <- as.character(Data_1112_A$ EquitySector)

# 9. Add a column with the mark to market amount for Data_1112_A
for(i in 1:nrow(Data_1112_A)){
  Data_1112_A$MtM_amount[i] <- (Data_1112_A$ MtM_Close [i]*Data_1112_A$Quantity[i])}
