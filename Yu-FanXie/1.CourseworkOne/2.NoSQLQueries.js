//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
// CourseworkOne
// Author  : Yufan Xie
// Topic   : NoSQL DataBase - MongoDB
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

//Query_1:
//- Aims: 
//Find the average beta, highest market price, average devident yield and 
//average PE ratio of large market cap sercurities in each industry (market cap greater than 10 billions).
//- Output:
//The industry Gold has a the lowest average beta ratio at -0.07 among the whole market.
//The nagetiva beta value illustrates that the prices or expected values of the securities in gold industry
//tend to go up, as the whole market goes down. 
//- Approch
//Use function aggregate to compute some statistics for large market cap companies based on the industry。
//---------------------------------------------------------------------------------------------------------------------------------------------------------------

db.CourseworkOne.aggregate(                  
    [{$match:{"MarketData.MarketCap":{"$gte":10000}}},
    {$group:{_id:"$StaticData.GICSSubIndustry",    
    AverageBeta:{$avg:"$MarketData.Beta"},
    MaxMarketPrice:{$max:"$MarketData.Price"},
    AverageDividendYield:{$avg:"$FinancialRatios.DividendYield"},
    AveragePERatio:{$avg:"$FinancialRatios.PERatio"}
    }},
    {$sort:{AverageBeta:1}}
    ])
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------------
  //Query_2:
  //- Aims: 
  //The industry average PE ratio for Application software, 121.64, is relatively higher than the figure for other industries.
  //It might be more stocks currently have a lower PE ratio than the industry average, 
  //and are undervalued than other stocks in same industry.
  //Hence, it is worth to find the securities in Application Software industry who have lower PE ratio than the industry average.
  //- Output:
  //There are 5 stocks in Application Software industry, who have a lower PE ratio than the industry average.
  //- Approch:
  //Use function $and to find documents which satisfy two conditions, within the Application Software industry,
  //and has lower PE ratio than the industry average, 121.64.
  
  //-----------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  db.CourseworkOne.find({$and: [{ "FinancialRatios.PERatio": {"$lt": 121.64} }, 
  {"StaticData.GICSSubIndustry": {"$eq":"Application Software"} } ] } )