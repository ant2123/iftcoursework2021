//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Big Data in Quantitative Finance
// Author  : Farid Rajabov
// Topic   : Coursework 1 - SQL
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------


//----SQL Query 1 - We Find good stocks for daytrading in the sectors of "Information Technology" and "Communication Services"----------------------------------------------------------------------------------------------------------------

SELECT Symbol,Security, ROUND(ROUND(AVG(equity_prices.close),2) - ROUND(AVG(equity_prices.open),2), 2) AS "Average Intraday Price Change",
CASE 
	WHEN ROUND(ROUND(AVG(equity_prices.close),2) - ROUND(AVG(equity_prices.open),2), 2) BETWEEN 0.01 AND 0.05 THEN 'Recommended Investment'
	WHEN ROUND(ROUND(AVG(equity_prices.close),2) - ROUND(AVG(equity_prices.open),2), 2) > 0.05 THEN 'Very Good Investment' 
	ELSE 'Bad Investment' 
END AS 'Investment Recommendation'
FROM equity_prices
LEFT JOIN equity_static ON equity_prices.Symbol_id = equity_static.symbol
Where GICSSector = "Information Technology" OR GICSSector = "Communication Services"
group by symbol_id
ORDER BY "Average Intraday Price Change" DESC;


//----SQL Query 2 - See the profitability of positions taken by the particular trader for daytrading long----------------------------------------------------------------------------------------------------------------

SELECT trader_name, symbol, net_amount, equity_prices.open, equity_prices.close,  
ROUND(ROUND(equity_prices.close,2)* portfolio_positions.net_amount - ROUND(equity_prices.open,2) * portfolio_positions.net_amount) AS  "Likely Profit or Loss if Daytraded Long"
FROM equity_prices
INNER JOIN portfolio_positions 
ON equity_prices.symbol_id = portfolio_positions.symbol 
AND equity_prices.cob_date = portfolio_positions.cob_date
INNER JOIN trader_static ON portfolio_positions.trader = trader_static.trader_id
GROUP BY symbol
ORDER BY "Likely Profit or Loss if Daytraded Long" DESC;


