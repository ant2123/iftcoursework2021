#-------------------------------------------------
#-------------------------------------------------
# Farid Rajabov - best code in the world :)
#-------------------------------------------------
# Coursework Two - Case One - CreateTable
#-------------------------------------------------



# Put Suspected Trades in a Table

def CrtSusTblSQL():
    TCCommand = "CREATE TABLE trades_suspects (\
    Symbol TEXT NOT NULL,\
    TradeId TEXT NOT NULL,\
    Trader TEXT NOT NULL,\
    cob_date TEXT NOT NULL,\
    Stock_Price INTEGER,\
    high INTEGER,\
    low INTEGER,\
    Suspicion_Status TEXT NOT NULL,\
    FOREIGN KEY (Symbol) REFERENCES equity_prices (symbol_id))"
    return (TCCommand)

