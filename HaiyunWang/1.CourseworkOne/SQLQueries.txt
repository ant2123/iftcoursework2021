
/*
1. Mark-to-market and P&L
Calculate the mark-to-market amount and P&L
for each equity owned by trader DGR1983 on  03-Jan-2020.
*/

SELECT portfolio_positions.symbol, portfolio_positions.net_quantity, portfolio_positions.net_amount, equity_prices.close,
portfolio_positions.net_quantity * equity_prices.close AS mtm_amount,
portfolio_positions.net_quantity * equity_prices.close - portfolio_positions.net_amount AS pnl
FROM portfolio_positions
LEFT JOIN equity_prices ON portfolio_positions.symbol = equity_prices.symbol_id
AND portfolio_positions.cob_date = equity_prices.cob_date


/*
2. Average close
Calculate the number of trading days and the average close price
for each equity during the period recorded by this dataset
*/

SELECT symbol_id,
COUNT(cob_date) as no_tradingdays,
ROUND(SUM(close)/COUNT(cob_date), 2) AS avg_close
FROM equity_prices
GROUP BY symbol_id


/*
3. Liquidity & level of activity of each sector
To measure the liquidity and level of activity of each GICS sector/industry, 
calculate the total trading volume for each sector/industry.
One limitation due to order of operations:
ORDER BY has the lowest priority, so we cannot reorder the output by total volume.
*/

SELECT GICSSector, SUM(volume) AS total_SecVol
FROM equity_static
LEFT JOIN equity_prices ON equity_static.symbol = equity_prices.symbol_id
GROUP BY GICSSector
ORDER BY total_SecVol

SELECT GICSIndustry, SUM(volume) AS total_IndVol
FROM equity_static
LEFT JOIN equity_prices ON equity_static.symbol = equity_prices.symbol_id
GROUP BY GICSIndustry
ORDER BY total_IndVol
