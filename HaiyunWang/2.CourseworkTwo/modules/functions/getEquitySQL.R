# Get equityPrices ---------------------------------------------------------
getEquitySQL <-  function(conSql){
  #-- send query
  equityPrices <- RSQLite::dbGetQuery(conSql, paste0("SELECT * FROM equity_prices"))
  #-- convert into dates
  equityPrices$cob_date <- lubridate::dmy(equityPrices$cob_date)
  return(equityPrices)
}

equityPrices <- getEquitySQL(conSql)
head(equityPrices)