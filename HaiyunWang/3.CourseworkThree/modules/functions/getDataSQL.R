library(RSQLite)
library(lubridate)

# Setting up connection for SQLite ---------------------------------------------
conSql <- dbConnect(RSQLite::SQLite(), Config$Directories$SQLDataBase)
# Get data from SQL ---------------------------------------------------------
getPricesSQL <-  function(conSql){
  #-- send query
  equity_prices <- RSQLite::dbGetQuery(conSql, paste0("SELECT * FROM equity_prices"))
  #-- convert into dates
  equity_prices$cob_date <- lubridate::dmy(equity_prices$cob_date)
  return(equity_prices)
}

equity_prices <- getPricesSQL(conSql)

getStaticSQL <-  function(conSql){
  #-- send query
  equity_static <- RSQLite::dbGetQuery(conSql, paste0("SELECT * FROM equity_static"))
  return(equity_static)
}

equity_static <- getStaticSQL(conSql)

getPortfolioSQL <-  function(conSql){
  #-- send query
  portfolio_positions <- RSQLite::dbGetQuery(conSql, paste0("SELECT * FROM portfolio_positions"))
  #-- convert into dates
  portfolio_positions$cob_date <- lubridate::dmy(portfolio_positions$cob_date)
  return(portfolio_positions)
}

portfolio_positions <- getPortfolioSQL(conSql)


