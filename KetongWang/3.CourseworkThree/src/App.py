import datetime
import sys
import os

from fastapi import FastAPI, Form
import uvicorn
from starlette.requests import Request
from starlette.templating import Jinja2Templates
sys.path.append(os.path.join(os.path.abspath('../modules/'),'db'))
import db_connection
# from modules.db import db_connection

app = FastAPI()

templates = Jinja2Templates(directory="../")
# The default limit number is 100
limit_num = 100

@app.get("/")
async def index(request: Request):
    # Go to Home.html page
    return templates.TemplateResponse("Home.html", {"request": request})


@app.post("/add")
async def add(trader: str = Form(...),
              ccy: str = Form(...),
              symbol: str = Form(...),
              quantity: int = Form(...),
              price: float = Form(...),
              trade_type: str = Form(...),
              counter_party: str = Form(...),
              ):
    res_message =""
    try:
        # Connecting to Equity.db
        conn = db_connection.get_sqlite_connection()
        cur = conn.cursor()
        flag = True
        # The quantity must be greater than 0
        if quantity<=0:
            res = {}
            res["flag"] = True
            res["msg"] = "Invalid quantity.Quantity must be an integer greater than 0"
            return res
        # Price must be greater than 0
        if price<=0:
            res = {}
            res["flag"] = True
            res["msg"] = "Invalid notional.Notional must be greater than 0"
            return res
        # Notional
        notional=price * quantity
        # Current time: System local time
        date = datetime.datetime.now()
        date_time=datetime.datetime.strftime(date,"%Y-%m-%d %H:%M:%S")
        date_str1 = datetime.datetime.strftime(date, '%d-%b-%Y')
        date_str2 = datetime.datetime.strftime(date, '%Y%m%d%H%M%S')
        # Check the existence of symbol
        cur.execute("select * from equity_static where symbol='{}'".format(symbol))
        symbol_res = cur.fetchone()
        symbol_flag = False
        if symbol_res:
            # Flag as True if exists
            symbol_flag = True
        # Checking
        max_high=None
        min_low=None
        max_volume=None

        # Find the highest price of the stock: HIGH
        if symbol_flag:
            # When symbol exists
            cur.execute("SELECT MAX(high) FROM equity_prices where symbol_id='{}'".format(symbol))
        else:
            # When it does not exist
            cur.execute("SELECT MAX(high) FROM equity_prices")
        res = cur.fetchone()
        if res:
            max_high=res[0]

        # Find the smallest 'low'
        if symbol_flag:
            # When symbol exists
            cur.execute("SELECT MIN(low) FROM equity_prices where symbol_id='{}'".format(symbol))
        else:
            # When it does not exist
            cur.execute("SELECT MIN(low) FROM equity_prices")
        res = cur.fetchone()
        if res:
            min_low = res[0]

        # Find the maximum volume
        if symbol_flag:
            # When symbol exists
            cur.execute("SELECT MAX(volume) FROM equity_prices where symbol_id='{}'".format(symbol))
        else:
            # When it does not exist
            cur.execute("SELECT MAX(volume) FROM equity_prices")
        res = cur.fetchone()
        if res:
            max_volume = res[0]

        # Entry rules: unit price less than 500% of the highest price, more than 10% of the lowest price, 
        #              quantity less than the maximum volume * 5
        if price<=5*max_high and price>=0.1*min_low and quantity<=5*max_volume:
            flag = False
        if flag:
            # Suspicious data, not allowed into the database
            res_message="Transaction amount/transaction quantity is abnormal, the transaction cannot be completed"
        else:
            # Can be imported into the database
            trade_id='S{}{}{}'.format(trader,symbol,date_str2)
            tmp = {
                "DateTime": "",
                "TradeId": trade_id,
                "Trader": trader,
                "Symbol": symbol,
                "Quantity": quantity,
                "Notional": notional,
                "TradeType": trade_type,
                "Ccy": ccy,
                "Counterparty": counter_party
            }
            res = get_isodatetime(date_time)
            tmp["DateTime"] = res
            # Get mongodb connection
            collections = db_connection.get_mongo_connection()
            # Insert data
            x = collections.insert_one(tmp)
            res_message = "Successful execution of transaction:_id:{}".format(x.inserted_id)
    except Exception as e:
        res_message="Transaction amount/transaction quantity is abnormal, the transaction cannot be completed."
    finally:
        # Return
        res={}
        res["flag"]=True
        res["msg"]=res_message
        return res

def get_isodatetime(date_time:str):
    """
    Format datetime
    :param date_time:
    :return:str
    """
    date = datetime.datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
    date_str1 = datetime.datetime.strftime(date, '%Y-%m-%d')
    date_str2 = datetime.datetime.strftime(date, '%H:%M:%S')
    res = "ISODate({}T{}.000Z)".format(date_str1, date_str2)
    return res

if __name__ == "__main__":
    if len(sys.argv)>0:
        port=int(sys.argv[1])
        uvicorn.run("App:app", host="localhost", port=port)
    else:
      uvicorn.run("App:app", host="localhost", port=5555)
