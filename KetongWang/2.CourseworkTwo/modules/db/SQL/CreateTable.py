
from modules.db.db_connection import get_sqlite_connection


sql="""create TABLE trades_suspects(
trade_id TEXT,
date TEXT,
trader TEXT,
symbol TEXT,
quantity INTEGER,
notional real,
trad_type TEXT,
ccy TEXT,
counterparty TEXT,
PRIMARY KEY(trade_id),
FOREIGN KEY(trader) REFERENCES trader_static(trader_id),
FOREIGN KEY(symbol) REFERENCES equity_static(symbol)
);"""

def create_table():
    try:
        conn = get_sqlite_connection()
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        print("table trades_suspects already exists")


