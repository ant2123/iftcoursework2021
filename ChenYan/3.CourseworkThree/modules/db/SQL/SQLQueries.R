#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chen Yan
# Topic   : Use Case Three.a (R) - visualising daily positions by trader.
# File    : SQL Queries
#--------------------------------------------------------------------------------------

# 1. SQL Query to load portfolio_positions ----------------------------------------
SQLQueryPortfolio <- paste0("SELECT * FROM portfolio_positions")

SQLQueryStatic <- paste0("SELECT * FROM equity_static")

SQLQueryPrice <- paste0("SELECT * FROM equity_prices")






