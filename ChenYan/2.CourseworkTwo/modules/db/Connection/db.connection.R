#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chen Yan
# Topic   : Connecting to SQLite and MongoDB
#--------------------------------------------------------------------------------------

# Setting up SQLite connection 
UserPath <- "/Users/YanC/Downloads/Study" 
FixedDataBase <- "/iftcoursework2021/000.DataBases/SQL" 
conSql <- dbConnect(RSQLite::SQLite(), paste0(UserPath, FixedDataBase, "/Equity.db")) 


# Setting up MongoDB Connection -------------------------------------------
conMongo <- mongo(collection = "CourseworkTwo", db = "Equity", url = "mongodb://localhost:27017",
                  verbose = FALSE, options = ssl_options()) 