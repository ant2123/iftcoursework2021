#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chen Yan
# Topic   : Main
#--------------------------------------------------------------------------------------

# install packages needed if not yet installed
if(!any(c("dplyr", "lubridate", "RSQLite") %in% installed.packages()[,1])){
  install.packages("ggplot2")
  install.packages("mongolite")
  install.packages("dplyr")
  install.packages("lubridate")
  install.packages("RSQLite")
} 

#import packages
library(mongolite) 
library(ggplot2)
library(dplyr)
library(lubridate)
library(RSQLite)

# important to set up directories
GITRepoDirectory <- "/Users/YanC/Downloads/Study/iftcoursework2021" 

# loading the sources
source(paste0(GITRepoDirectory, "/ChenYan/2.CourseworkTwo/config/script.config"))
source(paste0(GITRepoDirectory, Config$Directories$Connection, "/db.connection.R"))
source(paste0(GITRepoDirectory, Config$Directories$SQLQuery, "/SQLQueries.R"))
source(paste0(GITRepoDirectory, Config$Directories$SQLQuery, "/CreateTable.R"))
source(paste0(GITRepoDirectory, Config$Directories$TestFunction, "/TestFunction.R"))

# import equity data
fullEquity <- as.data.frame(dbGetQuery(conSql, SQLQueryPrices))

# select equity price on 11th and 12th separately
targetEquity11<-fullEquity[which(fullEquity$cob_date=="11-Nov-2021"),]
targetEquity12<-fullEquity[which(fullEquity$cob_date=="12-Nov-2021"),]

# import all trade data
fullUniverse <- conMongo$find(query = "{}") 

# select trade data on 11th and 12th separately and calculate notional price
Trade_Date<-c("2021-11-11","2021-11-12")

Trade_Data11<-TradeData(fullUniverse,Trade_Date[1])
Trade_Data11$notional_price<- Trade_Data11$Notional/Trade_Data11$Quantity

Trade_Data12<-TradeData(fullUniverse,Trade_Date[2])
Trade_Data12$notional_price<- Trade_Data12$Notional/Trade_Data12$Quantity


# fat finger test
FatFingerSuspect<-as.data.frame(matrix(nrow=0,ncol=ncol(Trade_Data11)),col.names=colnames(Trade_Data11))
FatFingerSuspect<-rbind(FatFingerSuspect,FatFingerTest(Trade_Data11,targetEquity11))
FatFingerSuspect<-rbind(FatFingerSuspect,FatFingerTest(Trade_Data12,targetEquity12))

# GBM test
GBMTest_suspects<-as.data.frame(matrix(nrow=0,ncol=ncol(Trade_Data11)),col.names=colnames(Trade_Data11))
GBMTest_suspects <- rbind(GBMTest_suspects,GBMTest11(Trade_Data11,fullEquity))
GBMTest_suspects <- rbind(GBMTest_suspects,GBMTest12(Trade_Data12,fullEquity))

# combined two table
trades_suspects<-rbind(FatFingerSuspect,GBMTest_suspects)
trades_suspects<-trades_suspects[!duplicated(trades_suspects, fromLast=TRUE), ]
trades_suspects<-trades_suspects[,-length(trades_suspects)]

# create a new table in SQL
dbExecute(conSql, SQLCreateSuspectsTable)

# insert suspect trades into trades_suspects table in SQL
for(i in 1:nrow(trades_suspects)){
  dbExecute(conSql, paste0("INSERT INTO trades_suspects (DateTime, TradeId, Trader, Symbol, Quantity, Notional,TradeType,Ccy,Counterparty) ", 
                           "VALUES (\"",
                           trades_suspects[i,"DateTime"],"\",\"",
                           trades_suspects[i,"TradeId"],"\",\"",
                           trades_suspects[i,"Trader"],"\",\"",
                           trades_suspects[i,"Symbol"],"\",",
                           trades_suspects[i,"Quantity"],",",
                           trades_suspects[i,"Notional"],",\"",
                           trades_suspects[i,"TradeType"],"\",\"",
                           trades_suspects[i,"Ccy"],"\",\"",
                           trades_suspects[i,"Counterparty"],"\")"))
  
}

# if need, delete the suspect trades table
dbExecute(conSql,SQLDeleteSuspectsTable)



# aggregate Quantity and Notional for all trades by date, trader, symbol, ccy
position11<-aggregate(Trade_Data11[,5:6],by=list(trader=Trade_Data11$Trader,symbol=Trade_Data11$Symbol,ccy=Trade_Data11$Ccy),FUN=sum)
position12<-aggregate(Trade_Data12[,5:6],by=list(trader=Trade_Data12$Trader,symbol=Trade_Data12$Symbol,ccy=Trade_Data12$Ccy),FUN=sum)

# reformatting the aggregated table
for(i in 1:nrow(position11)){
  position11$pos_id[i]<-paste0(position11$trader[i],"20211111",position11$symbol[i])
  position11$cob_date[i]<-"11-Nov-2021"
}

for(i in 1:nrow(position12)){
  position12$pos_id[i]<-paste0(position12$trader[i],"20211112",position12$symbol[i])
  position12$cob_date[i]<-"12-Nov-2021"
}

portfolio_position<-rbind(position11,position12)
colnames(portfolio_position)[4]<-'net_quantity'
colnames(portfolio_position)[5]<-'net_amount'
new_cols<-c('pos_id','cob_date','trader','symbol','ccy','net_quantity','net_amount')
portfolio_position<-portfolio_position[,new_cols]


# insert the results into portfolio_positions
for(i in 1:nrow(portfolio_position)){
  dbExecute(conSql, paste0("INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) ", 
                           "VALUES (\"",
                           portfolio_position[i,"pos_id"],"\",\"",
                           portfolio_position[i,"cob_date"],"\",\"",
                           portfolio_position[i,"trader"],"\",\"",
                           portfolio_position[i,"symbol"],"\",\"",
                           portfolio_position[i,"ccy"],"\",",
                           portfolio_position[i,"net_quantity"],",",
                           portfolio_position[i,"net_amount"],")"))
  
}


# if need, delete 11th and 12th position data
dbExecute(conSql,SQLDeletePosition)

# close the connection with SQL
dbDisconnect(conSql)

# close the connection with MongoDB
conMongo$disconnect()
