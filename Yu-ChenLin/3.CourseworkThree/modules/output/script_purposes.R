#-- get data
positions_new <- getPortfolioPositionSQL(ConSql)
prices_new  <- getPriceSQL(ConSql)
traderstatic_new  <- getTraderStaticSQL(ConSql)
equitystatic_new  <- getEquityStaticSQL(ConSql)

#Create 2021-11-12 portfolio allocation for selected trader "DGR1983"

#filter date, selected trader 
PortfolioAllocation<- positions_new %>%
  select(everything()) %>%  
  filter(cob_date == endofdate, trader == traderselect) #29 obs.

#portfolio allocation add sector
PAinfo <- left_join(PortfolioAllocation, equitystatic_new,by = c("symbol"="symbol"))

#porfolio endofdate1112 prices and return
#get all close price
closeprice<-prices %>% 
  select(close,symbol_id,cob_date) %>% 
  group_by(symbol_id) %>%
  mutate(lag1 = lag(close, order_by = cob_date))
#calculate selected trader portfolio's return 1112
endofdateret <- left_join(PortfolioAllocation, closeprice,by = c("symbol"='symbol_id',"cob_date")) %>% 
  mutate(weight = net_amount/sum(net_amount), portfolioReturns_daily =sum(weight*((close/lag1) -1)))
endofdatereturn <- endofdateret$portfolioReturns_daily
#endofdatereturn=0.012
#weight*return (net amount/sum net amount)*((11.12close price/11.11close price)-1)

#--------------------------------------------------------------------------------

#portfolio_historical data all from 2020-01-02 to endofdate
portfoliohistorical<-prices %>% 
  select(close,symbol_id,cob_date) %>% 
  filter(symbol_id %in% PortfolioAllocation$symbol)
portfoliohistoricalPrices<-spread(portfoliohistorical, symbol_id, close)
#convert into xts file
qxts <- xts(portfoliohistoricalPrices[,-1], order.by=portfoliohistoricalPrices[,1])
colSums(is.na(qxts))
#portfolio_historical return
portfoliohistoricalReturns <- na.omit(ROC(qxts))
portfolioReturn <- Return.portfolio(portfoliohistoricalReturns, weights= endofdateret$weight)
#---------------------------------------------
#benchmark S&P500
benchmarkPrices <- getSymbols.yahoo('^GSPC', from = startofdate, to = endofdateplusone, periodicity = 'daily', auto.assign=FALSE)[,4]
benchmarkReturns <- na.omit(ROC(benchmarkPrices))
colnames(benchmarkReturns) <-'S&P500.Return'
#colSums(is.na(benchmarkPrices))
#--------------------------------------------------------------------------------
#RISK AND PERFORMANCE
#s.d./volatility historical
SD <- sd(portfolioReturn, na.rm = FALSE) #0.01968

#sharp ratio historical
SharpeRatio(portfolioReturn, rf) #StdDev Sharp=0.04105(4%)

#alpha
CAPM.jensenAlpha(portfolioReturn, benchmarkReturns, rf) #0.02243859, >0 beat the market

#beta
CAPM.beta(portfolioReturn, benchmarkReturns, rf) #1.08989

table.AnnualizedReturns(portfolioReturn)
table.CalendarReturns(portfolioReturn)
