// Query 1

db.CourseworkOne.aggregate([

	{$match: {"StaticData.GICSSector": "Information Technology"} },

	{$group: {_id: "$StaticData.GICSSubIndustry", average_Beta: {$avg: "$MarketData.Beta"},average_PERatio: {$avg: "$FinancialRatios.PERatio"} } } ]).sort({average_Beta:-1})


//Query 2

db.CourseworkOne.aggregate([

	{$match: {"StaticData.GICSSector": "Information Technology"} },

	{$group: {_id: "$StaticData.GICSSubIndustry", total_market_cap: {$sum: "$MarketData.MarketCap"} } }]).sort({total_market_cap:-1})