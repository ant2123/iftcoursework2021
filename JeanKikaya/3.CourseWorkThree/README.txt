UCL IFT - Big Data in Quantitative Finance
USE Case one 
This script will perform the following task:

Aggregate and calculate weights and portfolio metrics for any given day (2021-11-12 for this case).
SPY ETF and GOVT ETF are downloaded from yahoo finance to evaluate the perfomance of the portfolio.

In order to trigger this script, open the command line to find the appropriate directory.

The script.ini file needs to be adjusted to create the database connections for SQLite and MongoDB.
Please edit the sql variable so it contains personal git repositroy 
To run the code from the command prompt, the user must pass arguments in order to produce the html file.

cd ./iftcoursework2021/JeanKikaya/3.CourseworkThree/src python App.py DGR1983 2020-01-02 2021-11-12

The arguments are in this order: 1.TraderID, 2.start date, 3.end date

List of traders: JBX1566, DGR1983,DHB1075, SML1458, DMZ1796, MRH5231

The start date should be set to 2020-01-02 since it is oldest date in the SQL dataset.