
SELECT (SUM(equity_prices.close)/SUM(equity_prices.open)) Growth, equity_static.GICSSector
FROM equity_prices JOIN equity_static ON equity_prices.symbol_id=equity_static.symbol
GROUP BY GICSSector
ORDER BY Growth DESC;



SELECT equity_static.GICSSector, equity_static.security,avg((equity_prices.close-equity_prices.open)/equity_prices.open) Expected_Return 
FROM equity_prices JOIN equity_static ON equity_prices.symbol_id=equity_static.symbol
GROUP BY equity_static.security
ORDER BY equity_static.GICSSector, Expected_Return DESC;

SELECT equity_static.GICSSector, equity_static.security, SUM(equity_prices.high)/SUM(equity_prices.low) Volatility 
FROM equity_prices JOIN equity_static ON equity_prices.symbol_id=equity_static.symbol
GROUP BY equity_static.security
ORDER BY equity_static.GICSSector, Volatility DESC;