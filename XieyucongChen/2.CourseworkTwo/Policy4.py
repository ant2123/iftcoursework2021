from scipy.stats import norm
import pandas as pd
import numpy as np

# Policy 4 ES relative (%): the trader cannot run a portfolio that has an expected shortfall greater than X%.

# compute the VaR for the stock 
def compute_ES(alpha, stock, start_date, end_date, con):
    equity_prices = pd.read_sql_query(f"SELECT * FROM equity_prices WHERE symbol_id = '{stock}'", con)
    # apply dates
    equity_prices = equity_prices[equity_prices["price_id"] >= f"{start_date}{stock}"]
    equity_prices = equity_prices[equity_prices["price_id"] <= f"{end_date}{stock}"]
    
    stockData = equity_prices["close"]
    returns = stockData.pct_change(periods=3).dropna()
    VaR = np.percentile(returns, alpha)

    belowVaR = returns <= VaR
    return returns[belowVaR].mean()



# historical method:
def check_policy4_historical(df, limit_dict, con):
    date_dict = {
    "20211112": "20210923",
    "20211111": "20210922"
    }
    breaches = []
    for index, agg in df.iterrows():

        if agg["Date"] != "2021-11-11" and agg["Date"] != "2021-11-12":
            continue
        if not (agg["Trader"], "ES", "relative", "USD") in limit_dict:
            continue

        ES = compute_ES(1, agg["Symbol"], date_dict[agg["Date"].replace("-", "")], agg["Date"].replace("-", ""), con)
        ES_limit = limit_dict[(agg["Trader"], "ES", "relative", "USD")]

        if ES * -100 > ES_limit[0]:
            print("ES policy breach", "limit:", ES_limit, agg)
            print("ES", ES)
            breaches.append((4, ES_limit[1], ES * -100.0, agg["Date"]))

    return breaches