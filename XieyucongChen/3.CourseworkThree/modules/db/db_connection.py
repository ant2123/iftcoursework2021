from pymongo import MongoClient
import sqlite3

def connect_mongodb(address, port):
    client = MongoClient(f'{address}:{port}')
    db = client.Equity
    CourseworkTwo = db.CourseworkTwo

    return client, CourseworkTwo

def connect_sqlite(sqlite_db_file):
    con = sqlite3.connect(sqlite_db_file)
    return con