db.CourseworkTwo.aggregate([{
    $match: {
        DateTime: {
            $gte: "ISODate(2021-11-11T00:00:00.000Z)",
            $lt: "ISODate(2021-11-12T00:00:00.000Z)"
        }
    }
},
{
    $group: {
        _id: {
            "Trader": "$Trader",
            "Symbol": "$Symbol",
            "Currency": "Ccy"
        },
        SumQuantity: {
            $sum: "$Quantity"
        },
        SumNotional: {
            $sum: "$Notional"
        }
    }
}])



db.CourseworkTwo.aggregate([
    {
        $project: {
            date: {
                $substr: ["$DateTime", 7, 11]
            }
        }
    }
])

