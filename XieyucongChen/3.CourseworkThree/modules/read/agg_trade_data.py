import pandas as pd

def read_agg_trade_data(CourseworkTwo):
    a = CourseworkTwo.aggregate([
    {
        "$addFields": {
            "date": {
                "$substr": ["$DateTime", 8, 10]
            }
        }
    },
    {
        "$group": {
            "_id": {
                "Trader": "$Trader",
                "Symbol": "$Symbol",
                "Currency": "$Ccy",
                "Date": "$date"
            },
            "SumQuantity": {
                "$sum": "$Quantity"
            },
            "SumNotional": {
                "$sum": "$Notional"
            }
        }
    }])


    ##### convert to dataframe

    aggregates = []

    for agg in a:
        # print(agg)
        df_dict = {}
        df_dict["Trader"] = agg["_id"]["Trader"]
        df_dict["Symbol"] = agg["_id"]["Symbol"]
        df_dict["Currency"] = agg["_id"]["Currency"]
        df_dict["Date"] = agg["_id"]["Date"]
        df_dict["SumQuantity"] = agg["SumQuantity"]
        df_dict["SumNotional"] = agg["SumNotional"]
        aggregates.append(df_dict)

    df = pd.DataFrame(aggregates)
    # print(df.head())
    return df

def read_trade_type_trade_data(CourseworkTwo):
    a = CourseworkTwo.aggregate([
    {
        "$addFields": {
            "date": {
                "$substr": ["$DateTime", 8, 10]
            }
        }
    },
    {
        "$group": {
            "_id": {
                "Trader": "$Trader",
                "Symbol": "$Symbol",
                "Currency": "$Ccy",
                "Date": "$date",
                "TradeType": "$TradeType"
            },
            "SumQuantity": {
                "$sum": "$Quantity"
            },
            "SumNotional": {
                "$sum": "$Notional"
            }
        }
    }])


    ##### convert to dataframe

    aggregates = []

    for agg in a:
        # print(agg)
        df_dict = {}
        df_dict["Trader"] = agg["_id"]["Trader"]
        df_dict["Symbol"] = agg["_id"]["Symbol"]
        df_dict["Currency"] = agg["_id"]["Currency"]
        df_dict["Date"] = agg["_id"]["Date"]
        df_dict["TradeType"] = agg["_id"]["TradeType"]
        df_dict["SumQuantity"] = agg["SumQuantity"]
        df_dict["SumNotional"] = agg["SumNotional"]
        aggregates.append(df_dict)

    df = pd.DataFrame(aggregates)
    # print(df.head())
    return df