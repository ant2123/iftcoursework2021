def read_trader_limits(con):
    cur = con.cursor()
    limits = cur.execute('SELECT * FROM trader_limits WHERE (limit_end is null or limit_end="")')

    # limits = [l for l in limits]
    limit_dict = {}


    for l in limits:
        # print(l)
    #     l[1] trader_id
    #     l[2] limit_type
    #     l[3] limit_category
    #     l[4] limit_amount
    #     l[5] currency
        limit_dict[(l[1], l[2], l[3], l[5])] = (l[4], l[0])
    return limit_dict
