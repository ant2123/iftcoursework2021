import pandas as pd
import numpy as np

# Policy 5: Volatility (%)
# the trader cannot run a portfolio that has a portfolio annualised return volatility greater than the limit express in percentages. Look-back for volatility calculations is 50Days.

# compute the volatility of the stock
def compute_volatility(stock, start_date, end_date, con):
    equity_prices = pd.read_sql_query(f"SELECT * FROM equity_prices WHERE symbol_id = '{stock}'", con)
    # apply dates
    equity_prices = equity_prices[equity_prices["price_id"] >= f"{start_date}{stock}"]
    equity_prices = equity_prices[equity_prices["price_id"] <= f"{end_date}{stock}"]

    volatility = np.std(equity_prices["close"] - equity_prices["open"])

    return volatility


def check_policy5(df, limit_dict, con):

    fifty_days_ago = {
        "20211112": "20210923",
        "20211111": "20210922"
    }

    breaches = []

    for index, agg in df.iterrows():
        if agg["Date"] != "2021-11-11" and agg["Date"] != "2021-11-12":
            continue
        if not (agg["Trader"], "VaR", "relative", "PCG") in limit_dict:
            continue

        volatility = np.sqrt(250) * compute_volatility(agg["Symbol"], fifty_days_ago[agg["Date"].replace("-", "")], agg["Date"].replace("-", ""), con)
        volatility_limit = limit_dict[(agg["Trader"], "volatility", "relative", "PCG")]
        if volatility > volatility_limit[0]:
            breaches.append((5, volatility_limit[1], agg['Symbol'], volatility, agg["Date"]))

    return breaches
