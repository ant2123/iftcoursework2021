import datetime

def recreate_table(con):
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS "policy_breaches";')
    sql = """CREATE TABLE "policy_breaches" (
	"policy_breach_id"	INTEGER NOT NULL, -- the unique id
	"policy_type"	INTEGER NOT NULL, -- the type of the policy, i.e., policy 1 is for long/short consideration policy
	"trader_limit_id"	TEXT NOT NULL, -- the trader limit id in the trader_limit table
    "symbol"	TEXT NOT NULL, -- the symbol of the breach
    "breach_value"  NUMERIC NOT NULL, -- the value that breaches the limit
    "breach_date" TEXT NOT NULL, -- the date that the breach happened
	"detect_time"	TEXT NOT NULL, -- the datetime that the breach is found
	PRIMARY KEY("policy_breach_id" AUTOINCREMENT)
);
"""
    cur.execute(sql)

# recreate: whether drop and recreate the table
def save_breaches(breaches, con, recreate=True):

    if recreate:
        recreate_table(con)

    cur = con.cursor()
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if len(breaches) == 0:
        return

    # add the time to breaches
    breaches = [(policy_type, symbol, trader_limit_id, breach_value, breach_date, now) for policy_type, symbol, trader_limit_id, breach_value, breach_date in breaches]

    insert_sql = f"""INSERT INTO "policy_breaches" ("policy_type", "trader_limit_id", "symbol", "breach_value", "breach_date", "detect_time")
VALUES (?,?,?,?,?,?);"""
   
    # print(insert_sql)
    cur.executemany(insert_sql, breaches)

    print('Inserted', cur.rowcount, 'records to the table.')

    con.commit()
