library(RSQLite)

#DB Connection
GITRepoDirectory <- "/Users/ryan_feng/iftcoursework2021" 

conSql <- dbConnect(dbDriver("SQLite"), dbname = "/Users/ryan_feng/iftcoursework2021/000.DataBases/SQL/Equity.db")

#SQL Query
Portfolio_Positions <- dbGetQuery(conSql,paste0("SELECT * FROM portfolio_positions ",
                                                "LEFT JOIN equity_prices ON portfolio_positions.symbol = equity_prices.symbol_id ",
                                                "WHERE portfolio_positions.cob_date = equity_prices.cob_date ",
                                                "ORDER BY 'cob_date'"))

Portfolio_Sectors <- dbGetQuery(conSql,"SELECT * from equity_static")
