
import sqlite3
import pymongo
from sqlalchemy import create_engine
import pandas as pd
import modules.input.SQLquery as query


#connect to sqlite
GITRepoDirectory = "/Users/ryan_feng/"
engine = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()

#retrive 2021/11/11 SQLite data
df1 = pd.read_sql_query(query.price11(),engine)
df1

#retrive 2021/11/12 SQLite data
df2 = pd.read_sql_query(query.price12(),engine)
df2

#connect to mongodb
client = pymongo.MongoClient('localhost',27017)
db = client['equity']
col = db['courseworktwo']


#retrive 2021/11/11 MongoDB data
Firstday = col.find({"DateTime":{ "$regex":"2021-11-11" }})
df3 = pd.DataFrame(Firstday)
df3['Price'] = (df3['Notional']/df3['Quantity'])
df3

#retrive 2021/11/12 MongoDB data
Secondday = col.find({"DateTime":{ "$regex":"2021-11-12" }})
df4 = pd.DataFrame(Secondday)
df4['Price'] = (df4['Notional']/df4['Quantity'])
df4

#merge 2021/11/11 data
df5 = pd.merge(df3,df1,on='Symbol')
df5

#merge 2021/11/12 data
df6= pd.merge(df4,df2,on='Symbol')
df6


#trade_suspect 11/11
df7 = df5.loc[(df5['Price'] < df5['low']) | (df5['Price'] > df5['high'])]
df7

#trade_suspect 11/12
df8 = df6.loc[(df6['Price'] < df6['low']) | (df6['Price'] > df6['high'])]
df8


#total trade_suspect 
df9 = pd.concat([df7,df8])
df9

#createtable
def createtable():
    con.execute("create table IF NOT EXISTS trades_suspects ("
                                     "DateTime TEXT NOT NULL,"
                                     "TradeId TEXT NOT NULL,"
                                     "Symbol TEXT NOT NULL,"
                                     "Quantity INTEGER NOT NULL,"
                                     "Notional INTEGER NOT NULL,"
                                     "TradeType TEXT NOT NULL,"
                                     "Ccy TEXT NOT NULL,"
                                     "Counterparty TEXT NOT NULL,"
                                     "FOREIGN  KEY (Symbol) REFERENCES equity_prices(symbol_id),"
                                     "PRIMARY KEY(TradeId))")

createtable()

#index reassignment
df9.index = range(len(df9))

#insert
for i in range(len(df9)):
    print('[INFO] Loading into trades_suspects',df9.loc[i,'DateTime'],'...\n')
    con.execute(f'INSERT INTO trades_suspects (DateTime, TradeId, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty)                 VALUES ("{df9.loc[i,"cob_date"]}",                "{df9.loc[i,"TradeId"]}",                "{df9.loc[i,"Symbol"]}",                {df9.loc[i,"Quantity"]},                {df9.loc[i,"Notional"]},                "{df9.loc[i,"TradeType"]}",                "{df9.loc[i,"Ccy"]}",                "{df9.loc[i,"Counterparty"]}")')


#data restructure 2021/11/11
df5['pos_id'] = df5['Trader'].astype(str)+'20211111'+df5['Symbol'].astype(str)
df10 = df5.rename(columns={'Trader':'trader', 'Symbol':'symbol', 'Ccy':'ccy', 'Quantity':'net_quantity', 'Notional':'net_amount'})
df11 = df10.groupby(['pos_id','cob_date','trader','symbol','ccy']).agg(net_quantity=('net_quantity','sum'),net_amount=('net_amount','sum')).reset_index()
df11

#append to portfolio_positions
df11.to_sql('portfolio_positions', con, if_exists='append', index=False )


#data restructure 2021/11/12
df6['pos_id'] = df6['Trader'].astype(str)+'20211112'+df6['Symbol'].astype(str)
df12 = df6.rename(columns={'Trader':'trader', 'Symbol':'symbol', 'Ccy':'ccy', 'Quantity':'net_quantity', 'Notional':'net_amount'})
df13 = df12.groupby(['pos_id','cob_date','trader','symbol','ccy']).agg(net_quantity=('net_quantity','sum'),net_amount=('net_amount','sum')).reset_index()
df13

#append to portfolio_positions
df13.to_sql('portfolio_positions', con, if_exists='append', index=False )

# switch off connection
con.close()





