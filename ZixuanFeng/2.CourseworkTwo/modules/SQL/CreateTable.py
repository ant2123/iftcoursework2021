def createtable():
    con.execute("create table IF NOT EXISTS trades_suspects ("
                                     "DateTime TEXT NOT NULL,"
                                     "TradeId TEXT NOT NULL,"
                                     "Symbol TEXT NOT NULL,"
                                     "Quantity INTEGER NOT NULL,"
                                     "Notional INTEGER NOT NULL,"
                                     "TradeType TEXT NOT NULL,"
                                     "Ccy TEXT NOT NULL,"
                                     "Counterparty TEXT NOT NULL,"
                                     "FOREIGN  KEY (Symbol) REFERENCES equity_prices(symbol_id),"
                                     "PRIMARY KEY(TradeId))")


