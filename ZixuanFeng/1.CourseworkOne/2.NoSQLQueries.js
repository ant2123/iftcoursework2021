NoSQLQueries

//Query1:
db.courseworkone.aggregate([{$match: {} }, {$group: {_id: "$StaticData.GICSSector", average_peratio: {$avg: "$FinancialRatios.PERatio"}, average_beta: {$avg: "$MarketData.Beta"}, total_marketcap: {$sum: "$MarketData.MarketCap"} } }, {$sort:{"average_petatio":-1}}])

//Query2:
db.courseworkone.find({$and: [{ "FinancialRatios.PERatio": {"$gte": 30} }, { "FinancialRatios.PayoutRatio": {"$gte": 40} }, { "MarketData.MarketCap": {"$gte": 10000} }, {"StaticData.GICSSector": {"$eq": "Health Care" } } ] } ).sort({"MarketData.MarketCap": -1}).limit(5)

//Query3:
db.courseworkone.find({"StaticData.GICSSector": "Financials"}).limit(5).pretty()