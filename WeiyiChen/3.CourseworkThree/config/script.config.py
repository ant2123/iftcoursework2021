#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Weiyi Chen
# Coursework : Coursework3
# Use Case   : Case Three.b
# File       : script.config
#--------------------------------------------------------------------------------------
GITRepoDirectory = "/Users/chenweiyi/Desktop/python/bigdata"
engine_1 = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine_1.connect()