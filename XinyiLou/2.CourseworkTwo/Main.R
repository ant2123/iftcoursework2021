#-------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : XINYI LOU
# Topic   : Coursework Two - Incorrect Trade Detection
#-------------------------------------------------------------------------------
# Install & Import packages
install.packages("RSQLite")
install.packages("mongolite")
install.packages("dplyr")
install.packages("lubridate")
install.packages("tidyr")
library(RSQLite)
library(mongolite)
library(dplyr)
library(lubridate)
library(tidyr)

# set up directories
GITRepoDirectory <- "C:/Users/57252/Desktop/iftcoursework2021" 

# Loading the R Script with the SQL Queries
source(paste0(GITRepoDirectory, "/XinyiLou/2.CourseworkTwo/config/script.config"))
source(paste0(GITRepoDirectory, Config$Directories$ScriptFolder, "/modules/SQLQueries.R"))

# Connecting to SQLite and MongoDB
conSql <- dbConnect(dbDriver("SQLite"),dbname = "C:/Users/57252/Desktop/000.DataBases/SQL/Equity.db")
conMongo <- mongo(collection = "CourseworkTwo", db = "Equity", url = "mongodb://localhost")

# build pipeline and Import equity prices from SQL
SQLQueryStatic <- paste0("SELECT * FROM equity_prices;")
fullEquityprices <- as.data.frame(dbGetQuery(conSql, SQLQueryStatic))

# rename the column
colnames(fullEquityprices)[9] <- "Symbol"

# pick up all data on 11.11 & 11.12
subset(fullEquityprices,cob_date=="11-Nov-2021") 
subset(fullEquityprices,cob_date=="12-Nov-2021") 

# subset the data with useful infromation
a1<- subset(fullEquityprices,cob_date=="11-Nov-2021",select=c(Symbol,cob_date,high,low))
a2<- subset(fullEquityprices,cob_date=="12-Nov-2021",select=c(Symbol,cob_date,high,low))

# Import data from MongoDB
fullUniverse <- conMongo$find(query = "{}")

# substring the date
fullUniverse$date<-substring(fullUniverse$DateTime,9,18) 

# create and calculate a new value called notional price
fullUniverse$Notionalprice<- fullUniverse$Notional/fullUniverse$Quantity
colnames(fullUniverse)[10] <- "cob_date"

# pick up all data on 11.11 & 11.12
subset(fullUniverse,cob_date=="2021-11-11") 
subset(fullUniverse,cob_date=="2021-11-12") 

# then subset them with useful information in a new table
a3<- subset(fullUniverse,cob_date=="2021-11-11",select=c(TradeId,cob_date,Symbol,Notionalprice))
a4<- subset(fullUniverse,cob_date=="2021-11-12",select=c(TradeId,cob_date,Symbol,Notionalprice))
a5<- right_join(a1,a3,by="Symbol")
a6<- right_join(a2,a4,by="Symbol")

# subset the abnormal trades on 11.11
b1<- subset(a5,low>=Notionalprice)
b2<- subset(a5,high<=Notionalprice)
b3<- full_join(b1,b2)

# subset the abnormal trades on 11.12
b4<- subset(a6,low>=Notionalprice)
b5<- subset(a6,high<=Notionalprice)
b6<- full_join(b4,b5)

# combine all abnormal trades on 11.11 & 11.12
trade_outliers<- full_join(b3,b6)
trade_outliers<- subset(trade_outliers,select = -c(cob_date.x))
colnames(trade_outliers)[5] <- "cob_date"

# create table in SQL and input my primary & foreign key
dbExecute(conSql,"
CREATE TABLE if not exists 'trades_suspects'
(
  TradeId TEXT PRIMARY KEY,
  Notionalprice INTEGER NOT NULL,
  high REAL NOT NULL,
  low REAL NOT NULL,
  Symbol TEXT NOT NULL,
  cob_date NOT NULL,
  FOREIGN KEY(Symbol) REFERENCES equity_prices(symbol_id)
)")

# insert data into the table
dbWriteTable(conSql, "trades_suspects", trade_outliers, row.names=FALSE, append=TRUE)

# rename column and subset useful data on 11.11
colnames(fullEquityprices)[7] <- "ccy"
c1<- subset(fullEquityprices,cob_date=="11-Nov-2021",select=c(Symbol,cob_date,ccy))
c2<- subset(fullUniverse,cob_date=="2021-11-11",select=c(Symbol,Trader,Notional,Quantity))
c3<- right_join(c1,c2,by="Symbol")

# aggregate quantity and notional
c3 <- aggregate(cbind(Quantity, Notional)~cob_date+Trader+Symbol+ccy, 
                       data=c3, 
                       FUN=sum)

# rename columns
colnames(c3)[3] <- "symbol"
colnames(c3)[2] <- "trader"
colnames(c3)[6] <- "net_amount"
colnames(c3)[5] <- "net_quantity"

# create new column called pos_id
c3$pos_id <- paste0(c3$trader,20211111, c3$symbol)

# subset useful data on 11.12
c4<- subset(fullEquityprices,cob_date=="12-Nov-2021",select=c(Symbol,cob_date,ccy))
c5<- subset(fullUniverse,cob_date=="2021-11-12",select=c(Symbol,Trader,Notional,Quantity))
c6<- right_join(c4,c5,by="Symbol")

# aggregate quantity and notional
c6 <- aggregate(cbind(Quantity, Notional)~cob_date+Trader+Symbol+ccy, 
                data=c6, 
                FUN=sum)

# rename columns
colnames(c6)[3] <- "symbol"
colnames(c6)[2] <- "trader"
colnames(c6)[6] <- "net_amount"
colnames(c6)[5] <- "net_quantity"
c6$pos_id <- paste0(c6$trader,20211112, c6$symbol)

# resort the columns
d1<- c3 %>% select(pos_id,cob_date, trader, symbol, ccy,net_quantity,net_amount)
d2<- c6 %>% select(pos_id,cob_date, trader, symbol, ccy,net_quantity,net_amount)

# combine 2 tables and insert it into SQL
d3<- rbind(d1,d2)
dbWriteTable(conSql, "portfolio_positions", d3,row.names=FALSE,append = TRUE)
