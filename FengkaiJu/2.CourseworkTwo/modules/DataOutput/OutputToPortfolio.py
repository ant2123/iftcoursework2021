import pymongo
import sqlite3

month=["Jan","Feb",'Mar','Apr','May','Jun','Jul','Aug','Sept',"Oct",'Nov',"Dec"]
monchange={}
for i in range(12):
    monchange["%02d"%(i+1)]=month[i]

#change the form of the data.
def rename(aggregated_data,cob_date):
    month=["Jan","Feb",'Mar','Apr','May','Jun','Jul','Aug','Sept',"Oct",'Nov',"Dec"]
    month_change={}
    for i in range(12):
        month_change["%02d"%(i+1)]=month[i]

    keys={"trader","symbol","ccy"}
    ID=aggregated_data["_id"]
    trader=ID["trader"]
    symbol=ID["symbol"]
    ccy=ID["ccy"]

    cob_date=cob_date.split("-")
    pos_id=trader+"".join(cob_date)+symbol
    cob_date=cob_date[2]+'-'+month_change[cob_date[1]]+"-"+cob_date[0]

    values=(pos_id,cob_date,trader,symbol,ccy,aggregated_data['net_quantity'],aggregated_data['net_notional'])

    return values

def outputToPortfolio(aggregated_data,cob_date,cursor):
    values=rename(aggregated_data,cob_date)
    base="Insert INTO portfolio_positions (pos_id,cob_date,trader,symbol,ccy,net_quantity,net_amount) values "
    cursor.execute(base+str(values))

