import pymongo

def loadMongoDB():
    my_client = pymongo.MongoClient("mongodb://localhost:27017/")
    trade_DB=my_client['equity']
    trade_DB=trade_DB["CourseworkTwo"]
    return trade_DB

def queryMongoDB(database,matches):
    trades=database.find(matches)
    total=200
    data=[]
    for i in range(total):
        data.append(trades[i])

    return data
