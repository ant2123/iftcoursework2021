import sqlite3


def saveSuspeciousTrades(error_list):
    conn = sqlite3.connect('trades_suspect.db')
    c=conn.cursor()
    c.execute('''Create Table SuspectTrades
                (TradeId Text PRIMARY KEY     NOT NULL,
                DateTime Text NOT NULL,
                Trader Text NOT NULL,
                Symbol Text NOT NULL,
                Quantity Int NOT NULL,
                Notional Int NOT NULL,
                TradeType Text NOT NULL,
                Ccy Text NOT NULL,
                Counterparty Text NOT NULL
                );''')

    keys=('TradeId','DateTime','Trader','Symbol','Quantity','Notional','TradeType','Ccy','Counterparty')
    for error in error_list:
        values=[error[i] for i in keys]
        # print(type(values))
        # print("Insert INTO SuspectTrades (TradeId,DateTime,Trader,Symbol,Quantity,Notional,TradeType,Ccy,Counterparty) Values "+str(tuple(values)))
        c.execute("Insert INTO SuspectTrades (TradeId,DateTime,Trader,Symbol,Quantity,Notional,TradeType,Ccy,Counterparty) Values "+str(tuple(values)))

    conn.commit()
