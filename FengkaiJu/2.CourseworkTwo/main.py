import pymongo
import sqlite3
import os

from modules.DataImport.LoadFromMongoDB import loadMongoDB,queryMongoDB
from modules.DataProcessing.FindSuspeciousTrades import findSuspeciousTrade
from modules.db.SQL.CreatTable import saveSuspeciousTrades

from modules.DataProcessing.Aggregate import aggregateData
from modules.DataOutput.OutputToPortfolio import outputToPortfolio

#remove the existing database to avoid a conflict in name and table name
try:
    os.remove("trades_suspect.db")
except:
    1

#load data
trade_DB=loadMongoDB()
conn = sqlite3.connect('equity.db')

query11={'DateTime':{ "$regex": "^ISODate\(2021-11-11" }}
query12={'DateTime':{ "$regex": "^ISODate\(2021-11-12" }}

trade_11=queryMongoDB(trade_DB,query11)#all trades in 2021-11-11, list form. Each element is a dict
trade_12=queryMongoDB(trade_DB,query12)#all trades in 2021-11-12, list form.

suspecious_trades_11=findSuspeciousTrade(trade_11,conn,'11-Nov-2021')#suspecious trades in 2021-11-11, date are in different forms, i.e., 11-Nov-2021 and 2021-11-11
suspecious_trades_12=findSuspeciousTrade(trade_12,conn,'12-Nov-2021')

suspecious_trades=suspecious_trades_11+suspecious_trades_12# all the suspecious trades. list form

saveSuspeciousTrades(suspecious_trades)#save all the suspecious trades



cursor=conn.cursor()

#aggregate parts, for the last question
aggregated_data=aggregateData(trade_DB,'2021-11-11')
for i in aggregated_data:
    outputToPortfolio(i,'2021-11-11',cursor)

aggregated_data=aggregateData(trade_DB,'2021-11-12')
for i in aggregated_data:
    outputToPortfolio(i,'2021-11-12',cursor)

conn.commit()
