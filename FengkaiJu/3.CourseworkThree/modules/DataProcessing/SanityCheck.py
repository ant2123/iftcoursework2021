import pymongo
import sqlite3
import datetime

def sanityCheck(trade):
    try:
        #conn = sqlite3.connect('D:/equity.db')
        month = ["Jan", "Feb", 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', "Oct", 'Nov', "Dec"]
        conn = sqlite3.connect('../../000.DataBases/SQL/equity.db')

        date=trade.DateTime
        date=date.split('(')[1]
        date=date[:10].split('-')
        yesterday = datetime.datetime(int(date[0]), int(date[1]), int(date[2])) - datetime.timedelta(1)
        date_year = yesterday.year
        date_mon = month[yesterday.month - 1]
        date_day = yesterday.day
        date = '%02d' % (date_day) + '-' + date_mon + '-' + str(date_year)

        base_Sql = "select * from equity_prices where cob_date='" + date + "' and symbol_id='"

        trade_info = trade.Symbol + "'"
        restrict = conn.execute(base_Sql + trade_info)
        restrict = restrict.fetchall()

        if len(restrict)==0:
            return True
        restrict = restrict[0]
        highest = restrict[2]*5.0
        lowest = restrict[3]/5.0
        avgprice = float(trade.Notional) / float(trade.Quantity)
        if (avgprice - highest) * (avgprice - lowest) > 0:

            return False
        else:
            return True
    except:
        return False

