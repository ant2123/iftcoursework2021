#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : Main.py
#--------------------------------------------------------------------------------------

import modules.db.NoSQL.NoSQLDataLoad as nsdl
import modules.db.SQL.SQLDataLoad as sdl
import modules.db.SQL.SQLDataWrite as sdw
import modules.db.SQL.CreateTable as ct
import modules.Utils.DataUtils as du
import modules.Utils.FuntionUtils as fu
import modules.Utils.PortfolioUtils as pu
import modules.Utils.ScriptUtils as su

def main(sql_path, database, collection, detect_columns, detect_dates):
    print("connecting to SQL ------")
    # sql_path = 'D:/Bitbucket/iftcoursework2021/000.DataBases/SQL/Equity.db'
    con_sql = sdl.connect_sqlite(sql_path)

    print("connecting to NoSQL ------")
    # database = 'Equity'
    # collection = 'CourseworkTwo'
    con_nosql = nsdl.connect_mongo()
    df_nosql = nsdl.read_mongo(con_nosql, database, collection)

    print("data pre-processing ------")
    df_nosql = du.calculate_trade_price(df_nosql, 'TradePrice')
    df_nosql = du.calculate_date_time_stamp(df_nosql, 'DateTimeStamp')
    df_nosql = du.calculate_date_time_str(df_nosql, 'DateTimeStr')

    print("testing and merging the results ------")
    # detect_columns = ['Quantity', 'TradePrice']
    # detect_dates = ['2021-11-11', '2021-11-12']
    trades_suspects = fu.merge_suspects(df_nosql, detect_columns, detect_dates, con_sql)

    print("creating trades_suspects and writing to sql ------")
    ct.create_suspect(con_sql, ct.create_suspect_query_sqlite())
    sdw.write_sqlite(trades_suspects, 'trades_suspects', con_sql)

    print("aggregating and wirting into portfolio_positions ------")
    new_portfolio_positions = pu.new_portfolio(con_sql, con_nosql, database, collection)
    sdw.write_sqlite(new_portfolio_positions, 'portfolio_positions', con_sql)

    print("closing connections ------")
    con_sql.close()
    con_nosql.close()

    print("done")

if __name__ == '__main__':
    sql_path, database, collection = su.read_config()
    detect_columns, detect_dates = su.read_params()

    main(sql_path, database, collection, detect_columns, detect_dates)