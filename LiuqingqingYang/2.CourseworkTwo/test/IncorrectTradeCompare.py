#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./test/IncorrectTradeCompare.py
#--------------------------------------------------------------------------------------

import pandas as pd
import time

import modules.db.SQL.SQLDataLoad as sdl

# detect incorrect trades by comparing data in mongodb and sqlite
def find_compare(data, date, conn):

    data = data[data['DateTimeStamp']<=pd.to_datetime(date)]
    trades = data.loc[data['DateTime'].str.contains(date)]

    date_sql = time.strftime("%d-%b-%Y", time.strptime(date,"%Y-%m-%d"))
    query_sql = "SELECT * FROM equity_prices WHERE cob_date='"+date_sql+"'"
    real_prices = sdl.read_sqlite(conn, query_sql)

    outliers_all = pd.DataFrame()
    for i in trades.index:
        symbol_id = trades.loc[i]['Symbol']
        high = real_prices[real_prices['symbol_id']==symbol_id]['high'].values[0]
        low = real_prices[real_prices['symbol_id']==symbol_id]['low'].values[0]
        if (trades.loc[i]['TradePrice']<low) | (trades.loc[i]['TradePrice']>high):
            outliers_all = outliers_all.append(trades.loc[[i]])

    return outliers_all