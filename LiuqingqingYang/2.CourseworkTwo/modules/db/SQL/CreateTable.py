#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/db/SQL/CreateTable.py
#--------------------------------------------------------------------------------------

def create_suspect_query_sqlite():
    query = "CREATE TABLE trades_suspects (\
    SuspectId TEXT PRIMARY KEY,\
    PriceId TEXT,\
    _id TEXT,\
    DateTime TEXT,\
    TradeId TEXT,\
    Trader TEXT,\
    Symbol TEXT,\
    Quantity INTEGER,\
    Notional REAL,\
    TradeType TEXT,\
    Ccy TEXT,\
    Counterparty TEXT,\
    TradePrice REAL,\
    FOREIGN KEY (PriceId) REFERENCES equity_prices(price_id),\
    FOREIGN KEY (Trader) REFERENCES trader_static(trader_id),\
    FOREIGN KEY (Symbol) REFERENCES equity_static(symbol))"
    return (query)

def create_suspect(conn, query=''):
    try:
        cu = conn.cursor()
        cu.execute(query)
    except:
        print('Table trades_suspects already exists.')
    print('Table created.')