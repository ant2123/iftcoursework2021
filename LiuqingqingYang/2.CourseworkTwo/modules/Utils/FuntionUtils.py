#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/Utils/FunctionUtils.py
#--------------------------------------------------------------------------------------

import pandas as pd

import test.IncorrectTradeTest as itt
import test.IncorrectTradeCompare as itc


def merge_suspects(data, columns, dates, conn):
    trades_suspects = pd.DataFrame()

    for column, date in zip(columns, dates):
        trades_suspects = trades_suspects.append(itt.find_zcore(data, column, date))
        trades_suspects = trades_suspects.append(itt.find_irq(data, column, date))
        trades_suspects = trades_suspects.append(itt.find_dbscan(data, column, date))
        trades_suspects = trades_suspects.append(itc.find_compare(data, date, conn))

#     trades_suspects['SuspectId'] = trades_suspects['DateTimeStr']+trades_suspects['Trader']+trades_suspects['Symbol']
    trades_suspects.insert(0,'PriceId',trades_suspects['DateTimeStr']+trades_suspects['Symbol']) 
    trades_suspects.insert(0,'SuspectId',trades_suspects['DateTimeStr']+trades_suspects['Trader']+trades_suspects['Symbol']) 
    trades_suspects = trades_suspects.drop_duplicates(['_id']).reset_index(drop=True).drop(['DateTimeStamp','DateTimeStr'],axis=1)
    
#     trades_suspects = trades_suspects.drop_duplicates(['_id']).reset_index(drop=True).drop(['TradePrice','DateTimeStamp'],axis=1)
    
    return trades_suspects