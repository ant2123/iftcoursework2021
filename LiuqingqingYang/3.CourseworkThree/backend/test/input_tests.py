import numpy as np
import pandas as pd
from sklearn import cluster
from sklearn import preprocessing

# Z-Score
def find_zcore(historical_data):   
    
    data_zscore = np.array(historical_data.copy())
    
    z_score = (data_zscore - data_zscore.mean()) / data_zscore.std()
#     data_zscore = z_score.abs() > 3.0
    data_zscore = abs(z_score) > 3.0
    
    outliers_all = np.array(historical_data)[data_zscore == True]

    return pd.DataFrame(outliers_all)

# IRQ
def find_irq(historical_data):
    
    outliers_all = []

    data_std = np.std(historical_data)
    data_mean = np.mean(historical_data)
    lower_limit = data_mean - data_std*3
    upper_limit = data_mean + data_std*3

    for i in range(len(historical_data)):
        quantity = historical_data[i]
        if (quantity < lower_limit)|(quantity > upper_limit):
            outliers_all.append(quantity)

    return pd.DataFrame(outliers_all)

# dbscan
def find_dbscan(historical_data):
    
    data_dbscan = np.array(preprocessing.scale(historical_data)).reshape(-1,1)
    
    dbscan = cluster.DBSCAN(eps = 0.8, min_samples = 7)
    dbscan.fit(data_dbscan)
    
    outliers_all = pd.DataFrame()
    outliers_all = outliers_all.append(historical_data)
    outliers_all['Cluster'] = dbscan.labels_

    outliers_all = outliers_all[outliers_all['Cluster']==-1].drop(['Cluster'], axis=1)
    
    return outliers_all


