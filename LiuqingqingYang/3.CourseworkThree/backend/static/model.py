from pydantic import BaseModel

class Trade(BaseModel):
    DateTime: str #?
    TradeId: str
    Trader: str
    Symbol: str
    Quantity: int
    Notional: float #?
    TradeType: str
    Ccy: str
    Counterparty: str
