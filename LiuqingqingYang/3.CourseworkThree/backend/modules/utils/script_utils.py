import configparser

# read script.config
def read_config():
    conf = configparser.ConfigParser()
    conf.read(".\config\script.config")
    conn_str = conf.get("config","conn_str")
    database = conf.get("config","database")
    collection =conf.get("config","collection")
    return conn_str, database, collection
