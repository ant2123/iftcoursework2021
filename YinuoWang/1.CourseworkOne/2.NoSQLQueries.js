// Use distinct() to show the sectors of this dataset. 
> db.CourseworkOne.distinct("StaticData.GICSSector")

// Group by the GICSSector and sort to compare the average Beta of each sector.
> db.CourseworkOne.aggregate([
    {$match: {} },{$group: {_id: "$StaticData.GICSSector", 
    average: {$avg: "$MarketData.Beta"} }},{"$sort":{average:-1}}]) 

// Work on the financial sector. Find those company with PERatio greater than 25, and sort them by MarketCap.
> db.CourseworkOne.find({$and: [{"FinancialRatios.PERatio": {"$gte": 40} }, 
{"StaticData.GICSSector": {"$eq": "Information Technology" } } ] } ).sort({"MarketData.MarketCap":-1})

// Focus on the Financial company. Group by the subindustry and calculate the total MarketCap.  
> db.CourseworkOne.aggregate([
    {$match: {"StaticData.GICSSector": "Information Technology"} },
    {$group: {_id: "$StaticData.GICSSubIndustry", total: {$sum: "$MarketData.MarketCap"} } }])


