# ---------------------------------------------------------------------------------
#
# Server
#
# ---------------------------------------------------------------------------------

# Define server logic --------------------------------------------

server <- function(input, output) {

    output$table1 <- DT::renderDataTable(DT::datatable({
        data <- exposuresBySector
        if (input$trader1 != "All") {
            data <- data[data$trader == input$trader1,]
        }
        if (input$dates1 != "All") {
            data <- data[data$date == input$dates1,]
        }
        data
    }))

    dat1 <- reactive({
        data <- portfolioPlot
        if (input$trader3 != "All") {
            data <- data[data$trader == input$trader3,]
        }
        if (input$date3 != "All") {
            data <- data[data$date == input$date3,]
        }
        data
    })

    output$portfolioPlot <- renderPlot({
            ggplot(dat1(),aes(x=GICSSector, y=amount))+geom_bar(stat="identity", fill = 'orange', width = 0.5)+labs(x="GICSSector",y="Amount")+theme(axis.title=element_text(size=15,face="bold"),axis.text.x=element_text(angle=30,size=10))},height=400,width=580
    )

    dat2 <- reactive({
        data <- exposuresPlot
        if (input$trader4 != "All") {
            data <- data[data$trader == input$trader4,]
        }
        if (input$date4 != "All") {
            data <- data[data$date == input$date4,]
        }
        data
    })

    output$exposurePlot <- renderPlot({
        validate(
            need(length(dat2()$exposure)>=1,"Sorry, there is no trade today.", FALSE) 
        )
        data <- dat2()
        ggplot(dat2(),aes(x="", y=exposure, fill=GICSSector))+geom_bar(width=1,stat = "identity")
    })

    output$table2 <- DT::renderDataTable(DT::datatable({
        data <- portfolioPositions
        if (input$trader2 != "All") {
            data <- data[data$trader == input$trader2,]
        }
        if (input$dates2 != "All") {
            data <- data[data$date == input$dates2,]
        }
        data
    }))

}

