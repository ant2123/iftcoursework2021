# %% loading the errors to the table
for i in range(len(All_suspects)):
    print('[INFO] Loading into trades_suspects',All_suspects.loc[i,'Symbol'],'...\n')
    con.execute(f'INSERT INTO trades_suspects (Symbol, Price, low, high, cob_date, Trader) \
                VALUES ("{All_suspects.loc[i,"Symbol"]}",\
                {All_suspects.loc[i,"Price"]},\
                {All_suspects.loc[i,"low"]},\
                {All_suspects.loc[i,"high"]},\
                "{All_suspects.loc[i,"cob_date"]}",\
                "{All_suspects.loc[i,"Trader"]}")')
# %% inserting into portfolio_positions table
for i in range(len(group)):
    print('[INFO] Loading into portfolio_positions',group.loc[i,'Trader'],'...\n')
    con.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) \
                VALUES ("{group.loc[i,"pos_id"]}",\
                "{group.loc[i,"cob_date"]}",\
                "{group.loc[i,"Trader"]}",\
                "{group.loc[i,"Symbol"]}",\
                "{group.loc[i,"Ccy"]}",\
                {group.loc[i,"Quantity"]},\
                {group.loc[i,"Notional"]})')
