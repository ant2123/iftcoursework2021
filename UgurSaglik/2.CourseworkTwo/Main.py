

#%% importing packages
import pymongo
#%%
from pymongo import MongoClient
#%%
import pandas as pd
#%%
import numpy as np
#%%
from sklearn.cluster import KMeans
#%%
from sqlalchemy import create_engine
#%%
import datetime

#%% Installing pymongo
pip install pymongo
#%%
pip install "pymongo[srv]"

#%% connecting MongoDB and SQL
cluster = MongoClient("mongodb+srv://ugurdb:Fenerbahce2539*@cluster0.90gxs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["Equity"]
collection = db["CourseworkTwo"]

#%%
engine = create_engine(f"sqlite:///C:/Users/ugisa/OneDrive/Desktop/New folder (2)/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()


#%% read json and read database
november11 = collection.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-11T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-11T23:59:59.000Z"}}]})
november12 = collection.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-12T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-12T23:59:59.000Z"}}]})
november11 = pd.DataFrame(november11)
november12 = pd.DataFrame(november12)

def SQLQuery_11Nov():
    return ("SELECT low, high, symbol_id as Symbol, cob_date FROM equity_prices WHERE cob_date = '11-Nov-2021'")
def SQLQuery_12Nov():
    return ("SELECT low, high, symbol_id AS Symbol, cob_date FROM equity_prices WHERE cob_date = '12-Nov-2021'")
SQL_11Nov = pd.read_sql_query(SQLQuery_11Nov(),engine)
SQL_12Nov = pd.read_sql_query(SQLQuery_12Nov(),engine)

#%% Equity price calculation for each trade 11/11 , 11/12
november11["Price"] = (november11["Notional"] / november11["Quantity"])
november12["Price"] = (november12["Notional"] / november12["Quantity"])

#%% mongodb - sql merge 11/nov and 12/nov
merge_nov11 = pd.merge(SQL_11Nov, november11, on=["Symbol"])
merge_nov12 = pd.merge(SQL_12Nov, november12, on=["Symbol"])

# %% suspect condition price<low or price>high 11/11 12/11
condition = [(merge_nov11['Price'] < merge_nov11['low']), 
             (merge_nov11['Price'] > merge_nov11['high'])]
value = ['suspect', 'suspect']
merge_nov11['check'] = np.select(condition, value)

condition = [(merge_nov12['Price'] < merge_nov12['low']), 
             (merge_nov12['Price'] > merge_nov12['high'])]
value = ['suspect', 'suspect']
merge_nov12['check'] = np.select(condition, value)

#%% incorrect prices 11/11 12/11
november_11 = merge_nov11['check']=='suspect'
suspect_Nov_11 = merge_nov11[november_11]
november_12 = merge_nov12['check']=='suspect'
suspect_Nov_12 = merge_nov12[november_12]

# %% incorrect prices combined
All_suspects = pd.concat([suspect_Nov_11, suspect_Nov_12])
All_suspects.index = range(len(All_suspects))
All_suspects

# %% create table trades_suspects
def SQLCreateRetTable():
    Command = "CREATE TABLE trades_suspects (\
    Symbol TEXT NOT NULL,\
    Price INTEGER,\
    low INTEGER,\
    high INTEGER,\
    cob_date TEXT,\
    Trader TEXT,\
    FOREIGN KEY (Price) REFERENCES equity_prices (symbol_id))"
    return (Command)

# %% execute table
con.execute(SQLCreateRetTable())

# %% addind the errors to the table
for i in range(len(All_suspects)):
    print('[INFO] Loading into trades_suspects',All_suspects.loc[i,'Symbol'],'...\n')
    con.execute(f'INSERT INTO trades_suspects (Symbol, Price, low, high, cob_date, Trader) \
                VALUES ("{All_suspects.loc[i,"Symbol"]}",\
                {All_suspects.loc[i,"Price"]},\
                {All_suspects.loc[i,"low"]},\
                {All_suspects.loc[i,"high"]},\
                "{All_suspects.loc[i,"cob_date"]}",\
                "{All_suspects.loc[i,"Trader"]}")')

# %% creating pos id
''
merge_nov12 ['pos_id'] = merge_nov12 ['Trader'].astype(str) + '20211112' + merge_nov12 ['Symbol']
merge_nov11  ['pos_id'] = merge_nov11  ['Trader'].astype(str) + '20211111' + merge_nov11 ['Symbol']

# %% concat nov11 and nov12
merge_pos_id = pd.concat([merge_nov11, merge_nov12])

merge_pos_id 
# %% groupby and aggregation 

group = merge_pos_id.groupby(['pos_id', 'cob_date', 'Trader', 'Symbol', 'Ccy',]).agg(Quantity = ('Quantity','sum'),Notional = ('Notional', 'sum')).reset_index()

group
# %% inserting into portfolio_positions table
for i in range(len(group)):
    print('[INFO] Loading into portfolio_positions',group.loc[i,'Trader'],'...\n')
    con.execute(f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) \
                VALUES ("{group.loc[i,"pos_id"]}",\
                "{group.loc[i,"cob_date"]}",\
                "{group.loc[i,"Trader"]}",\
                "{group.loc[i,"Symbol"]}",\
                "{group.loc[i,"Ccy"]}",\
                {group.loc[i,"Quantity"]},\
                {group.loc[i,"Notional"]})')

