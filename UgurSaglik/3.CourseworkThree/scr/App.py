#%% Import
from fastapi import FastAPI
from pydantic import BaseModel
from fastapi import APIRouter
from modules.db.db_mongo import collection_name
from bson import ObjectId
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sqlalchemy import create_engine
import datetime
from modules.db.db_sql import engine
# %% create an instance of the first api
app = FastAPI()
#%% creating a class
class Trade(BaseModel):
    DateTime: str
    TradeId: str
    Trader: str
    Symbol: str
    Quantity: int
    Notional: int
    TradeType: str
    Ccy: str
    Counterparty: str
# %% Returning the dictionary
def trade_serializer(trade):
    return {
        "id": str(trade["_id"]),
        "DateTime": trade["DateTime"],
        "TradeId": trade["TradeId"],
        "Trader": trade["Trader"],
        "Symbol": trade["Symbol"],
        "Quantity": trade["Quantity"],
        "Notional": trade["Notional"],
        "TradeType": trade["TradeType"],
        "Ccy": trade["Ccy"],
        "Counterparty": trade["Counterparty"]
    }
# %% Returning the list 
def trades_serializer(trades):
    return [trade_serializer(trade) for trade in trades]
# %% create api route object 
trade_api_router = APIRouter()
# %% Retrieve trade
@trade_api_router.get("/")
async def get_trades():
    trades = trades_serializer(collection_name.find())
    return {"status": "ok", "data": trades}
# %% Retrieve trade
@trade_api_router.get("/{id}")
async def get_trade(id: str):
    trade = trades_serializer(collection_name.find({"_id": ObjectId(id)}))
    return {"status": "ok", "data": trade}
# %% Post trade
@trade_api_router.post("/")
async def post_trade(trade: Trade):
    _id = collection_name.insert_one(dict(trade))
    trade = trades_serializer(collection_name.find({"_id": _id.inserted_id}))
    return {"status": "ok", "data": trade}
# %% update trade
@trade_api_router.put("/{id}")
async def update_trade(id: str, trade: Trade):
    collection_name.find_one_and_update({"_id": ObjectId(id)}, {
        "$set": dict(trade)
    })
    trade = trades_serializer(collection_name.find({"_id": ObjectId(id)}))
    return {"status": "ok", "data": trade}
# %% delete trade
@trade_api_router.delete("/{id}")
async def delete_trade(id: str):
    collection_name.find_one_and_delete({"_id": ObjectId(id)})
    return {"status": "ok", "data": []}
# %% include router
app.include_router(trade_api_router)
#%%
# %% Queries for November 11 and 12
november11 = collection_name.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-11T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-11T23:59:59.000Z"}}]})
november12 = collection_name.find({"$and" : [{"DateTime" : {"$gte" : "ISODate(2021-11-12T00:00:00.000Z)"}}, {"DateTime" : {"$lte" : "ISODate(2021-11-12T23:59:59.000Z"}}]})
#%% Data transfer into a dataframe
november11 = pd.DataFrame(november11)
november12 = pd.DataFrame(november12)
# %% November 11 and 12 SQL Queries
def SQLQuery_11Nov():
    return ("SELECT low, high, symbol_id as Symbol, cob_date FROM equity_prices WHERE cob_date = '11-Nov-2021'")
def SQLQuery_12Nov():
    return ("SELECT low, high, symbol_id AS Symbol, cob_date FROM equity_prices WHERE cob_date = '12-Nov-2021'")
#%% Importing the price ranges from SQL
SQL_11Nov = pd.read_sql_query(SQLQuery_11Nov(),engine)
SQL_12Nov = pd.read_sql_query(SQLQuery_12Nov(),engine)

# %% Calculating equity prices
november11["Price"] = (november11["Notional"] / november11["Quantity"])
november12["Price"] = (november12["Notional"] / november12["Quantity"])
# %% merging SQL and MongoDB
merge_nov11 = pd.merge(SQL_11Nov, november11, on=["Symbol"])
merge_nov12 = pd.merge(SQL_12Nov, november12, on=["Symbol"])
# %% setting conditions to find out the suspects
condition = [(merge_nov11['Price'] < merge_nov11['low']), 
             (merge_nov11['Price'] > merge_nov11['high'])]
value = ['suspect', 'suspect']
merge_nov11['check'] = np.select(condition, value)

condition = [(merge_nov12['Price'] < merge_nov12['low']), 
             (merge_nov12['Price'] > merge_nov12['high'])]
value = ['suspect', 'suspect']
merge_nov12['check'] = np.select(condition, value)
# %% Incorrect prices for Nov 11 and 12
november_11 = merge_nov11['check']=='suspect'
suspect_Nov_11 = merge_nov11[november_11]
november_12 = merge_nov12['check']=='suspect'
suspect_Nov_12 = merge_nov12[november_12]
# %% Displaying all the suspects
All_suspects = pd.concat([suspect_Nov_11, suspect_Nov_12])
All_suspects.index = range(len(All_suspects))
All_suspects