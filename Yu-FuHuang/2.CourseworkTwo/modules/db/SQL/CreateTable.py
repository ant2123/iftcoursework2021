
query = 'CREATE TABLE "policy_breaches" (\
"index" INTEGER,\
  "_id" TEXT,\
  "Symbol" TEXT,\
  "NetQuantity" REAL,\
  "Trader" TEXT,\
  "price_id" TEXT,\
  "open" REAL,\
  "high" REAL,\
  "low" REAL,\
  "close" REAL,\
  "volume" INTEGER,\
  "currency" TEXT,\
  "cob_date" TEXT\
)'\