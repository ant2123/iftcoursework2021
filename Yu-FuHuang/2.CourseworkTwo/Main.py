import pymongo
import sqlite3
import numpy as np
import pandas as pd

connection1 = pymongo.MongoClient(host='localhost', port=27017)
collection = connection1.Equity.CourseworkTwo
df1 =  pd.DataFrame(list(collection.find()))
# df1

connection2 = sqlite3.connect('C:/Users/USER/Desktop/banking/big data/Equity.db')
cursor = connection2.cursor()

portfolio_positions_1110 = pd.DataFrame(list(cursor.execute("SELECT * FROM portfolio_positions WHERE cob_date='10-Nov-2021'")))
portfolio_positions_1110.columns = [i[0] for i in cursor.description]
# portfolio_positions_1110

df1_1111 = df1.loc[df1['DateTime'].str.contains('2021-11-11')]
df1_1110 = df1.loc[df1['DateTime'].str.contains('2021-11-10')]
net_quantity_1111 = pd.merge(df1_1110, df1_1111[['Symbol','Quantity','TradeType']], on='Symbol')
# net_quantity_1111
net_quantity_1111_buy = net_quantity_1111[net_quantity_1111['TradeType_y']=='BUY']
net_quantity_1111_sell = net_quantity_1111[net_quantity_1111['TradeType_y']=='SELL']
net_quantity_1111_buy['NetQuantity'] = net_quantity_1111_buy['Quantity_x']+net_quantity_1111_buy['Quantity_y']
# net_quantity_1111_buy

net_quantity_1111_sell['NetQuantity'] = net_quantity_1111_sell['Quantity_x']-net_quantity_1111_sell['Quantity_y']
# net_quantity_1111_sell

net_quantity_1111 = net_quantity_1111_buy.append(net_quantity_1111_sell)
#net_quantity_1111

equity_prices_1111 = pd.DataFrame(list(cursor.execute("SELECT * FROM equity_prices WHERE cob_date='11-Nov-2021'")))
equity_prices_1111.columns = [i[0] for i in cursor.description]
# equity_prices_1111

equity_prices_1111 = equity_prices_1111.rename(columns = {"symbol_id":"Symbol"})
# equity_prices_1111

compare_1111 = pd.merge(net_quantity_1111[['_id','Symbol','NetQuantity','Trader']], equity_prices_1111, on='Symbol')
# compare_1111

# MRH5231:5% Volume
compare_1111_MRH = compare_1111[compare_1111['Trader']=='MRH5231']
result1 = compare_1111_MRH[compare_1111_MRH['volume']*0.05<compare_1111_MRH['NetQuantity']]
# result1

# JBX1566:125% Volume
compare_1111_JBX = compare_1111[compare_1111['Trader']=='JBX1566']
result2 = compare_1111_JBX[compare_1111_JBX['volume']*1.25<compare_1111_JBX['NetQuantity']]
# result2

# DGR1983:50% Volume
compare_1111_DGR = compare_1111[compare_1111['Trader']=='DGR1983']
result3 = compare_1111_DGR[compare_1111_DGR['volume']*0.5<compare_1111_DGR['NetQuantity']]
# result3

#  DHB1075:125%
compare_1111_DHB = compare_1111[compare_1111['Trader']=='DHB1075']
result4 = compare_1111_DHB[compare_1111_DHB['volume']*1.25<compare_1111_DHB['NetQuantity']]
# result4

# SML1458:125%
compare_1111_SML = compare_1111[compare_1111['Trader']=='SML1458']
result5 = compare_1111_SML[compare_1111_SML['volume']*1.25<compare_1111_SML['NetQuantity']]
# result5

# SDMZ1796:20%
compare_1111_SDMZ = compare_1111[compare_1111['Trader']=='SDMZ1796']
result6 = compare_1111_SDMZ[compare_1111_SDMZ['volume']*0.2<compare_1111_SDMZ['NetQuantity']]
# result6

policy_breaches_1111 = pd.DataFrame()
policy_breaches_1111 = policy_breaches_1111.append(result1)
policy_breaches_1111 = policy_breaches_1111.append(result2)
policy_breaches_1111 = policy_breaches_1111.append(result3)
policy_breaches_1111 = policy_breaches_1111.append(result4)
policy_breaches_1111 = policy_breaches_1111.append(result5)
policy_breaches_1111 = policy_breaches_1111.append(result6)
# policy_breaches_1111

# net_quantity_1111
df1_1112 = df1.loc[df1['DateTime'].str.contains('2021-11-12')]
# df1_1112

net_quantity_1112 = pd.merge(df1_1111, df1_1112[['Symbol','Quantity','TradeType']], on='Symbol')
# net_quantity_1112

net_quantity_1112_buy = net_quantity_1112[net_quantity_1112['TradeType_y']=='BUY']
net_quantity_1112_sell = net_quantity_1112[net_quantity_1112['TradeType_y']=='SELL']

net_quantity_1112_buy['NetQuantity'] = net_quantity_1112_buy['Quantity_x']+net_quantity_1112_buy['Quantity_y']
net_quantity_1112_sell['NetQuantity'] = net_quantity_1112_sell['Quantity_x']-net_quantity_1112_sell['Quantity_y']

net_quantity_1112 = net_quantity_1112_buy.append(net_quantity_1112_sell)
# net_quantity_1112

equity_prices_1112 = pd.DataFrame(list(cursor.execute("SELECT * FROM equity_prices WHERE cob_date='12-Nov-2021'")))
equity_prices_1112.columns = [i[0] for i in cursor.description]
# equity_prices_1111
equity_prices_1112 = equity_prices_1112.rename(columns = {"symbol_id":"Symbol"})
# equity_prices_1112
compare_1112 = pd.merge(net_quantity_1112[['_id','Symbol','NetQuantity','Trader']], equity_prices_1112, on='Symbol')
# compare_1111

# MRH5231:5% Volume
compare_1112_MRH = compare_1112[compare_1112['Trader']=='MRH5231']
result1 = compare_1112_MRH[compare_1112_MRH['volume']*0.05<compare_1112_MRH['NetQuantity']]
# result1

# JBX1566:125% Volume
compare_1112_JBX = compare_1112[compare_1112['Trader']=='JBX1566']
result2 = compare_1112_JBX[compare_1112_JBX['volume']*1.25<compare_1112_JBX['NetQuantity']]
# result2

# DGR1983:50% Volume
compare_1112_DGR = compare_1112[compare_1112['Trader']=='DGR1983']
result3 = compare_1112_DGR[compare_1112_DGR['volume']*0.5<compare_1112_DGR['NetQuantity']]
# result3

#  DHB1075:125%
compare_1112_DHB = compare_1112[compare_1112['Trader']=='DHB1075']
result4 = compare_1112_DHB[compare_1112_DHB['volume']*1.25<compare_1112_DHB['NetQuantity']]
# result4

# SML1458:125%
compare_1112_SML = compare_1112[compare_1112['Trader']=='SML1458']
result5 = compare_1112_SML[compare_1112_SML['volume']*1.25<compare_1112_SML['NetQuantity']]
# result5

# SDMZ1796:20%
compare_1112_SDMZ = compare_1112[compare_1112['Trader']=='SDMZ1796']
result6 = compare_1112_SDMZ[compare_1112_SDMZ['volume']*0.2<compare_1112_SDMZ['NetQuantity']]
# result6

policy_breaches_1112 = pd.DataFrame()
policy_breaches_1112 = policy_breaches_1112.append(result1)
policy_breaches_1112 = policy_breaches_1112.append(result2)
policy_breaches_1112 = policy_breaches_1112.append(result3)
policy_breaches_1112 = policy_breaches_1112.append(result4)
policy_breaches_1112 = policy_breaches_1112.append(result5)
policy_breaches_1112 = policy_breaches_1112.append(result6)
# policy_breaches_1112

policy_breaches = policy_breaches_1111.append(policy_breaches_1112)
# policy_breaches

policy_breaches['_id'] = policy_breaches['_id'].astype(str)
policy_breaches.to_sql('policy_breaches', connection2, if_exists='replace')