#SQL create a new table: trades_suspects ----------------------------------

SQLCreateRetTable <- "CREATE TABLE trades_suspects (
  symbol_id TEXT NOT NULL,
  trade_id TEXT NOT NULL,
  trader TEXT NOT NULL,
  quantity INTEGER,
  notional INTEGER,
  price FLOAT,
  high FLOAT,
  low FLOAT,
  currency TEXT NOT NULL,
  cob_date TEXT NOT NULL,
  PRIMARY KEY (trade_id)
  FOREIGN KEY (trader) REFERENCES portfolio_positions(trader))"
