# Helpers for Section 4 Aggregating Data

# Get cob_date format from DateTime
AllTrades$DateTime <- as.Date(AllTrades$DateTime)
AllTrades$DateTime <- format(AllTrades$DateTime,"%d-%b-%Y")

# Drop columns for AllTrades
AllTrades <- subset(AllTrades, select= -c(TradeId,TradeType,Counterparty))


