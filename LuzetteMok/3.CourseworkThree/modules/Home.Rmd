---
title: 
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyquant)
library(dplyr)
library(ggplot2)
library(quantmod)
library(magrittr)
library(broom)
```

# **Portfolio Report for Trader JBX1566 on 12-Nov-2021**

## Introduction

> Trader John Black (ID: JBX1566) takes under US ETF Equity Strategy, with a focus on S&P500 stocks in his portfolio. His portfolio has been active since 28-Jun-2015. This report is an analysis on his portfolio as of 12-Nov-2021. On 12-Nov-2021, there are 30 stocks traded in the portfolio. We start off by looking at the portfolio composition from trades on 12-Nov-2021, then, we calculate one-day return for the portfolio stocks. To visualize the performance of the 30 stocks over the year of 2021, we find weighted portfolio returns and include risk metrics such as the covariance matrix. And lastly, a comparison of the benchmark S&P500 is included.

## Portfolio Weights on 12-Nov-2021

#### Pie chart

```{r echo=FALSE}
x = c('AIG','AMZN','ANSS','AON','APTV','CBRE','CMCSA','EFX','GD','HBAN','HES','HOLX','HSIC','HSY','IP','JWN','MLM','MSI','NLSN','PFE','PHM','PLD','PRGO','TWTR','UHS','UNP','UPS','WAT','WU','XLNX')
wts = c(0.004241443,0.6862624,0.02840263,0.007271267,0.004128471,0.0005090231,0.005204491,0.003393605,0.03865347,0.001569482,0.01176018,0.003525722,0.001708315,0.0004319871,0.002416249,0.002495126,0.06090563,0.006044579,0.0002556432,0.009656774,0.003745179,0.005399882,0.001563229,0.00256581,0.0006328167,0.03498496,0.002073888,0.06892189,0.000223972,0.001051894)
data <- data.frame(
  group=x,
  value=wts)
ggplot(data, aes(x="", y=value, fill=group)) +
  geom_bar(stat="identity", width=1, color="white") +
  coord_polar("y", start=0) +
  theme_void() +
  geom_text(aes(x = "", label = c("","AMZN","","","","","","","","","","","","","","","","","","","","","","","","WAT","","","","")), color = "white", size=5)
```

## **One-Day Return for individual stock components**

#### Horizontal bar plot

```{r echo=FALSE}
Returns = c(-0.0110431532,0.0151619870,0.0166316390,0.0036089020,0.0088160464,-0.0007592294,-0.0029817369,0.0161811124,0.0094429655,-0.0018427518,0.0105755042,0.0128081531,0.0101975781,-0.0037005887,-0.0173479561,-0.0229784759,0.0134057971,0.0145255003,0.0009492169,-0.0089677162,0.0130177515,-0.0006715466,0.0000000000,0.0051943055,0.0043755278,0.0078150981,-0.0004688452,0.0180884451,0.0150000000,0.0240444487)
            
barplot(Returns,
  main="One day returns 12-Nov-2021",
  xlab="Returns",
  ylab="Stocks",
  names.arg = x,
  border="dark blue",
  col=c("#eb8060","#a1e9f0","#b9e38d","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#eb8060","#eb8060","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#a1e9f0","#b9e38d","#a1e9f0","#b9e38d"),
  cex.names = 0.7, horiz = TRUE)
```

The green shaded bars indicate the top three stocks with the highest returns, whilst the red color ones indicate the three worst performing stocks that day.

## Weighted Portfolio Returns

#### Histogram

```{r echo=FALSE}
tickers = c('AIG','AMZN','ANSS','AON','APTV','CBRE','CMCSA','EFX','GD','HBAN','HES','HOLX','HSIC','HSY','IP','JWN','MLM','MSI','NLSN','PFE','PHM','PLD','PRGO','TWTR','UHS','UNP','UPS','WAT','WU','XLNX')
wts = c(0.004241443,0.6862624,0.02840263,0.007271267,0.004128471,0.0005090231,0.005204491,0.003393605,0.03865347,0.001569482,0.01176018,0.003525722,0.001708315,0.0004319871,0.002416249,0.002495126,0.06090563,0.006044579,0.0002556432,0.009656774,0.003745179,0.005399882,0.001563229,0.00256581,0.0006328167,0.03498496,0.002073888,0.06892189,0.000223972,0.001051894)
price_data <- tq_get(tickers,
                     from = '2020-11-12',
                     to = '2021-11-12',
                     get = 'stock.prices')
ret_data <- price_data %>%
  group_by(symbol) %>%
  tq_transmute(select = adjusted,
               mutate_fun = periodReturn,
               period = "daily",
               col_rename = "ret")
ret_data %>%
  group_by(symbol) %>%
  slice(c(1,2))
wts_tbl <- tibble(symbol = tickers,
                  wts = wts)
head(wts_tbl)
ret_data <- left_join(ret_data,wts_tbl, by = 'symbol')
ret_data <- ret_data %>%
  mutate(wt_return = wts * ret)
port_ret <- ret_data %>%
  group_by(date) %>%
  summarise(port_ret = sum(wt_return))
port_ret %>%
  ggplot(aes(x = port_ret)) + 
  geom_histogram(bins = 60) +
  theme_light() +
  labs(x = "Portfolio Returns",
       y = "Frequency",
       title = "Daily Portfolio returns histogram")
```

## Average returns and risk metrics

| Metrics              | JBX1566 Portfolio |
|----------------------|-------------------|
| Average daily return | -0.0004616328     |
| Annual return        | 0.3393112         |
| Variance             | 0.0000685887      |
| Standard Deviation   | 0.00828182958     |

## S&P 500 Benchmark Comparison

```{r echo=FALSE}
start = as.Date("2020-11-12") 
end = as.Date("2021-11-12")
getSymbols(c("AMZN", "WAT", "MLM","^GSPC"), src = "yahoo", from = start, to = end)
stocks = as.xts(data.frame(A = AMZN[, "AMZN.Adjusted"], 
                           B = WAT[, "WAT.Adjusted"], C = MLM[, "MLM.Adjusted"], 
                           E = GSPC[,"GSPC.Adjusted"]))
names(stocks) = c("Amazon", "Waters Cop", "Martin Marietta","S&P 500")
index(stocks) = as.Date(index(stocks))
stocks_series = tidy(stocks) %>% 
  ggplot(aes(x=index,y=value, color=series)) +
  labs(title = "Top Three Weighted Stocks and S&P 500: Daily Stock Prices 12 Nov 2020-21") +
  xlab("Date") + ylab("Price") +
  scale_color_manual(values = c("Red", "Black", "DarkBlue","Orange"))+
  geom_line()
stocks_series
```

Clearer movement of individual stock price is shown in this second set of graphs.

```{r echo=FALSE}
stocks_series2 = tidy(stocks) %>% 
  ggplot(aes(x=index,y=value, color=series)) + 
  geom_line() +
  facet_grid(series~.,scales = "free") + 
  labs(title = "Top Three Weighted Stocks and S&P500: Daily Stock Prices 12 Nov 2020-21 (2)") +
  xlab("Date") + ylab("Price") +
  scale_color_manual(values = c("Red", "Black", "DarkBlue","Orange"))
stocks_series2
```

## Summary

> This portfolio is very diverse and consists of stocks from all sectors ranging from financial services (e.g:Western Union), to medical services (e.g: Pfizer). This makes it a very stable portfolio with low volatility. It has a standard deviation of 0.00828182958, which is lower than the usual average portfolio risks. Average annual returns are up to 33.9%, whilst average daily returns are -0.046%. Comparing the top 3 weighted stocks in John's portfolio to S&P500 benchmark over the year, AMZN and WAT fluctuated more, whereas MLM has broadly the same trend as GSPC.
>
> On 12 Nov, the best performing stock for John is XLNX (one day return 2.4%), whereas JWN made the largest loss out of the 30 stocks (one day loss 2.3%). this indicates a good return rate of highly renowned stocks and a low volatility .
