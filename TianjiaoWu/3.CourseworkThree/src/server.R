#--------------------------------------------------------------------------------------
# Big Data in Quantitative Finance: Coursework Three
# Use case: Three.a (R)
#--------------------------------------------------------------------------------------

# Install and import packages in current environment
library(dplyr)
library(stringr)
library(RSQLite)
library(mongolite)
library(lubridate)
library(shiny)

# Define server logic required
shinyServer(function(input, output) {
    # Filter data based on selections
    output$table <- DT::renderDataTable(DT::datatable({
        data <- TradeTwo
        if (input$trader != "All") {
            data <- data[data$trader == input$trader,]
        }
        if (input$cob_date != "All") {
            data <- data[data$cob_date == input$cob_date,]
        }
        data
    }))
    
})

