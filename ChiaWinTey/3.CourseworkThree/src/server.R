#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chia Win Tey
# Topic   : Coursework Three (Case 3A)
# File    : server.R
#--------------------------------------------------------------------------------------
# Import packages in current environment
library(mongolite) 
library(dplyr)
library(lubridate)
library(RSQLite)
library(data.table)
library(shiny)
library(ggplot2)

# Server controls what is displayed by the user interface
server <- function(input,output){
  a <- reactive({
    MainExposure %>%
      filter(trader == input$Trader,cob_date == input$Date)
  })
  b <- reactive({
    PortfolioPosition %>%
      filter(trader == input$Trader,cob_date == input$Date)
  })
  c <- reactive({
    MainExposure %>%
      filter(trader == input$Trader,cob_date == input$Date)
  })
  output$table <- renderTable({ 
    a()
  })
  output$table1 <- renderTable({
    b()
  })
  output$plot2<- renderPlot({
    ggplot(c())+theme_bw()+
      geom_bar(aes(x="",y=Exposure,fill=GICSSector),
               stat="identity")+
      coord_polar("y",start=0)+
      ggtitle("Trader's Main Exposure")+
      theme(plot.title = element_text(hjust=0.5,size=20),
            axis.title = element_blank(),
            axis.text = element_blank(),
            axis.ticks = element_blank(),
            panel.grid = element_blank(),
            panel.border = element_blank())
    })
}


