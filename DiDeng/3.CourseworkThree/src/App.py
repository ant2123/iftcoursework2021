#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import packages
import sqlite3
import json
from scipy import stats
import numpy as np
from sqlalchemy import create_engine
import datetime
import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd

# Connect mongo and read json
mongocon=MongoClient('mongodb://localhost')
db=mongocon.equity
col=db.coursework2
trades = col.find({})
print(trades)
trades = pd.DataFrame(trades)

# Connect sql and read database
Directory = "C:/Users/dell/Desktop/Big Data/cw3"
engine = create_engine(f"sqlite:///{Directory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()
def SQLQueryStatic():
    return ("SELECT * FROM equity_prices")
equitystatic = pd.read_sql_query(SQLQueryStatic(),engine)


# In[2]:


def SQLQueryStatic(table_name):
    return ("SELECT * FROM " + table_name)
equitystatic = pd.read_sql_query(SQLQueryStatic("equity_prices"),engine)
print(equitystatic)
equityportfolio = pd.read_sql_query(SQLQueryStatic("portfolio_positions"),engine)
print(equityportfolio)


# In[3]:


# Check the structure of data
print(trades.keys(),equitystatic.keys())


# In[4]:


# Print yy_mm_dd of json trader file:
def isodate_to_yymmdd(isodate):
    year_json=isodate[8:12]
    month_json=isodate[13:15]
    day_json=isodate[16:18]
    return year_json,month_json,day_json
year_json=np.zeros(len(trades["DateTime"])).astype(int)
month_json=np.zeros(len(trades["DateTime"])).astype(int)
day_json=np.zeros(len(trades["DateTime"])).astype(int)
for i in range(len(trades["DateTime"])):
    year_i,month_i,day_i=isodate_to_yymmdd(trades["DateTime"][i])
    year_json[i]=int(year_i)
    month_json[i]=int(month_i)
    day_json[i]=int(day_i)
date=year_json*10000+month_json*100+day_json
tradesid=np.zeros(len(trades["DateTime"])).astype(str)
for i in range(len(trades["DateTime"])):
    tradesid[i]=str(date[i])+str(trades["Symbol"][i])
    
equityid=np.array(equitystatic["price_id"]).astype(str)
print(date)
print(tradesid)


# In[5]:


# Filtered out the portfolio of a selected trader(MRH5231) in 20211112
select=np.where((date>=20211112)&(date<=20211112)&(trades["Trader"]=="MRH5231"))[0]
trades_select=trades.loc[select,:]
print(trades_select)
print(len(trades_select))


# In[6]:


select=np.where((equityportfolio["trader"]=="MRH5231")& (equityportfolio["cob_date"]=="12-Nov-2021"))[0]
portfolio_select=equityportfolio.loc[select,:]
print(portfolio_select)
print(len(portfolio_select))


# In[7]:


# First, use NKE as an example
select=np.where((equitystatic["symbol_id"]=="NKE"))[0]
NKE_select=equitystatic.loc[select,:]
print(NKE_select)


# In[8]:


# Print yy_mm_dd of equity portfolio file:
def price_id_to_yymmdd(price_id):
    year_portfolio=price_id[0:4]
    month_portfolio=price_id[4:6]
    day_portfolio=price_id[6:8]
    return year_portfolio,month_portfolio,day_portfolio
year_portfolio=np.zeros(len(NKE_select["price_id"])).astype(int)
month_portfolio=np.zeros(len(NKE_select["price_id"])).astype(int)
day_portfolio=np.zeros(len(NKE_select["price_id"])).astype(int)
for i in range(len(NKE_select["price_id"])):
    year_i,month_i,day_i=price_id_to_yymmdd(np.array(NKE_select["price_id"])[i])
    year_portfolio[i]=int(year_i)
    month_portfolio[i]=int(month_i)
    day_portfolio[i]=int(day_i)
date=year_portfolio*10000+month_portfolio*100+day_portfolio
#print(date)


# In[9]:


# For the clarity of x-coordinate, transfer the ddmmyy to the days from first trading day in the file
days_month=np.array([31,29,31,30,31,30,31,31,30,31,30,31,31,28,31,30,31,30,31,31,30,31,30,31])
print(days_month) 
days_portfolio=np.zeros(len(NKE_select["price_id"])).astype(int)
for i in range(len(date)):
    index_i=(year_portfolio[i]-2020)*12+month_portfolio[i]-1
    days_i=sum(days_month[:index_i])+day_portfolio[i]-2
    days_portfolio[i]=int(days_i)
print(days_portfolio)


# In[10]:


# Draw the graph 
plt.scatter(days_portfolio,NKE_select["close"],s=5)
plt.show()


# In[11]:


# OLS prediction
x_days=np.array(days_portfolio)
y_NKE=np.array(NKE_select["close"])
slope_NKE, intercept_NKE, r_value, p_value, std_err = stats.linregress(x_days,y_NKE)
print('Slope: ',slope_NKE,'\nIntercept: ',intercept_NKE)


# In[12]:


def predict_y_for(x,slope,intercept):
    return slope * x + intercept
plt.scatter(x_days,y_NKE)
plt.plot(x_days, predict_y_for(x_days,slope_NKE,intercept_NKE), c='r')
plt.show()


# In[13]:

# Another way of OLS prediction
#A = np.vstack([x, np.ones(len(x))]).T
#print(A)
#m,c=np.linalg.lstsq(A,y,rcond=None)[0]
#m,c
#_ = plt.plot(x, y, 'o', label='Original data', markersize=3)
#_ = plt.plot(x, m*x + c, 'r', label='Fitted line')
#_ = plt.legend()
#plt.show()


# In[14]:


days_portfolio_reverse=days_portfolio[::-1]
prices_linear_NKE=slope_NKE * days_portfolio_reverse + intercept_NKE
#print(prices_linear_NKE)


# In[15]:


prices_diff_NKE=(NKE_select["close"]-prices_linear_NKE)/prices_linear_NKE
print(prices_diff_NKE)
plt.hist(prices_diff_NKE)
plt.show()


# In[16]:


# Match the days with the cob_date for each stock
def cob_date_to_days(cob_date):
    day=int(cob_date[0:2])
    month=cob_date[3:6]
    year=int(cob_date[7:12])
    month_symbol=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    index=np.where(month==np.array(month_symbol))[0][0]
    month=index+1
    days_month=np.array([31,29,31,30,31,30,31,31,30,31,30,31,31,28,31,30,31,30,31,31,30,31,30,31])
    index=(year-2020)*12+month-1
    days=sum(days_month[:index])+day-2
    return days
days=cob_date_to_days("12-Nov-2021")
cob_date_unique=np.unique(np.array(equitystatic.loc[:,["cob_date"]]))
cob_days=np.zeros(len(cob_date_unique)).astype(int)
for i in range(len(cob_date_unique)):
    cob_days[i]=cob_date_to_days(cob_date_unique[i])
print(cob_days[0:10])
print(cob_date_unique[0:10])
index_order=np.argsort(cob_days)
cob_days=cob_days[index_order]
cob_date_unique=cob_date_unique[index_order]
#print(cob_days[0:10])
#print(cob_date_unique[0:10])
table_cob_date=pd.DataFrame({"cob_days":cob_days,"cob_date":cob_date_unique})
print(table_cob_date)


# In[17]:


# Calculate price_linear function, slope, intercept
slope=np.zeros(len(portfolio_select))
intercept=np.zeros(len(portfolio_select))
table=[]

for i in range(len(portfolio_select)):
    select_tmp=np.where(equitystatic["symbol_id"]==np.array(portfolio_select["symbol"])[i])[0]   #print(equitystatic.loc[select_tmp,:])
    table_tmp=pd.merge(equitystatic.loc[select_tmp,:],table_cob_date,on=["cob_date"])
    table.append(table_tmp)     #print(table_tmp["cob_days"],table_tmp["close"])
    slope[i], intercept[i], r_value, p_value, std_err = stats.linregress(table_tmp["cob_days"],table_tmp["close"])
#print(slope[i],intercept[i])
#print(slope[0:10],intercept[0:10])
def calc_price_linear(cob_days,slope,intercept):
    return cob_days*slope+intercept


# Calculate diff=price-price_linear
diff=[]
price_linear=[]
price=[]
sigma_diff=np.zeros(len(portfolio_select))
for i in range(len(portfolio_select)):
    price_linear_i=calc_price_linear(np.array(table[i]["cob_days"]),slope[i],intercept[i])
    price_i=np.array(table[i]["close"])
    diff_i=price_i-price_linear_i
    sigma_diff_i=np.std(diff_i,ddof=1)
    
 
    diff.append(diff_i)
    price_linear.append(price_linear_i)
    price.append(price_i)
    sigma_diff[i]=sigma_diff_i

# diff~N(0,sigma_diff)

    
# Caculate combined risk and performance
amount=np.array(portfolio_select["net_quantity"])
print(amount)
table_close=np.zeros(14632)
price_final=np.zeros(len(portfolio_select))
price_linear_final=np.zeros(len(portfolio_select))
for i in range(len(portfolio_select)):
    table_close_i=np.array(table[i]["close"])   #    print(table_close_i)
    price_final_i=table_close_i[0]              #    print(price_final_i)
    price_final[i]=price_final_i
    price_linear_final_i=price_linear[i][0]
    price_linear_final[i]=price_linear_final_i
print(price_final,price_linear_final)

# price_expect_final=price_linear_final+diff:N(price_linear_final,diff)
# diff_expect_i: N(0,diff_i)
# price_expect_i=price_linear_i+diff_expect_i

sigma_combined=np.sqrt(np.sum((sigma_diff*amount)**2))
print(sigma_combined)
profit_long_term=np.sum((price_final-price_linear_final)*amount)  
print(profit_long_term)
#profit_short_term：N(0,sigma_combined)  = Σdiff_expect_i* amount_i
#profit=profit_long_term+profit_short_term:N(profit_long_term,sigma_combined)  cdf


# In[18]:


# Close connections
con.close()
mongocon.close()

