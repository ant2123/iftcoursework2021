from typing import Optional

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
import re
from modules.db.db_connection import sql_conn, mongo_conn
import configparser


class Item(BaseModel):
    DateTime: Optional[str] = None
    TradeId: Optional[str] = None
    Trader: Optional[str] = None
    Symbol: Optional[str] = None
    Quantity: Optional[int] = None
    Notional: Optional[float] = None
    TradeType: Optional[str] = None
    Ccy: Optional[str] = None
    Counterparty: Optional[str] = None

    def check_params(self):
        missing_key = []
        for (k, v) in self.__dict__.items():
            if not v:
                missing_key.append(k)
        return missing_key

    def check_price(self, low, high):
        price = self.Notional / self.Quantity
        if price < low or price > high:
            return False
        return True

    def check_Notional(self, already_store, volume):
        return self.Notional + already_store <= volume


class Result(object):
    def __init__(self, code, message, data=None):
        self.code = code
        self.message = message
        self.data = data

    @staticmethod
    def failed(msg, data=None):
        return Result(-1, msg, data)


def read_sqlite_config():
    conf_parser = configparser.ConfigParser()
    conf_parser.read("config/script.config")
    return conf_parser.get("sqlite_config", "sqldatabase")


def read_mongo_config():
    conf_parser = configparser.ConfigParser()
    conf_parser.read("config/script.config")
    mongo_db = conf_parser.get("mongo_config", "db")
    mongo_collection = conf_parser.get("mongo_config", "collection")
    mongo_url = conf_parser.get("mongo_config", "url")
    return mongo_url, mongo_db, mongo_collection


def transfer_date_format(date_origin):
    month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    date = re.findall(r"\((\d*)-(\d*)-(\d*).*\)", date_origin)
    if len(date):
        date = date[0]
        date = date[2] + "-" + month[int(date[1]) - 1] + "-" + date[0]
        return date
    raise Exception()


def split_date_from_origin(date_origin):
    date = re.findall(r"\((\d*)-(\d*)-(\d*).*\)", date_origin)
    date = date[0]
    return "{}-{}-{}".format(date[0], date[1], date[2])


def find_limited_range(sqlite, data_origin, Symbol):
    date = transfer_date_format(data_origin)
    sql = "SELECT low, high, volume FROM equity_prices WHERE cob_date == '" + date + "' and symbol_id == " + "'" + Symbol + "'"
    result = sqlite.execute(sql).fetchone()
    if not result:
        raise Exception()
    low = result[0]
    high = result[1]
    volume = result[2]
    return low, high, volume


app = FastAPI()


@app.post("/items/")
async def create_item(item: Item):
    missing_key = item.check_params()
    if missing_key:
        print(missing_key)
        return Result.failed(msg="missing key", data=missing_key)
    mongo_db_conf = read_mongo_config()
    sqlite_name = read_sqlite_config()
    try:
        collection = mongo_conn(*mongo_db_conf)
    except Exception:
        return Result.failed("cannot link to mongodb, check script.config")

    try:
        conn, sqlite_c = sql_conn(sqlite_name)
    except Exception:
        return Result.failed("cannot link to sqlite, check script.config")

    try:
        low, high, volume = find_limited_range(sqlite_c, item.DateTime, item.Symbol)
    except Exception as e:
        return Result.failed("date or symbol is wrong")

    if not item.check_price(low, high):
        return Result.failed("price is not correct, please double check")

    mongo_query = {"DateTime": {"$regex": split_date_from_origin(item.DateTime), "$options": "$i"}, "Symbol": item.Symbol}
    already_store_items = collection.find(mongo_query)
    already_store = 0
    for x in already_store_items:
        already_store += float(x['Notional'])
    print(already_store)
    print(volume)
    if not item.check_Notional(already_store, volume):
        return Result.failed("Notional is not correct, please double check")
    collection.insert_one(item.dict())
    return Result(code=0, message="succeed")


if __name__ == '__main__':
    uvicorn.run(app='App:app', host="127.0.0.1", port=8000, reload=True, debug=True)
