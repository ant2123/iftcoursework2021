from backend.ApplyQuery import *
from backend.Databases import *
import json

def traders_info():
    data = Query(trader_statics,col=['trader_id',"trader_name","golive_date"],query=SELECT)
    trader_name = []
    trader_id = []
    date = []
    for row in data:
        trader_id.append(row[0])
        trader_name.append(row[1])
        date.append(row[2])
        break
    return trader_name,trader_id,date

def Main():
    file_name = open('/Users/parinamehta/Desktop/iftcoursework2021/000.DataBases/NoSQL/CourseworkTwo.json')
    first_file = open('/Users/parinamehta/Desktop/iftcoursework2021/000.DataBases/NoSQL/CourseworkOne.json')
    file = json.load(file_name)
    first_file = json.load(first_file)
    trader_name,trader_id,join_date = traders_info()

    # Extracting Net Profit & Total Trades
    count = 0
    net_trade = 0
    expense = 0.0
    earning = 0.0
    buy_count = 0
    sell_count = 0
    sell_qty = 0
    buy_qty = 0
    for lines in file:
        if str(lines['Trader']) == trader_id[0]:
            date = (str(lines['DateTime']).split("(")[1]).split("T")[0]
            if date == "2021-11-12":
                qty = float(lines['Quantity'])
                item_symbol = lines['Symbol']
                type_name = lines['TradeType']
                for symbol_file in first_file:
                    if item_symbol == symbol_file['Symbol']:
                        rate = symbol_file['MarketData']['Price']
                        if type_name == "BUY":
                            expense = (rate * qty) + expense
                            buy_qty += lines['Quantity']
                            buy_count += 1
                        else:
                            qty = qty * (-1)
                            earning = (rate * qty) + earning
                            sell_count += 1
                            sell_qty += lines['Quantity']
                            sell_qty = sell_qty * (-1)
                        break
            count += 1
    net_trade = sell_count + buy_count
    return trader_name,trader_id,net_trade,earning,expense,buy_count,sell_count,join_date,sell_qty,buy_qty



