#aggregating Quantity and Notional for all trades by date, trader, symbol, ccy and insert the results into portfolio_positions
c.execute (""" INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)
select all_trades.tradeid||all_trades.DateTime||equity_static.symbol , 
all_trades.dateTime , all_trades.trader  , all_trades.symbol , all_trades.ccy , sum( quantity) , sum(notional)
from all_trades join equity_static 
on all_trades.symbol = equity_static.symbol
group by all_trades.tradeid||all_trades.dateTime||equity_static.symbol , 
all_trades.dateTime , all_trades.trader  , all_trades.symbol , all_trades.ccy
""")


# In[38]:


conn.commit()