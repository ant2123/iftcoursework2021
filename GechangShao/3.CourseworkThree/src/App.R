#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Gechang Shao
# Date : 2022-01-29
# Topic   : Use Case Three.a (R) - visualising daily positions by trader.
# File    : App.R
#--------------------------------------------------------------------------------------



# install packages needed if not yet installed ----------------------------

if (!any(
  c(
    "dplyr",
    "lubridate",
    "RSQLite",
    "shiny",
    "shinythemes",
    "reader",
    "shinydashboard"
  ) %in% installed.packages()[, 1]
)) {
  install.packages("dplyr")
  install.packages("lubridate")
  install.packages("RSQLite")
  install.packages("shiny")
  install.packages("shinythemes")
  install.packages("readr")
  install.packages("shinydashboard")
}


# Import Libraries --------------------------------------------------------

library(lubridate)
library(RSQLite)
library(dplyr)
library(shiny)
library(shinythemes)
library(readr)
library(shinydashboard)


# prevent scientific notation ---------------------------------------------

options(scipen = 999)


# Parse ARGs --------------------------------------------------------------

# ---Set Args
Args = c(
  "/Users/gechangshao/Desktop/iftcoursework2021/GechangShao/3.CourseworkThree",
  "script.config",
  "script.params"
)
# Args <- commandArgs(TRUE)

# ---Set working directory
setwd(Args[1])

# ---Source config
source(paste0("./config/", Args[2]))

# ---Source params
source(paste0("./config/", Args[3]))


# Setting up connection for SQLite ----------------------------------------

source(paste0(Args[1], Config$Directories$setup))


# Import data from SQLite ------------------------------------------

# ---Import Equity Static
EquityStatic <-
  as.data.frame(dbGetQuery(conSql, "SELECT * FROM equity_static"))

# ---check the structure of Equity Static
str(EquityStatic)
typeof(EquityStatic)
dim(EquityStatic)

# ---Import Portfolio Position and calculate the market price of equity
PortfolioPositions <-
  as.data.frame(
    dbGetQuery(
      conSql,
      "SELECT symbol, trader, portfolio_positions.cob_date, net_quantity, net_amount, open, high, low, close,  net_amount*1.0/net_quantity AS innitial_prices, close* net_quantity AS amount, (close* net_quantity)-net_amount AS benefit FROM portfolio_positions LEFT JOIN equity_prices ON symbol = symbol_id AND portfolio_positions.cob_date = equity_prices.cob_date"
    )
  )

# ---Import Trader Static
TraderStatic <-
  as.data.frame(dbGetQuery(conSql, "SELECT * FROM trader_static"))



# Prepare data for visualization page -------------------------------------

# ---Match the name in Trader Static with Portfolio Positions
PositionsWithName <-
  merge(
    TraderStatic,
    PortfolioPositions,
    by.x = "trader_id",
    by.y = "trader",
    all = FALSE
  )

# ---Match the industry in Equity Static with Portfolio Positions
PositionsWithIndustry <-
  merge(
    EquityStatic,
    PositionsWithName,
    by.x = "symbol",
    by.y = "symbol",
    all = FALSE
  )

# ---Adjust the format of date
PositionsWithIndustry$date <-
  as.Date.character(PositionsWithIndustry[, "cob_date"], format = "%d-%b-%Y")

# ---aggregate the market value of positions by sector
PositionsBySector <-
  aggregate(
    cbind(net_quantity, amount) ~ date + trader_name + GICSSector + fund_currency,
    data = PositionsWithIndustry,
    FUN = sum
  )

# ---calculate the exposure by sector
for (i in 1:nrow(PositionsBySector)) {
  dataset1 <-
    PositionsBySector[which(
      PositionsBySector$date == PositionsBySector[i, "date"] &
        PositionsBySector$trader_name == PositionsBySector[i, "trader_name"]
    ), ]
  gross_amount <- sum(dataset1$amount)
  PositionsBySector[i, "gross_amount"] <- gross_amount
}
PositionsBySector$exposure <-
  PositionsBySector$amount / PositionsBySector$gross_amount


# create shiny object -----------------------------------------------------

runApp("src")


# disconnet SQLite --------------------------------------------------------

dbDisconnect(conSql)

