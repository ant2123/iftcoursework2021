#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Gechang Shao
# Topic   : Create new table
#--------------------------------------------------------------------------------------

# ----------1. SQL create a new table: trades_suspects ----------------------------------
SQLCreateRetTable <- "CREATE TABLE trades_suspects (
  DateTime TEXT NOT NULL,
  TradeId TEXT NOT NULL,
  Trader TEXT NOT NULL,
  Symbol TEXT NOT NULL,
  Quantity INTEGER NOT NULL,
  Notional INTEGER NOT NULL, 
  TradeType TEXT NOT NULL,
  Ccy TEXT NOT NULL,
  Counterparty TEXT NOT NULL,
	FOREIGN KEY (Symbol) REFERENCES equity_prices(symbol_id),
	PRIMARY KEY(TradeId))"

# ----------2. SQL Delete a table 
SQLDeleteTable <- "DROP TABLE trades_suspects"
