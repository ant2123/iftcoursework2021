#%%
from os.path import dirname
import sys 

# __file__: the absolute path for the current file #'c:\\Users\\haris\\Desktop\\CW3\\config\\config.py'

sys.path.append( 
    dirname( dirname( dirname(__file__) ) )
 )

# connecting to config file  
from config.config import PATH_DB

#%% Connecting to sql database

import sqlite3
import pandas as pd
from pymongo import MongoClient

EquityDatabase = PATH_DB
SqlConnection = sqlite3.connect(EquityDatabase)
Cursor = SqlConnection.cursor()

#%% Connecting to mongodb

MongoConnection = MongoClient('mongodb://localhost')
db = MongoConnection.Equity
Collection = db.CourseworkTwo

# %%
