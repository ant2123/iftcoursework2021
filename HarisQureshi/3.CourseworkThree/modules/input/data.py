# prepare data for component, including: (1)read data (2)process data

#%%
from os.path import dirname
import sys 
import pandas as pd 
from datetime import datetime as dt
import numpy as np

# add path # __file__: the absolute path for the current file # 'c:\\Users\\haris\\Desktop\\CW3\\config\\config.py'

sys.path.append( 
     dirname(dirname( dirname(__file__) ) )
 )

# from modules.component.metric import EquityPrice

from modules.db.connect import SqlConnection, Collection

# (1) Read Data
Trader = 'JBX1566' # just for testing
Date = '12-Nov-2021'

#%%

def getData(Trader, Date):
    # mongo
    TradesNov1112 = pd.DataFrame(Collection.find({"$and": [{"DateTime": {"$gt": "ISODate(2021-11-11T00:00:00.000Z)","$lte": "ISODate(2021-11-13T00:00:00.000Z)"}}, {"Trader": {"$eq": Trader}}]}))
    # sql
    PortfolioPosition = pd.read_sql_query(f"SELECT pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount from portfolio_positions WHERE trader = '{Trader}'", SqlConnection) # insert 
    EquityPrice = pd.read_sql_query(f"SELECT high, low, close, cob_date, symbol_id as symbol from equity_prices", SqlConnection)
    EquityStatic = pd.read_sql_query(f"SELECT * from equity_static", SqlConnection)
    return(PortfolioPosition, EquityPrice, EquityStatic , TradesNov1112)

# import data from mongo and sql and create NewPortfolioPosition
def processData(PortolioPosition, TradesNov1112, EquityPrice):
    # process mongo & sql data togother
    # create pos_id
    TradesNov1112['pos_id'] = TradesNov1112['Trader'].astype(str) + '20211111' + TradesNov1112['Symbol']
    # deal with time format
    TradesNov1112['cob_date'] = TradesNov1112['DateTime'].apply( lambda x:dt.strftime( dt.strptime(x[8:18],'%Y-%m-%d'), '%d-%b-%Y' ))
    # aggregate mongo data (TradesNov1112)
    TradesAggregate = TradesNov1112.groupby(['pos_id', 'cob_date', 'Trader', 'Symbol', 'Ccy',]).agg(Quantity = ('Quantity','sum'),Notional = ('Notional', 'sum')).reset_index()
    # merge sql data and mongo data
    TradesAggregate.columns = PortolioPosition.columns # rename before mergering
    NewPortfolioPosition = pd.concat([PortolioPosition, TradesAggregate], axis=0 )
        # clean EquityPrice
    EquityPrice['Date'] = EquityPrice['cob_date'].apply(lambda x: dt.strptime(x,"%d-%b-%Y"))
    
    return( NewPortfolioPosition, EquityPrice )

# merge PortfolioPosition, EquityPrice and EquityStatic
def mergeData(PortfolioPosition, EquityPrice, EquityStatic):
    #merge NewPortfolioPosition & EquityPrice
    PortfolioPosition = pd.merge(
                            PortfolioPosition,
                            EquityPrice,
                            on = ['cob_date','symbol'],
                            how = 'left' 
    )
    #merge NewPortfolioPosition & EquityStatic
    PortfolioPosition = pd.merge(
                            PortfolioPosition,
                            EquityStatic,
                            on = ['symbol'],
                            how = 'left'
    )
    return(PortfolioPosition)

# get equity return
def get_equity_return(EquityPrice):
    df = EquityPrice.copy()
    df = df.sort_values(by=['Date']) 
    Return = np.log( df['close'].shift() / df['close'] )
    return( Return ) 

# function to include every together
def readAndProcessData(Trader, Date):
    # get NewPortfolioPosition
    PortfolioPosition, EquityPrice, EquityStatic, TradesNov1112 = getData(Trader, Date) # we dont use abbreviations
    NewPortfolioPosition, EquityPrice = processData(PortfolioPosition, TradesNov1112, EquityPrice)
    # add return & weight
    EquityPrice['ReturnEquity'] = EquityPrice.groupby(['symbol']).apply(get_equity_return).values 
    NewPortfolioPosition = mergeData(NewPortfolioPosition, EquityPrice, EquityStatic)
    NewPortfolioPosition['sum_net_amount'] = NewPortfolioPosition.groupby(['Date'])['net_amount'].transform(np.nansum).values
    NewPortfolioPosition['weight'] = (NewPortfolioPosition['net_amount']/NewPortfolioPosition['sum_net_amount']).values
    
    
    #NewPortfolioPosition['Date'] =NewPortfolioPosition["cob_date"].apply(lambda x: dt.strptime(x,"%d-%b-%Y"))
    return( NewPortfolioPosition)

#%%
