    |_3.CourseworkThree
    |--config/
    |---script.config (.ini files are good too)
    |---script.params
    |--modules/
    |--static/
    |--src/
    |---App.* (if Shiny is used, then here should be placed server.R and ui.R)
    |--test/
    |--Home.html (mandatory for Use case 1 and 2, optional for the other)


(1) Step1: copy lUca's structure
- we need main.py / README.md in every project 
(2) Step2: start from config/ one module
- Modules:
- - components: create pictures, dataframe for designing HTML page
- - db: connect database 
- - input: get data / process data / prepare data for components
- - output: output

- src: 
- HTML template

(3) write codes from config 
