#%% 
import pandas as pd
from os.path import dirname
import os 
import sys 
from datetime import datetime as dt
sys.path.append( 
      dirname( dirname(__file__) ) 
 )

# get data & params
from config.params import Trader, Date # get the params 
from modules.component.graph import plot_pie
from modules.input.data import readAndProcessData

#%% 

def createHTML():
    # create HTML
    
    df = readAndProcessData(Trader, Date) 
    
     # Set up multiple variables to store the titles, text, data within the report

    page_title_text='Portfolio Report for {} as of 12 Nov 2021'.format(df['trader'][0])
    title_text = 'Portfolio Report for {} as of 12 Nov 2021'.format(df['trader'][0])
    text = 'Welcome to this analytics report, this report higlights some risk and performance metrics for the trader JBX1566'
    prices_text = 'Raw Data for Portfolio Positions as of 12 Nov 2021'

    # Combine everything together using a long f-string
    html = f'''
        <html>
            <head>
                <title>{page_title_text}</title>
            </head>
            <body>
                <h1>{title_text}</h1>
                <p>{text}</p>
                <img src='{os.path.join(dirname(__file__),'pie_chart.png')}' width="700">
                <img src='{os.path.join(dirname(__file__),'line_chart.png')}' width="700">
                <img src='{os.path.join(dirname(__file__),'table_1.png')}' width="700">
                <img src='{os.path.join(dirname(__file__),'table_2.png')}' width="700">
                <h2>{prices_text}</h2>
                {df.to_html()}
            </body>
        </html>
        '''
        # use absolute path for safety


    # Write the html string as an HTML file
    with open('Home.html', 'w') as f:
        f.write(html)


# %%

