# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 20:29:21 2021

@author: Haris
"""

def SQLtrades_suspectsTable():
    trades_suspects_table = "CREATE TABLE trades_suspects (\
    TradeId TEXT PRIMARY KEY,\
    DateTime TEXT NOT NULL,\
    Trader TEXT NOT NULL,\
    Symbol TEXT NOT NULL,\
    DayLow INTEGER,\
    DayHigh INTEGER,\
    TradePrice INTEGER,\
    Quantity INTEGER,\
    Notional INTEGER,\
    Counterparty TEXT NOT NULL,\
    FOREIGN KEY (Trader) REFERENCES equity_prices(symbol_id))"
    return (trades_suspects_table)