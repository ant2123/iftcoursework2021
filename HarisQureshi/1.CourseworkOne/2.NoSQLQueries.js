Q1)

db.CourseworkOne.find(
	{$and: [{"FinancialRatios.PERatio": {"$gt": 15, "$lte": 25}},
	{"FinancialRatios.DividendYield": {"$gt": 2}}, 
	{"FinancialRatios.PayoutRatio": {"$gt": 40, "$lte": 60}}, 
	{"StaticData.GICSSector": {"$in": [ "Industrials", "Materials"] } }]},
	{"StaticData.SECfilings":0, "MarketData.MarketCap":0, "MarketData.Beta":0}).limit(5).pretty()

Q2)

db.CourseworkOne.aggregate([
	{$match: {"StaticData.GICSSector": "Financials"}}, 
	{$group: {_id:"$StaticData.GICSSubIndustry", 
	Total_MarketCap: {$sum: "$MarketData.MarketCap" } } } ])
