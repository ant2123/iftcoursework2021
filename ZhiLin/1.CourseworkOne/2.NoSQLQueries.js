/**The first query: Calculate the average PE and Beta for each GICS sector by selecting companies with a payout ratio greater than 20.*/
db.CourseworkOne.aggregate([
  {$match:{"FinancialRatios.PayoutRatio":{$gte:20}}},
  {$group:{_id:"$StaticData.GICSSector",
  average_beta:{$avg:"$MarketData.Beta"},
  average_pe:{$avg:"$FinancialRatios.PERatio"}}},
  {$sort:{"average_pe":1}}])
/**{ _id: 'Financials',
  average_beta: 0.9490196078431372,
  average_pe: 17.272549019607844 }
{ _id: 'Consumer Discretionary',
  average_beta: 1.057837837837838,
  average_pe: 20.87027027027027 }
{ _id: 'Materials', average_beta: 1.0265, average_pe: 21.815 }
{ _id: 'Communication Services',
  average_beta: 0.7424999999999999,
  average_pe: 24.587500000000002 }
{ _id: 'Industrials',
  average_beta: 1.049777777777778,
  average_pe: 24.664444444444445 }
{ _id: 'Consumer Staples',
  average_beta: 0.5533333333333333,
  average_pe: 26.383333333333333 }
{ _id: 'Information Technology',
  average_beta: 1.1864864864864864,
  average_pe: 26.405405405405407 }
{ _id: 'Utilities',
  average_beta: 0.2652,
  average_pe: 28.151999999999997 }
{ _id: 'Health Care',
  average_beta: 0.8310000000000001,
  average_pe: 36.87 }
{ _id: 'Energy', average_beta: 1.045, average_pe: 43.475 }
{ _id: 'Real Estate',
  average_beta: 0.49448275862068963,
  average_pe: 268.71724137931034 } */


/**The second query: With the above result, I set some selection criteria and ranked 10 companies in descending order of market capital in Financials. */
db.CourseworkOne.find({$and:[{"FinancialRatios.PERatio":{"$lte":17.27}},
                           {"MarketData.Beta":{"$lte":0.95}},
                           {"FinancialRatios.PayoutRatio":{"$gte":20}},
                           {"StaticData.GICSSector":{"$eq":"Financials"}}]})
                           .sort({"MarketData.MarketCap":-1})
                           .limit(10)
/**{ _id: ObjectId("619d0444d71fc36d554894d0"),
  Symbol: 'WFC',
  StaticData: 
   { Security: 'Wells Fargo',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Diversified Banks' },
  MarketData: { Price: 50.93, MarketCap: 224403, Beta: 0.84 },
  FinancialRatios: { DividendYield: 3.6, PERatio: 10.9, PayoutRatio: 39.4 } }
{ _id: ObjectId("619d0444d71fc36d554894b1"),
  Symbol: 'USB',
  StaticData: 
   { Security: 'U.S. Bancorp',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Diversified Banks' },
  MarketData: { Price: 56.58, MarketCap: 89160, Beta: 0.79 },
  FinancialRatios: { DividendYield: 2.7, PERatio: 13, PayoutRatio: 35.1 } }
{ _id: ObjectId("619d0444d71fc36d5548932c"),
  Symbol: 'BK',
  StaticData: 
   { Security: 'The Bank of New York Mellon Corp.',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Asset Management & Custody Banks' },
  MarketData: { Price: 46.73, MarketCap: 44050, Beta: 0.78 },
  FinancialRatios: { DividendYield: 2.5, PERatio: 12.1, PayoutRatio: 29.7 } }
{ _id: ObjectId("619d0444d71fc36d5548932e"),
  Symbol: 'BBT',
  StaticData: 
   { Security: 'BB&T Corporation',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Regional Banks' },
  MarketData: { Price: 53.49, MarketCap: 40982, Beta: 0.83 },
  FinancialRatios: { DividendYield: 3.1, PERatio: 13.3, PayoutRatio: 41.4 } }
{ _id: ObjectId("619d0444d71fc36d554892f4"),
  Symbol: 'AFL',
  StaticData: 
   { Security: 'AFLAC Inc',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Life & Health Insurance' },
  MarketData: { Price: 52.47, MarketCap: 38847, Beta: 0.68 },
  FinancialRatios: { DividendYield: 2, PERatio: 12.7, PayoutRatio: 25.7 } }
{ _id: ObjectId("619d0444d71fc36d55489301"),
  Symbol: 'ALL',
  StaticData: 
   { Security: 'Allstate Corp',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Property & Casualty Insurance' },
  MarketData: { Price: 108.18, MarketCap: 35611, Beta: 0.69 },
  FinancialRatios: { DividendYield: 1.8, PERatio: 13.9, PayoutRatio: 24.7 } }
{ _id: ObjectId("619d0444d71fc36d554894aa"),
  Symbol: 'TRV',
  StaticData: 
   { Security: 'The Travelers Companies Inc.',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Property & Casualty Insurance' },
  MarketData: { Price: 132.16, MarketCap: 34412, Beta: 0.69 },
  FinancialRatios: { DividendYield: 2.4, PERatio: 14.7, PayoutRatio: 35.5 } }
{ _id: ObjectId("619d0444d71fc36d55489493"),
  Symbol: 'STI',
  StaticData: 
   { Security: 'SunTrust Banks',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Regional Banks' },
  MarketData: { Price: 69.03, MarketCap: 30645, Beta: 0.91 },
  FinancialRatios: { DividendYield: 3, PERatio: 12.6, PayoutRatio: 37.5 } }
{ _id: ObjectId("619d0444d71fc36d55489415"),
  Symbol: 'MTB',
  StaticData: 
   { Security: 'M&T Bank Corp.',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Regional Banks' },
  MarketData: { Price: 157.8, MarketCap: 21096, Beta: 0.82 },
  FinancialRatios: { DividendYield: 2.5, PERatio: 11.3, PayoutRatio: 28.7 } }
{ _id: ObjectId("619d0444d71fc36d554893cc"),
  Symbol: 'HIG',
  StaticData: 
   { Security: 'Hartford Financial Svc.Gp.',
     SECfilings: 'reports',
     GICSSector: 'Financials',
     GICSSubIndustry: 'Property & Casualty Insurance' },
  MarketData: { Price: 57.87, MarketCap: 20924, Beta: 0.72 },
  FinancialRatios: { DividendYield: 2.1, PERatio: 12.8, PayoutRatio: 26.5 } } */