
import pymongo
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from datetime import datetime
import datetime
import sqlite3
import scipy
import os,sys
os.chdir(sys.path[0])
import configparser
from modules.db.db_connection import sql_connection, mongo_connection
from modules.db.SQL import CreateTable

def sql_connection(sql_name):
    connection = sqlite3.connect(sql_name)
    cursor = connection.cursor()
    return connection,cursor

def mongo_connection(db_name, collection_name, url):
    client_mongo = pymongo.MongoClient(url)
    db_mongo = client_mongo[db_name]
    collection_mongo = db_mongo[collection_name]
    return collection_mongo


def main():
    # read config/paras
    config = configparser.ConfigParser()
    config.read(r"C:\Users\Patrick_Lin\Documents\iftcoursework2021\ZhiLin\2.CourseworkTwo\config\script.config")
    sql_name = config.get("config","sqldata")
    x = config.get("config","dbcon")
    create_table = config.get("config", "sqlcreate")
    

    config.read(r"C:\Users\Patrick_Lin\Documents\iftcoursework2021\ZhiLin\2.CourseworkTwo\config\script.params")
    mongo_db = config.get("params","mongo_db")
    mongo_con = config.get("params","mongo_collection")
    mongo_url = config.get("params","url")
    # connect sql/mongo
    print(mongo_db, mongo_con, mongo_url)
    try:
        col = mongo_connection(mongo_db, mongo_con, mongo_url)

    except:
        print("Connection Failed")
        sys.exit(-1)
    try:
        conn, cursor = sql_connection(sql_name)
    except:
        print("Connection Failed")
        sys.exit(-1)


   
    fulldata_mongo = col.find({})
    fulldata = pd.DataFrame(fulldata_mongo)

    fulldata["Date_Time"] = fulldata.DateTime.apply(lambda x: datetime.datetime.strptime(x, "ISODate(%Y-%m-%dT%H:%M:%S.000Z)") ).values
    fulldata = fulldata.rename(columns={'Symbol':'symbol_id'})

    #Method1: Use trade vs price method to find out the fat finger errors
    #select data of two days sqperately
    start = '2021-11-11'
    middle = '2021-11-12'
    end = '2021-11-13'
    con1=fulldata['Date_Time']>=start
    con2=fulldata['Date_Time']<middle
    con3=fulldata['Date_Time']>=middle
    con4=fulldata['Date_Time']<end
    
    #Put two days data into two dataframes, and create a column called check_price by computing "notional/quantity"
    Nov11, Nov12= fulldata[con1&con2], fulldata[con3&con4]
    Nov11['check_price']=Nov11['Notional']/Nov11['Quantity']
    Nov12['check_price']=Nov12['Notional']/Nov12['Quantity']


    # conn = sqlite3.connect('Equity.db')
    # cursor = conn.cursor()
    sql_Nov11 = pd.DataFrame(cursor.execute("SELECT * FROM equity_prices WHERE cob_date = '11-Nov-2021'"))
    sql_Nov12 = pd.DataFrame(cursor.execute("SELECT * FROM equity_prices WHERE cob_date = '12-Nov-2021'"))
    sql_Nov11.columns = ['price_id','open','high','low','close','volume','currency',
                    'cob_date','symbol_id']
    sql_Nov12.columns = ['price_id','open','high','low','close','volume','currency',
                    'cob_date','symbol_id']

    #Merge dataframes from mongodb and equity_prices table from Equity.db  for the two checking days
    s1 = pd.merge(Nov11, sql_Nov11, how='inner', on=['symbol_id'])
    s2 = pd.merge(Nov12, sql_Nov12, how='inner', on=['symbol_id'])

    #If check_price falls outside the high and low range, it is marked a 0 as fat finger error
    def function(check_price,low,high):
        if check_price >= low and check_price<=high:
            return 1
        else:
            return 0
        
    s1['test'] = s1.apply(lambda s1: function(s1.check_price, s1.low, s1.high), axis = 1)
    s2['test'] = s2.apply(lambda s2: function(s2.check_price, s2.low, s2.high), axis = 1)

    #find fat finger errors for two days
    fat_finger11 = s1.loc[s1['test'] == 0]
    fat_finger12 = s2.loc[s2['test'] == 0]
    #cancat the fat errors 
    trades_suspects = pd.concat([fat_finger11,fat_finger12])
    trades_suspects = trades_suspects[["Date_Time","TradeId","Trader","symbol_id","Quantity","Notional","TradeType","Ccy","Counterparty","price_id"]]

    print(trades_suspects)

    CreateTable.create_sql_table('sqlite:///'+sql_name,trades_suspects)
    cursor.executescript('''
    PRAGMA foreign_keys=on;
    BEGIN TRANSACTION;
    CREATE TABLE trades_suspects (Date_Time TEXT PRIMARY KEY NOT NULL,
                                  TradeId TEXT NOT NULL,
                                  Trader TEXT NOT NULL,
                                  symbol_id TEXT NOT NULL,
                                  Quantity INTEGER NOT NULL,
                                  Notional REAL NOT NULL,
                                  TradeType TEXT NOT NULL,
                                  Ccy TEXT NOT NULL,
                                  Counterparty TEXT NOT NULL,
                                  price_id TEXT NOT NULL,
                                  FOREIGN KEY(price_id) REFERENCES equity_prices(price_id));

    INSERT INTO trades_suspects SELECT * FROM oldtable;

    DROP TABLE oldtable;
    COMMIT TRANSACTION;''')
    #In order to set primary and foreign keys, a new table is created.
    #The data from "oldtable" which is a table created by the dataframe named trades_suspects, 
    #has been inserted into the new table called "trades_suspects".
    #This block of code has been moved to main.py from CreateTable.py and the same result can be achieved.


    #Method-1: Use Z-score method to find out the fat finger errors
    #Import data from the "portfolio_positions" table in Equity.db from Sqlite
    fulldata_mongo = col.find({})
    df_mongo_zscore = pd.DataFrame(fulldata_mongo)
    df_mongo_zscore = df_mongo_zscore.rename(columns={'Symbol':'symbol','Trader':'trader','Quantity':'net_quantity','Notional':'net_amount',
                                    'Ccy':'ccy'})
    df_mongo_zscore["NewTime"] = df_mongo_zscore.DateTime.apply(lambda x: datetime.datetime.strptime(x, "ISODate(%Y-%m-%dT%H:%M:%S.000Z)") )
    df_mongo_zscore["cob_date"] = df_mongo_zscore.NewTime.apply(lambda x: x.strftime("%d-%b-%Y"))
    df_mongo_zscore["time_for_id"] = df_mongo_zscore.NewTime.apply(lambda x:x.strftime("%Y%m%d"))
    df_mongo_zscore["pos_id"] = df_mongo_zscore["trader"] + df_mongo_zscore["time_for_id"]+df_mongo_zscore["symbol"]
    df_mongo_zscore = df_mongo_zscore[['pos_id','cob_date','trader','symbol','ccy','net_quantity','net_amount']]

    sql_position = pd.DataFrame(cursor.execute("SELECT * FROM portfolio_positions"))
    sql_position.columns = ['pos_id','cob_date','trader','symbol','ccy','net_quantity','net_amount']

    #Concatenate two dataframes and create a new column to demonstrate the price for each trade
    total_df = pd.concat([sql_position,df_mongo_zscore])
    total_df['check'] = total_df['net_amount']/total_df['net_quantity']
    total_df["time_range"] = total_df.cob_date.apply(lambda x: datetime.datetime.strptime(x, "%d-%b-%Y") ).values


    #Set time start and end points
    start = '2021-01-01'
    end = '2021-11-13'
    con1 = total_df['time_range']>=start
    con2 = total_df['time_range']<end
    total_df = total_df[con1&con2]


    #Data clean and make a deep copy
    cleaneddf2 = total_df.dropna(subset=['check'])
    cleaneddf3 = cleaneddf2.copy(deep=True)


    # Find fat finger errors by using Scipy Zscore
    from scipy import stats
    cleaneddf3['check_zscore']=cleaneddf3.groupby(['symbol'])['check'].transform(stats.zscore)
    test = pd.DataFrame(cleaneddf3[['pos_id','cob_date','trader','symbol','ccy','net_quantity','net_amount',"check_zscore","check"]].sort_values(by=["symbol"])).reset_index()

    fat_finger1 = test.loc[test["check_zscore"] > 3.5]
    fat_finger2 = test.loc[test["check_zscore"] < -3.45]
    trades_suspects_zscore = pd.concat([fat_finger1,fat_finger2])
    print(trades_suspects_zscore)


    # # Aggregate Quantity and Notional for all trades and insert into portfolio_positions
    #Import data from mongodb, and adjust the dataframe form as same as the "portfolio_positions" table in Equity.db

    fulldata_mongo = col.find({})
    df_mongo = pd.DataFrame(fulldata_mongo)
    df_mongo = df_mongo.rename(columns={'Symbol':'symbol','Trader':'trader','Quantity':'quantity','Notional':'amount',
                                    'Ccy':'ccy'})
    df_mongo["NewTime"] = df_mongo.DateTime.apply(lambda x: datetime.datetime.strptime(x, "ISODate(%Y-%m-%dT%H:%M:%S.000Z)") )
    df_mongo["cob_date"] = df_mongo.NewTime.apply(lambda x: x.strftime("%d-%b-%Y"))
    df_mongo["time_for_id"] = df_mongo.NewTime.apply(lambda x:x.strftime("%Y%m%d"))
    df_mongo["pos_id"] = df_mongo["trader"] + df_mongo["time_for_id"]+df_mongo["symbol"]
    df_mongo = df_mongo[['pos_id','cob_date','trader','symbol','ccy','quantity','amount']]

    df_mongo["time_range"] = df_mongo.cob_date.apply(lambda x: datetime.datetime.strptime(x, "%d-%b-%Y") ).values

    start_mongo = '2021-11-11'
    middle_mongo = '2021-11-12'
    end_mongo = '2021-11-13'
    con1_mongo = df_mongo['time_range'] >= start_mongo
    con2_mongo = df_mongo['time_range'] < middle_mongo
    con3_mongo = df_mongo['time_range'] >= middle_mongo
    con4_mongo = df_mongo['time_range'] < end_mongo

    df_mongo_Nov11 = df_mongo[con1_mongo&con2_mongo]
    df_mongo_Nov12 = df_mongo[con3_mongo&con4_mongo]

    #Use groupby symbol
    df_mongo_Nov11['net_quantity'] = df_mongo_Nov11.groupby(['symbol','trader'])['quantity'].transform('sum')
    df_mongo_Nov11['net_amount'] = df_mongo_Nov11.groupby(['symbol','trader'])['amount'].transform('sum')

    df_mongo_Nov12['net_quantity'] = df_mongo_Nov12.groupby(['symbol','trader'])['quantity'].transform('sum')
    df_mongo_Nov12['net_amount'] = df_mongo_Nov12.groupby(['symbol','trader'])['amount'].transform('sum')

    df_mongo_Nov11.drop_duplicates("pos_id",'first',inplace= True)
    df_mongo_Nov12.drop_duplicates("pos_id",'first',inplace= True)

    df_mongo_Nov_2days = pd.concat([df_mongo_Nov11,df_mongo_Nov12])
    df_mongo_Nov_2days = df_mongo_Nov_2days[['pos_id','cob_date','trader','symbol','ccy','net_quantity','net_amount']]

    from sqlalchemy import create_engine
    mydb = create_engine('sqlite:///'+sql_name)
    df_mongo_Nov_2days.to_sql('portfolio_positions', mydb, if_exists='append',index=False)
    #A total of 3972 transactions have been recorded

if __name__ == '__main__':
    main()