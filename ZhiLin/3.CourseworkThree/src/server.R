#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Zhi Lin
# Coursework : Coursework3
# Use Case   : Incorrect Trade Detection
# File       : server.R
#--------------------------------------------------------------------------------------
server <- function(input, output) {
  exposure_table <- reactive({
    Portfolio_Agg %>%
      filter(trader == input$id) %>%
      filter(cob_date == input$date)
  })
  position_table <- reactive({
    PortfolioPosition %>%
      filter(trader == input$id) %>%
      filter(cob_date == input$date)
  })
  output$position_table <- DT::renderDataTable({
    position_table()
  })
  output$exposure_table <- DT::renderDataTable({
    exposure_table()
  })
  output$pie_chart <- renderPlot({
    ggplot(exposure_table(),aes(x="",y=exposures,fill=GICSSector))+
      geom_bar(stat="identity",width=10)+coord_polar(theta = "y")+
      ggtitle("Trader's Exposures")+
      geom_text(aes(label = paste0(round(exposures*100), "%")), position = position_stack(vjust = 0.5))
  })
  output$msg_menu <- renderMenu({
    dropdownMenu(type = "messages",
                 messageItem(
                   from = "Information Source",
                   message = "Information Content")
    )
  })
  output$tsk_menu <- renderMenu({
    dropdownMenu(type = "tasks",
                 taskItem(value = 90, 
                          color = "green","Work Progress")
    )
  })
  output$tab1UI <- renderUI({
    box(width = NULL, status = "primary",
        sidebarLayout(
          sidebarPanel(
            box(width = 12,
                collapsible = TRUE,
                div(style = "height: 15px; background-color: white;"),
                title = "Datatable Info:",
                p("The data table of daily positions by trader is displayed within the chart display area on the right. 
                  Users can select a date and trader to visualize positions in this table.")),
            #selectizeInput('id', 'Select trader id', trader, selected = NULL),
            #selectizeInput('date', 'Select trade date',cob_date, selected = NULL),
            textOutput(outputId = "tab_intro"),
            tags$head(tags$style("#tab_intro{font-size: 15px;font-style: italic;}"))
          ),
          mainPanel(
            h4(strong("Table Preview")),
            tabsetPanel(type = "tabs",
                        tabPanel("Positions",DT::dataTableOutput("position_table"))
                        )
          )
        )
    )
  })
  output$tab2UI <- renderUI({
    box(width = NULL, status = "primary",
        sidebarLayout(
          sidebarPanel(
            box(width = 12,
                collapsible = TRUE,
                div(style = "height: 15px; background-color: white;"),
                title = "Datatable Info:",
                p("The data table of sector exposures by trader is displayed within the chart display area on the right. 
                  Users can select a date and trader to visualize exposures in this table.")),
            #selectizeInput('id', 'Select trader id', trader, selected = NULL),
            #selectizeInput('date', 'Select trade date',cob_date, selected = NULL),
            textOutput(outputId = "tab_intro"),
            tags$head(tags$style("#tab_intro{font-size: 15px;font-style: italic;}"))
          ),
          mainPanel(
            h4(strong("Table Preview")),
            tabsetPanel(type = "tabs",
                        tabPanel("Exposures", DT::dataTableOutput("exposure_table"),plotOutput("pie_chart",height="500px"))
            )
          )
        )
    )
  })
}
